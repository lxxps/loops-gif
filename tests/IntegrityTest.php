<?php

/*
 * This file is part of the loops/gif package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Loops\Gif;

/**
 * Tests for UNpacker/Packer integrity
 *
 * @package    loops/gif
 * @author     Loops <pierrotevrard@gmail.com>
 */
class IntegrityTest extends \PHPUnit_Framework_TestCase
{
  
  /**
   * Test 1: Filesystem support
   */
  public function test1()
  {
    $files = json_decode( $_ENV['loops/gif/integrity_fixtures'] );
    
    foreach( $files as $file )
    {
      $binary1 = file_get_contents( $file );
      
      $binary2 = Packer::pack( Unpacker::unpack( $file ) );
      
      $this->assertSame( $binary2 , $binary1 );
    }
  }
}