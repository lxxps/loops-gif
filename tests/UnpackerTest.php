<?php

/*
 * This file is part of the loops/gif package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Loops\Gif;

/**
 * Tests for Unpacker
 *
 * @package    loops/gif
 * @author     Loops <pierrotevrard@gmail.com>
 */
class UnpackerTest extends \PHPUnit_Framework_TestCase
{
  /*
   * Test case 1: Unpack GIF generated on the fly
   */
  public function test1()
  {
    $handle = tmpfile();
    
    // Header
    fwrite( $handle , 'GIF89a' );
    
    // Logical Screen Descriptor
    fwrite( $handle , pack( 'vvCCC' 
      , 200 // Logical Screen Width, 2 bytes
      , 250 // Logical Screen Height, 2 bytes

      , // <Packed Fields>, 1 byte
          ( 0b1 << 7 ) // Global Color Table Flag, bit OXXXXXXX
        + ( 0b111 << 4 ) // Color Resolution, bits XOOOXXXX
        + ( 0b0 << 3 ) // Sort Flag, bit XXXXOXXX
        + ( 0b10 << 0 ) // Size of Global Color Table, bits XXXXXOOO

      , 7 // Background Color Index, 1 byte
      , 0 // Pixel Aspect Ratio, 1 byte
    ) );
    
    // Global Color Table
    // Size of Global Color Table: 0b10, means 8 colors (2 << size)
    fwrite( $handle , str_repeat( 'xxx' , 2 << 0b10 ) );
    
    // Graphic Control Extension
    fwrite( $handle , pack( 'AACCvCC' 
      , "\x21" // Extension Introducer
      , "\xF9" // Extension Label
      
      , 4 // Block Size, 1 byte

      , // <Packed Fields>, 1 byte
          ( 0b0 << 5 ) // Reserved, bit OOOXXXXX
        + ( 0b10 << 2 ) // Disposal Method, bit XXXOOOXX
        + ( 0b0 << 1 ) // User Input Flag, bit XXXXXXOX
        + ( 0b1 << 0 ) // Transparent Color Flag, bits XXXXXXXO
      
      , 1000 // Delay Time, 2 bytes
      , 7 // Transparent Color Index, 1 byte
      
      , 0 // Block Terminator
    ) );
    
    // Image Descriptor
    fwrite( $handle , pack( 'AvvvvC' 
      , "\x2C" // Image Separator
      
      , 20 // Image Left Position, 2 bytes
      , 30 // Image Top Position, 2 bytes
      , 200 // Image Width, 2 bytes
      , 300 // Image Height, 2 bytes

      , // <Packed Fields>, 1 byte
          ( 0b1 << 7 ) // Local Color Table Flag, bit OXXXXXXX
        + ( 0b1 << 6 ) // Interlace Flag, bit X0XXXXXX
        + ( 0b0 << 5 ) // Sort Flag, bit XXOXXXXX
        + ( 0b10 << 3 ) // Reserved, bits XXXOOXXX
        + ( 0b1 << 0 ) // Size of Local Color Table, bits XXXXXOOO
    ) );
    
    // Local Color Table
    // Size of Local Color Table: 0b1, means 4 colors (2 << size)
    fwrite( $handle , str_repeat( 'xxx' , 2 << 0b1 ) );
    
    // Table Based Image Data
    fwrite( $handle , pack( 'CCA255CA15C' 
      , 128 // LZW Minimum Code Size
      
      , 255 // Data Sub-Block Size
      , str_repeat( 'x' , 255 ) // Data Sub-Block Values
      
      , 15 // Data Sub-Block Size
      , str_repeat( 'x' , 15 ) // Data Sub-Block Values
      
      , 0 // Block Terminator
    ) );
    
    // Application Extension
    fwrite( $handle , pack( 'AACA8A3CA255CA78C' 
      , "\x21" // Extension Introducer
      , "\xFF" // Extension Label
      
      , 11 // Block Size, 1 byte
      
      , 'xxxxxxxx' // Application Identifier, 8 bytes
      , 'xxx' // Application Authentication Code, 3 bytes
      
      , 255 // Data Sub-Block Size
      , str_repeat( 'x' , 255 ) // Data Sub-Block Values
      
      , 78 // Data Sub-Block Size
      , str_repeat( 'x' , 78 ) // Data Sub-Block Values
      
      , 0 // Block Terminator
    ) );
    
    // Comment Extension
    fwrite( $handle , pack( 'AACA255CA78C' 
      , "\x21" // Extension Introducer
      , "\xFE" // Extension Label
      
      , 255 // Data Sub-Block Size
      , str_repeat( 'x' , 255 ) // Data Sub-Block Values
      
      , 78 // Data Sub-Block Size
      , str_repeat( 'x' , 78 ) // Data Sub-Block Values
      
      , 0 // Block Terminator
    ) );
    
    // Graphic Control Extension (2nd occurrence)
    fwrite( $handle , pack( 'AACCvCC' 
      , "\x21" // Extension Introducer
      , "\xF9" // Extension Label
      
      , 4 // Block Size, 1 byte

      , // <Packed Fields>, 1 byte
          ( 0b0 << 5 ) // Reserved, bit OOOXXXXX
        + ( 0b0 << 2 ) // Disposal Method, bit XXXOOOXX
        + ( 0b0 << 1 ) // User Input Flag, bit XXXXXXOX
        + ( 0b1 << 0 ) // Transparent Color Flag, bits XXXXXXXO
      
      , 500 // Delay Time, 2 bytes
      , 0 // Transparent Color Index, 1 byte
      
      , 0 // Block Terminator
    ) );
    
    // Plain Text Extension
    fwrite( $handle , pack( 'AACvvvvCCCCCA255CA127C' 
      , "\x21" // Extension Introducer
      , "\x01" // Extension Label
      
      , 12 // Block Size, 1 byte
      
      , 20 // Text Grid Left Position, 2 bytes
      , 40 // Text Grid Top Position, 2 bytes
      , 200 // Text Grid Width, 2 bytes
      , 400 // Text Grid Height, 2 bytes
      , 1 // Character Cell Width, 1 byte
      , 2 // Character Cell Height, 1 byte
      , 4 // Text Foreground Color Index, 1 byte
      , 8 // Text Background Color Index, 1 byte
      
      , 255 // Data Sub-Block Size
      , str_repeat( 'x' , 255 ) // Data Sub-Block Values
      
      , 127 // Data Sub-Block Size
      , str_repeat( 'x' , 127 ) // Data Sub-Block Values
      
      , 0 // Block Terminator
    ) );
    
    // NETSCAPE2.0 Application Extension
    fwrite( $handle , pack( 'AACA8A3CCvC' 
      , "\x21" // Extension Introducer
      , "\xFF" // Extension Label
      
      , 11 // Block Size, 1 byte
      
      , 'NETSCAPE' // Application Identifier, 8 bytes
      , '2.0' // Application Authentication Code, 3 bytes
      
      , 3 // Block Size, 1 byte
      
      , 1 // Sub-Block Index, 1 byte
      , 200 // Number of Repetitions, 2 bytes
      
      , 0 // Block Terminator
    ) );
    
    // Trailer
    fwrite( $handle , "\x3B" );
    
    // extra useless data
    fwrite( $handle , 'xxx' );
    
    
    // now process check
    $unpacker = new Unpacker( $handle );
    
    
    // before to run the process, let's do some POOP check
    
    // first check: Unpacker should have create a new stream
    $this->assertTrue( $unpacker->__handle !== $handle );
    
    // second check: we should be at first position of the stream
    $this->assertTrue( ftell( $unpacker->__handle ) === 0 );
    
    // third check: packs should be empty
    $this->assertTrue( count( $unpacker->__packs ) === 0 );
    
    // initialize counter
    $counter = 0;
    
    // close handle
    fclose( $handle );
    
    
    // sounds good
    
    // let do an iteration, but without foreach
    
    // Header
    $this->assertTrue( $unpacker->valid() );
    $pack = $unpacker->current();
    $this->assertSame( $pack->getName() , 'Header' );
    $this->assertSame( $pack->getData('Signature') , 'GIF' );
    $this->assertSame( $pack->getData('Version') , '89a' );
    
    // go to next pack
    $unpacker->next();
    // make sure pack counter is ok
    $this->assertTrue( count( $unpacker->__packs ) === ++$counter );
    
    // Logical Screen Descriptor
    $this->assertTrue( $unpacker->valid() );
    $pack = $unpacker->current();
    $this->assertSame( $pack->getName() , 'Logical Screen Descriptor' );
    $this->assertSame( $pack->getData('Logical Screen Width') , 200 );
    $this->assertSame( $pack->getData('Logical Screen Height') , 250 );
    $this->assertSame( $pack->getData('Global Color Table Flag') , 0b1 );
    $this->assertSame( $pack->getData('Color Resolution') , 0b111 );
    $this->assertSame( $pack->getData('Sort Flag') , 0b0 );
    $this->assertSame( $pack->getData('Size of Global Color Table') , 0b10 );
    $this->assertSame( $pack->getData('Background Color Index') , 7 );
    $this->assertSame( $pack->getData('Pixel Aspect Ratio') , 0 );
    
    // go to next pack
    $unpacker->next();
    // make sure pack counter is ok
    $this->assertTrue( count( $unpacker->__packs ) === ++$counter );
    
    // Global Color Table
    $this->assertTrue( $unpacker->valid() );
    $pack = $unpacker->current();
    $this->assertSame( $pack->getName() , 'Global Color Table' );
    // 8 RGB colors with value 'x'
    $table = array_fill( 0 , 8 , array( 'Red' => ord( 'x' ) , 'Green' => ord( 'x' ) , 'Blue' => ord( 'x' ) ) );
    // keep in mind that key are filled is the reverse order
    $this->assertSame( $pack->getData('Color Table') , array_reverse( $table , true ) );
    
    // go to next pack
    $unpacker->next();
    // make sure pack counter is ok
    $this->assertTrue( count( $unpacker->__packs ) === ++$counter );
    
    // Graphic Control Extension (1st occurrence)
    $this->assertTrue( $unpacker->valid() );
    $pack = $unpacker->current();
    $this->assertSame( $pack->getName() , 'Graphic Control Extension' );
    $this->assertSame( $pack->getData('Reserved') , 0b0 );
    $this->assertSame( $pack->getData('Disposal Method') , 0b10 );
    $this->assertSame( $pack->getData('User Input Flag') , 0b0 );
    $this->assertSame( $pack->getData('Transparent Color Flag') , 0b1 );
    $this->assertSame( $pack->getData('Delay Time') , 1000 );
    $this->assertSame( $pack->getData('Transparent Color Index') , 7 );
    
    // go to next pack
    $unpacker->next();
    // make sure pack counter is ok
    $this->assertTrue( count( $unpacker->__packs ) === ++$counter );
    
    // Image Descriptor
    $this->assertTrue( $unpacker->valid() );
    $pack = $unpacker->current();
    $this->assertSame( $pack->getName() , 'Image Descriptor' );
    $this->assertSame( $pack->getData('Image Left Position') , 20 );
    $this->assertSame( $pack->getData('Image Top Position') , 30 );
    $this->assertSame( $pack->getData('Image Width') , 200 );
    $this->assertSame( $pack->getData('Image Height') , 300 );
    $this->assertSame( $pack->getData('Local Color Table Flag') , 0b1 );
    $this->assertSame( $pack->getData('Interlace Flag') , 0b1 );
    $this->assertSame( $pack->getData('Sort Flag') , 0b0 );
    $this->assertSame( $pack->getData('Reserved') , 0b10 );
    $this->assertSame( $pack->getData('Size of Local Color Table') , 0b1 );
    
    // go to next pack
    $unpacker->next();
    // make sure pack counter is ok
    $this->assertTrue( count( $unpacker->__packs ) === ++$counter );
    
    // Local Color Table
    // Size of Local Color Table: 0b1, means 4 colors (2 << size)
    $this->assertTrue( $unpacker->valid() );
    $pack = $unpacker->current();
    $this->assertSame( $pack->getName() , 'Local Color Table' );
    // 8 RGB colors with value 'x'
    $table = array_fill( 0 , 4 , array( 'Red' => ord( 'x' ) , 'Green' => ord( 'x' ) , 'Blue' => ord( 'x' ) ) );
    // keep in mind that key are filled is the reverse order
    $this->assertSame( $pack->getData('Color Table') , array_reverse( $table , true ) );
    
    // go to next pack
    $unpacker->next();
    // make sure pack counter is ok
    $this->assertTrue( count( $unpacker->__packs ) === ++$counter );
    
    // Table Based Image Data
    $this->assertTrue( $unpacker->valid() );
    $pack = $unpacker->current();
    $this->assertSame( $pack->getName() , 'Table Based Image Data' );
    $this->assertSame( $pack->getData('LZW Minimum Code Size') , 128 );
    $this->assertSame( $pack->getData('Image Data') , str_repeat( 'x' , 255 ).str_repeat( 'x' , 15 ) );
    
    // go to next pack
    $unpacker->next();
    // make sure pack counter is ok
    $this->assertTrue( count( $unpacker->__packs ) === ++$counter );
    
    // Application Extension
    $this->assertTrue( $unpacker->valid() );
    $pack = $unpacker->current();
    $this->assertSame( $pack->getName() , 'xxxxxxxxxxx Application Extension' );
    $this->assertSame( $pack->getData('Application Identifier') , 'xxxxxxxx' );
    $this->assertSame( $pack->getData('Application Authentication Code') , 'xxx' );
    $this->assertSame( $pack->getData('Application Data') , str_repeat( 'x' , 255 ).str_repeat( 'x' , 78 ) );
    
    // go to next pack
    $unpacker->next();
    // make sure pack counter is ok
    $this->assertTrue( count( $unpacker->__packs ) === ++$counter );
    
    // Comment Extension
    $this->assertTrue( $unpacker->valid() );
    $pack = $unpacker->current();
    $this->assertSame( $pack->getName() , 'Comment Extension' );
    $this->assertSame( $pack->getData('Comment Data') , str_repeat( 'x' , 255 ).str_repeat( 'x' , 78 ) );
    
    // go to next pack
    $unpacker->next();
    // make sure pack counter is ok
    $this->assertTrue( count( $unpacker->__packs ) === ++$counter );
    
    // Graphic Control Extension (2nd occurrence)
    $this->assertTrue( $unpacker->valid() );
    $pack = $unpacker->current();
    $this->assertSame( $pack->getName() , 'Graphic Control Extension' );
    $this->assertSame( $pack->getData('Reserved') , 0b0 );
    $this->assertSame( $pack->getData('Disposal Method') , 0b0 );
    $this->assertSame( $pack->getData('User Input Flag') , 0b0 );
    $this->assertSame( $pack->getData('Transparent Color Flag') , 0b1 );
    $this->assertSame( $pack->getData('Delay Time') , 500 );
    $this->assertSame( $pack->getData('Transparent Color Index') , 0 );
    
    // go to next pack
    $unpacker->next();
    // make sure pack counter is ok
    $this->assertTrue( count( $unpacker->__packs ) === ++$counter );
    
    // Plain Text Extension
    $this->assertTrue( $unpacker->valid() );
    $pack = $unpacker->current();
    $this->assertSame( $pack->getName() , 'Plain Text Extension' );
    $this->assertSame( $pack->getData('Text Grid Left Position') , 20 );
    $this->assertSame( $pack->getData('Text Grid Top Position') , 40 );
    $this->assertSame( $pack->getData('Text Grid Width') , 200 );
    $this->assertSame( $pack->getData('Text Grid Height') , 400 );
    $this->assertSame( $pack->getData('Character Cell Width') , 1 );
    $this->assertSame( $pack->getData('Character Cell Height') , 2 );
    $this->assertSame( $pack->getData('Text Foreground Color Index') , 4 );
    $this->assertSame( $pack->getData('Text Background Color Index') , 8 );
    $this->assertSame( $pack->getData('Plain Text Data') , str_repeat( 'x' , 255 ).str_repeat( 'x' , 127 ) );
    
    // go to next pack
    $unpacker->next();
    // make sure pack counter is ok
    $this->assertTrue( count( $unpacker->__packs ) === ++$counter );
    
    // NETSCAPE2.0 Application Extension
    $this->assertTrue( $unpacker->valid() );
    $pack = $unpacker->current();
    $this->assertSame( $pack->getName() , 'NETSCAPE2.0 Application Extension' );
    $this->assertSame( $pack->getData('Application Identifier') , 'NETSCAPE' );
    $this->assertSame( $pack->getData('Application Authentication Code') , '2.0' );
    $this->assertSame( $pack->getData('Sub-Block Index') , 1 );
    $this->assertSame( $pack->getData('Number of Repetitions') , 200 );
    
    // go to next pack
    $unpacker->next();
    // make sure pack counter is ok
    $this->assertTrue( count( $unpacker->__packs ) === ++$counter );
    
    // Trailer
    $this->assertTrue( $unpacker->valid() );
    $pack = $unpacker->current();
    $this->assertSame( $pack->getName() , 'Trailer' );
    
    // go to next pack
    $unpacker->next();
    // make sure pack counter is ok
    $this->assertTrue( count( $unpacker->__packs ) === ++$counter );
    
    
    // finally, this one should be false
    $this->assertFalse( $unpacker->valid() );
    
    
    // after the process, let's do some POOP check on extra useless data
    
    // first check: the handle should not be at its end
    $this->assertFalse( feof( $unpacker->__handle ) );
    
    // second check: we should be able to fetch extra useless data
    $this->assertTrue( fread( $unpacker->__handle , 1024 ) === 'xxx' );
    
  }
  
  /**
   * Test 2: Filesystem support
   */
  public function test2()
  {
    $files = json_decode( $_ENV['loops/gif/unpacker_filesupport_fixtures'] );
    
    foreach( $files as $file )
    {
      $unpacker = new Unpacker( $file );
      
      // we must be able to found these blocks
      $this->assertTrue( $unpacker->hasPack( 'Header' ) );
      $this->assertTrue( $unpacker->hasPack( 'Logical Screen Descriptor' ) );
      $this->assertTrue( $unpacker->hasPack( 'Graphic Control Extension' ) );
      $this->assertTrue( $unpacker->hasPack( 'Image Descriptor' ) );
      $this->assertTrue( $unpacker->hasPack( 'Table Based Image Data' ) );
      $this->assertTrue( $unpacker->hasPack( 'Graphic Control Extension' , 1 ) );
      $this->assertTrue( $unpacker->hasPack( 'Image Descriptor' , 1 ) );
      $this->assertTrue( $unpacker->hasPack( 'Table Based Image Data' , 1 ) );
      $this->assertTrue( $unpacker->hasPack( 'Trailer' ) );
      
      unset( $unpacker );
    }
  }
  
  /**
   * Test 3: HTTP support
   */
  public function test3()
  {
    $files = json_decode( $_ENV['loops/gif/unpacker_httpsupport_fixtures'] );
    
    // test with filename
    foreach( $files as $file )
    {
      $unpacker = new Unpacker( $file );
      
      // we must be able to found these blocks
      $this->assertTrue( $unpacker->hasPack( 'Header' ) );
      $this->assertTrue( $unpacker->hasPack( 'Logical Screen Descriptor' ) );
      $this->assertTrue( $unpacker->hasPack( 'Graphic Control Extension' ) );
      $this->assertTrue( $unpacker->hasPack( 'Image Descriptor' ) );
      $this->assertTrue( $unpacker->hasPack( 'Table Based Image Data' ) );
      $this->assertTrue( $unpacker->hasPack( 'Graphic Control Extension' , 1 ) );
      $this->assertTrue( $unpacker->hasPack( 'Image Descriptor' , 1 ) );
      $this->assertTrue( $unpacker->hasPack( 'Table Based Image Data' , 1 ) );
      $this->assertTrue( $unpacker->hasPack( 'Trailer' ) );
      
      unset( $unpacker );
    }
    
    // test with stream
    foreach( $files as $file )
    {
      try
      {
        // an http stream is not seekable, so we should get a warning message
        $handle = fopen( $file , 'rb' );
        $unpacker = new Unpacker( $handle );
      
        throw new \PHPUnit_Framework_UnintentionallyCoveredCodeError( 'An exception must be thrown' );
      }
      catch( \PHPUnit_Framework_Error_Warning $e )
      {
        // expected error, redo without trigger
        $unpacker = @ new Unpacker( $handle );
      }
      
      // we must be able to found these blocks
      $this->assertTrue( $unpacker->hasPack( 'Header' ) );
      $this->assertTrue( $unpacker->hasPack( 'Logical Screen Descriptor' ) );
      $this->assertTrue( $unpacker->hasPack( 'Graphic Control Extension' ) );
      $this->assertTrue( $unpacker->hasPack( 'Image Descriptor' ) );
      $this->assertTrue( $unpacker->hasPack( 'Table Based Image Data' ) );
      $this->assertTrue( $unpacker->hasPack( 'Graphic Control Extension' , 1 ) );
      $this->assertTrue( $unpacker->hasPack( 'Image Descriptor' , 1 ) );
      $this->assertTrue( $unpacker->hasPack( 'Table Based Image Data' , 1 ) );
      $this->assertTrue( $unpacker->hasPack( 'Trailer' ) );
      
      unset( $unpacker );
    }
  }
  
  /**
   * Test 4: data:// support
   */
  public function test4()
  {
    $files = json_decode( $_ENV['loops/gif/unpacker_datasupport_fixtures'] );
    
    foreach( $files as $file )
    {
      $unpacker = new Unpacker( 'data:image/gif;base64,'.base64_encode( file_get_contents( $file ) ) );
      
      // we must be able to found these blocks
      $this->assertTrue( $unpacker->hasPack( 'Header' ) );
      $this->assertTrue( $unpacker->hasPack( 'Logical Screen Descriptor' ) );
      $this->assertTrue( $unpacker->hasPack( 'Graphic Control Extension' ) );
      $this->assertTrue( $unpacker->hasPack( 'Image Descriptor' ) );
      $this->assertTrue( $unpacker->hasPack( 'Table Based Image Data' ) );
      $this->assertTrue( $unpacker->hasPack( 'Graphic Control Extension' , 1 ) );
      $this->assertTrue( $unpacker->hasPack( 'Image Descriptor' , 1 ) );
      $this->assertTrue( $unpacker->hasPack( 'Table Based Image Data' , 1 ) );
      $this->assertTrue( $unpacker->hasPack( 'Trailer' ) );
      
      unset( $unpacker );
    }
  }
}