<?php

/*
 * This file is part of the loops/gif package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Loops\Gif;

/**
 * Tests for Pack_Trailer
 *
 * @package    loops/gif
 * @author     Loops <pierrotevrard@gmail.com>
 */
class Pack_TrailerTest extends \PHPUnit_Framework_TestCase
{
  /*
   * Test case 1: Unpack
   */
  public function test1()
  {
    $handle = tmpfile();
    fwrite( $handle , "\x3B" );
    fseek( $handle , 0 );
    
    $pack = Pack_Factory::instance( 'Trailer' );
    $pack->unpack( $handle );
    
    fclose( $handle );
  }
  
  /*
   * Test case 2: Pack
   */
  public function test2()
  {
    $handle = tmpfile();
    
    $pack = Pack_Factory::instance( 'Trailer' );
    $pack->pack( $handle );
    
    fseek( $handle , 0 );
    $this->assertSame( fread( $handle , 1024 ) , "\x3B" );
    
    fclose( $handle );
  }
  
  
  /*
   * Test case 3: Invalid
   */
  public function test3()
  {
    // Too short
    $handle = tmpfile();
    fwrite( $handle , '' ); // Trailer requires 1 byte
    fseek( $handle , 0 );
    
    try
    {
      $pack = Pack_Factory::instance( 'Trailer' );
      $pack->unpack( $handle );
      
      throw new \PHPUnit_Framework_UnintentionallyCoveredCodeError( 'An exception must be thrown' );
    }
    catch( Exception $e )
    {
      $this->assertSame( $e->getCode() , 'GIF001' );
    }
    
    fclose( $handle );
    
    
    // Invalid Trailer
    $handle = tmpfile();
    fwrite( $handle , 'x' );
    fseek( $handle , 0 );
    
    try
    {
      $pack = Pack_Factory::instance( 'Trailer' );
      $pack->unpack( $handle );
      
      throw new \PHPUnit_Framework_UnintentionallyCoveredCodeError( 'An exception must be thrown' );
    }
    catch( Exception $e )
    {
      $this->assertSame( $e->getCode() , 'GIF018' );
    }
    
    fclose( $handle );
  }
}
