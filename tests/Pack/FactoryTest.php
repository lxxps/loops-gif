<?php

/*
 * This file is part of the loops/gif package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Loops\Gif;

/**
 * Tests for Pack_Factory
 *
 * @package    loops/gif
 * @author     Loops <pierrotevrard@gmail.com>
 */
class Pack_FactoryTest extends \PHPUnit_Framework_TestCase
{
  /**
   * Test case 1: Overwrote class to use
   */
  public function test1()
  {
    // load factory
    class_exists( '\\Loops\\Gif\\Pack_Factory' , true );
    // register map
    $previous = Config::getPackFactoryMap();
    
    // create new classes
    eval( 
        'class MyPack1_Header extends \\Loops\\Gif\\Pack_Header {}'.PHP_EOL
      . 'class MyPack1_LogicalScreenDescriptor extends \\Loops\\Gif\\Pack_LogicalScreenDescriptor {}'.PHP_EOL
      . 'class MyPack1_GlobalColorTable extends \\Loops\\Gif\\Pack_GlobalColorTable {}'.PHP_EOL
      . 'class MyPack1_ImageDescriptor extends \\Loops\\Gif\\Pack_ImageDescriptor {}'.PHP_EOL
      . 'class MyPack1_LocalColorTable extends \\Loops\\Gif\\Pack_LocalColorTable {}'.PHP_EOL
      . 'class MyPack1_TableBasedImageData extends \\Loops\\Gif\\Pack_TableBasedImageData {}'.PHP_EOL
      . 'class MyPack1_Trailer extends \\Loops\\Gif\\Pack_Trailer {}'.PHP_EOL

      . 'class MyPack1_Extension_Comment extends \\Loops\\Gif\\Pack_Extension_Comment {}'.PHP_EOL
      . 'class MyPack1_Extension_PlainText extends \\Loops\\Gif\\Pack_Extension_PlainText {}'.PHP_EOL
      . 'class MyPack1_Extension_Application extends \\Loops\\Gif\\Pack_Extension_Application {}'.PHP_EOL
      . 'class MyPack1_Extension_GraphicControl extends \\Loops\\Gif\\Pack_Extension_GraphicControl {}'.PHP_EOL
      
      . 'class MyPack1_Extension_Application_Netscape20 extends \\Loops\\Gif\\Pack_Extension_Application_Netscape20 {}'.PHP_EOL
    );
    
    // map them
    $map = array(
      'Header' => 'MyPack1_Header' ,
      'Logical Screen Descriptor' => 'MyPack1_LogicalScreenDescriptor' ,
      'Global Color Table' => 'MyPack1_GlobalColorTable' ,
      'Image Descriptor' => 'MyPack1_ImageDescriptor' ,
      'Local Color Table' => 'MyPack1_LocalColorTable' ,
      'Table Based Image Data' => 'MyPack1_TableBasedImageData' ,
      'Trailer' => 'MyPack1_Trailer' ,

      'Comment Extension' => 'MyPack1_Extension_Comment' ,
      'Plain Text Extension' => 'MyPack1_Extension_PlainText' ,
      'Application Extension' => 'MyPack1_Extension_Application' ,
      'Graphic Control Extension' => 'MyPack1_Extension_GraphicControl' ,

      'NETSCAPE2.0 Application Extension' => 'MyPack1_Extension_Application_Netscape20' ,
    );
    
    Config::setPackFactoryMap( $map );
    
    foreach( $map as $name => $class )
    {
      $this->assertSame( Pack_Factory::classname( $name ) , $class );
    }
    
    // restore previous configuration
    Config::setPackFactoryMap( $previous );
  }
  
  /**
   * Test case 2: Initialize a pack from a name and some data
   */
  public function test2()
  {
    $pack = Pack_Factory::instance( 'Image Descriptor' , array(
      'Image Left Position' => 20,
      'Image Top Position' => 30,
      'Image Width' => 200,
      'Image Height' => 300,
      'Local Color Table Flag' => 0b1,
      'Interlace Flag' => 0b1,
      'Sort Flag' => 0b0,
      'Reserved' => 0b10,
      'Size of Local Color Table' => 0b111,
    ) );
      
    $this->assertSame( $pack->getName() , 'Image Descriptor' );
    $this->assertSame( $pack->getData('Image Left Position') , 20 );
    $this->assertSame( $pack->getData('Image Top Position') , 30 );
    $this->assertSame( $pack->getData('Image Width') , 200 );
    $this->assertSame( $pack->getData('Image Height') , 300 );
    $this->assertSame( $pack->getData('Local Color Table Flag') , 0b1 );
    $this->assertSame( $pack->getData('Interlace Flag') , 0b1 );
    $this->assertSame( $pack->getData('Sort Flag') , 0b0 );
    $this->assertSame( $pack->getData('Reserved') , 0b10 );
    $this->assertSame( $pack->getData('Size of Local Color Table') , 0b111 );
  }
}
