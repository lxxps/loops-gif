<?php

/*
 * This file is part of the loops/gif package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Loops\Gif;

/**
 * Tests for Pack_LocalColorTable
 *
 * @package    loops/gif
 * @author     Loops <pierrotevrard@gmail.com>
 */
class Pack_LocalColorTableTest extends \PHPUnit_Framework_TestCase
{
  /*
   * Test case 1: Unpack
   */
  public function test1()
  {
    // 4 colors test
    $handle = tmpfile();
    fwrite( $handle , pack( 'CCC' , 255 , 0 , 0 ) ); // red
    fwrite( $handle , pack( 'CCC' , 0 , 255 , 0 ) ); // green
    fwrite( $handle , pack( 'CCC' , 0 , 0 , 255 ) ); // blue
    fwrite( $handle , pack( 'CCC' , 255 , 255 , 255 ) ); // white
    fseek( $handle , 0 );
    
    $pack = Pack_Factory::instance( 'Local Color Table' );
    $pack->setColorTableSize( 1 ); // 1 means 4 colors: nb colors = 2 << size
    $pack->unpack( $handle );
    
    fclose( $handle );
    
    $table = $pack->getData('Color Table');
    
    $this->assertSame( $table[0] , array( 'Red' => 255 , 'Green' => 0 , 'Blue' => 0 ) );
    $this->assertSame( $table[1] , array( 'Red' => 0 , 'Green' => 255 , 'Blue' => 0 ) );
    $this->assertSame( $table[2] , array( 'Red' => 0 , 'Green' => 0 , 'Blue' => 255 ) );
    $this->assertSame( $table[3] , array( 'Red' => 255 , 'Green' => 255 , 'Blue' => 255 ) );
    
    
    // 256 colors test
    $handle = tmpfile();
    
    // table used for comparison
    $table = array();
    for( $i = 0; $i < 256; $i++ )
    {
      $r = rand( 0 , 255 );
      $g = rand( 0 , 255 );
      $b = rand( 0 , 255 );
      
      $table[] = array( 'Red' => $r , 'Green' => $g , 'Blue' => $b );
      
      // write to handle
      fwrite( $handle , pack( 'CCC' , $r , $g , $b ) );
    }
    
    fseek( $handle , 0 );
    
    $pack = Pack_Factory::instance( 'Local Color Table' );
    $pack->setColorTableSize( 7 ); // 7 means 256 colors
    $pack->unpack( $handle );
    
    fclose( $handle );
    
    // order of keys will be reversed, but that's expected
    $this->assertSame( $pack->getData('Color Table') , array_reverse( $table , true ) );
  }
  
  /*
   * Test case 2: Pack
   */
  public function test2()
  {
    // test with 4 colors
    $handle = tmpfile();
    
    $pack = Pack_Factory::instance( 'Local Color Table' );
    $pack->setData( 'Color Table' , array(
      array( 'Red' => 255 , 'Green' => 0 , 'Blue' => 0 ),
      array( 'Red' => 0 , 'Green' => 255 , 'Blue' => 0 ),
      array( 'Red' => 0 , 'Green' => 0 , 'Blue' => 255 ),
      array( 'Red' => 255 , 'Green' => 255 , 'Blue' => 255 ),
    ) );
    $pack->pack( $handle );
    
    fseek( $handle , 0 );
    $this->assertSame( fread( $handle , 1024 ) , 
        pack( 'CCC' , 255 , 0 , 0 )
      . pack( 'CCC' , 0 , 255 , 0 )
      . pack( 'CCC' , 0 , 0 , 255 )
      . pack( 'CCC' , 255 , 255 , 255 )
    );
    
    fclose( $handle );
    
    
    // test with 6 colors, must result to 8 colors
    $handle = tmpfile();
    
    $pack = Pack_Factory::instance( 'Local Color Table' );
    $pack->setData( 'Color Table' , array(
      array( 'Red' => 255 , 'Green' => 0 , 'Blue' => 0 ),
      array( 'Red' => 0 , 'Green' => 255 , 'Blue' => 0 ),
      array( 'Red' => 0 , 'Green' => 0 , 'Blue' => 255 ),
      array( 'Red' => 127 , 'Green' => 127 , 'Blue' => 127 ), 
      array( 'Red' => 63 , 'Green' => 63 , 'Blue' => 63 ), 
      array( 'Red' => 255 , 'Green' => 255 , 'Blue' => 255 ),
    ) );
    $pack->pack( $handle );
    
    fseek( $handle , 0 );
    $this->assertSame( fread( $handle , 1024 ) , 
        pack( 'CCC' , 255 , 0 , 0 )
      . pack( 'CCC' , 0 , 255 , 0 )
      . pack( 'CCC' , 0 , 0 , 255 )
      . pack( 'CCC' , 127 , 127 , 127 )
      . pack( 'CCC' , 63 , 63 , 63 )
      . pack( 'CCC' , 255 , 255 , 255 )
      . pack( 'CCC' , 0 , 0 , 0 ) // this color is automatically added to fit table size
      . pack( 'CCC' , 0 , 0 , 0 ) // this color is automatically added to fit table size
    );
    
    fclose( $handle );
    
    
    // test with 300 colors, must result to 256 colors
    $handle = tmpfile();
    
    // init fixtures
    $table = array();
    $binary = '';
    for( $i = 0; $i < 300; $i++ )
    {
      $r = rand( 0 , 255 );
      $g = rand( 0 , 255 );
      $b = rand( 0 , 255 );
      
      $table[] = array( 'Red' => $r , 'Green' => $g , 'Blue' => $b );
      // above 256, the color cannot be used
      if( $i < 256 ) $binary .= pack( 'CCC' , $r , $g , $b );
    }
    
    $pack = Pack_Factory::instance( 'Local Color Table' );
    $pack->setData( 'Color Table' , $table );
    $pack->pack( $handle );
    
    fseek( $handle , 0 );
    $this->assertSame( fread( $handle , 1024 ) , $binary );
    
    fclose( $handle );
    
    
    // test with 0 color, must result to 2 colors
    $handle = tmpfile();
    
    $pack = Pack_Factory::instance( 'Local Color Table' );
    $pack->setData( 'Color Table' , array() );
    $pack->pack( $handle );
    
    fseek( $handle , 0 );
    $this->assertSame( fread( $handle , 1024 ) , 
        pack( 'CCC' , 0 , 0 , 0 ) // this color is automatically added to fit table size
      . pack( 'CCC' , 0 , 0 , 0 ) // this color is automatically added to fit table size
    );
    
    fclose( $handle );
  }
  
  
  /*
   * Test case 3: Invalid
   */
  public function test3()
  {
    // Too short
    $handle = tmpfile();
    fwrite( $handle , 'xxx' );
    fseek( $handle , 0 );
    
    try
    {
      $pack = Pack_Factory::instance( 'Local Color Table' );
      $pack->setColorTableSize( 1 ); // 1 means 4 colors
      $pack->unpack( $handle );
      
      throw new \PHPUnit_Framework_UnintentionallyCoveredCodeError( 'An exception must be thrown' );
    }
    catch( Exception $e )
    {
      $this->assertSame( $e->getCode() , 'GIF001' );
    }
    
    fclose( $handle );
    
    
    // No size
    $handle = tmpfile();
    fwrite( $handle , 'xxx' );
    fseek( $handle , 0 );
    
    try
    {
      $pack = Pack_Factory::instance( 'Local Color Table' );
      $pack->unpack( $handle );
      
      throw new \PHPUnit_Framework_UnintentionallyCoveredCodeError( 'An exception must be thrown' );
    }
    catch( Exception $e )
    {
      $this->assertSame( $e->getCode() , 'GIF015' );
    }
    
    fclose( $handle );
  }
  
  
  /*
   * Test case 4: Size and count
   */
  public function test4()
  {
    // 0 colors should provide a size 0 (2 colors)
    $pack = Pack_Factory::instance( 'Local Color Table' );
    $pack->setData( 'Color Table' , array() );
    
    // fetching count will calculate size
    $this->assertSame( $pack->getColorTableCount() , 2 );
    $this->assertSame( $pack->getColorTableSize() , 0 );
    
    
    // 2 colors should provide a size 0 (2 colors)
    $pack = Pack_Factory::instance( 'Local Color Table' );
    $pack->setData( 'Color Table' , array_fill( 0 , 2 , array( 'Red' => 255 , 'Green' => 255 , 'Blue' => 255 ) ) );
    
    // fetching count will calculate size
    $this->assertSame( $pack->getColorTableCount() , 2 );
    $this->assertSame( $pack->getColorTableSize() , 0 );
    
   
    // 4 colors should provide a size 1 (4 colors)
    $pack = Pack_Factory::instance( 'Local Color Table' );
    $pack->setData( 'Color Table' , array_fill( 0 , 4 , array( 'Red' => 255 , 'Green' => 255 , 'Blue' => 255 ) ) );
    
    // fetching count will calculate size
    $this->assertSame( $pack->getColorTableCount() , 4 );
    $this->assertSame( $pack->getColorTableSize() , 1 );
    
    
    // 6 colors should provide a size 2 (8 colors)
    $pack = Pack_Factory::instance( 'Local Color Table' );
    $pack->setData( 'Color Table' , array_fill( 0 , 6 , array( 'Red' => 255 , 'Green' => 255 , 'Blue' => 255 ) ) );
    
    // fetching count will calculate size
    $this->assertSame( $pack->getColorTableCount() , 8 );
    $this->assertSame( $pack->getColorTableSize() , 2 );
    
    
    // 40 colors should provide a size 2 (64 colors)
    $pack = Pack_Factory::instance( 'Local Color Table' );
    $pack->setData( 'Color Table' , array_fill( 0 , 40 , array( 'Red' => 255 , 'Green' => 255 , 'Blue' => 255 ) ) );
    
    // fetching count will calculate size
    $this->assertSame( $pack->getColorTableCount() , 64 );
    $this->assertSame( $pack->getColorTableSize() , 5 );
    
    
    // 250 colors should provide a size 7 (256 colors)
    $pack = Pack_Factory::instance( 'Local Color Table' );
    $pack->setData( 'Color Table' , array_fill( 0 , 250 , array( 'Red' => 255 , 'Green' => 255 , 'Blue' => 255 ) ) );
    
    // fetching count will calculate size
    $this->assertSame( $pack->getColorTableCount() , 256 );
    $this->assertSame( $pack->getColorTableSize() , 7 );
    
  }
}
