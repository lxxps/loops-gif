<?php

/*
 * This file is part of the loops/gif package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Loops\Gif;

/**
 * Tests for Pack_ImageDescriptor
 *
 * @package    loops/gif
 * @author     Loops <pierrotevrard@gmail.com>
 */
class Pack_ImageDescriptorTest extends \PHPUnit_Framework_TestCase
{
  /*
   * Test case 1: Unpack
   */
  public function test1()
  {
    $handle = tmpfile();
    fwrite( $handle , pack( 'AvvvvC' 
      , "\x2C" // Image Separator
      
      , 20 // Image Left Position, 2 bytes
      , 30 // Image Top Position, 2 bytes
      , 200 // Image Width, 2 bytes
      , 300 // Image Height, 2 bytes

      , // <Packed Fields>, 1 byte
          ( 0b1 << 7 ) // Local Color Table Flag, bit OXXXXXXX
        + ( 0b1 << 6 ) // Interlace Flag, bit X0XXXXXX
        + ( 0b0 << 5 ) // Sort Flag, bit XXOXXXXX
        + ( 0b10 << 3 ) // Reserved, bits XXXOOXXX
        + ( 0b111 << 0 ) // Size of Local Color Table, bits XXXXXOOO
    ) );
    fseek( $handle , 0 );
    
    $pack = Pack_Factory::instance( 'Image Descriptor' );
    $pack->unpack( $handle );
    
    fclose( $handle );
    
    $this->assertSame( $pack->getData('Image Left Position') , 20 );
    $this->assertSame( $pack->getData('Image Top Position') , 30 );
    $this->assertSame( $pack->getData('Image Width') , 200 );
    $this->assertSame( $pack->getData('Image Height') , 300 );
    $this->assertSame( $pack->getData('Local Color Table Flag') , 0b1 );
    $this->assertSame( $pack->getData('Interlace Flag') , 0b1 );
    $this->assertSame( $pack->getData('Sort Flag') , 0b0 );
    $this->assertSame( $pack->getData('Reserved') , 0b10 );
    $this->assertSame( $pack->getData('Size of Local Color Table') , 0b111 );
  }
  
  /*
   * Test case 2: Pack
   */
  public function test2()
  {
    $handle = tmpfile();
    
    $pack = Pack_Factory::instance( 'Image Descriptor' );
    $pack->setData( 'Image Left Position' , 40 );
    $pack->setData( 'Image Top Position' , 20 );
    $pack->setData( 'Image Width' , 400 );
    $pack->setData( 'Image Height' , 200 );
    $pack->setData( 'Local Color Table Flag' , 0b1 );
    $pack->setData( 'Interlace Flag' , 0b0 );
    $pack->setData( 'Sort Flag' , 0b1 );
    $pack->setData( 'Reserved' , 0b01 );
    $pack->setData( 'Size of Local Color Table' , 0b101 );
    $pack->pack( $handle );
    
    fseek( $handle , 0 );
    $this->assertSame( fread( $handle , 1024 ) , pack( 'AvvvvC'
      , "\x2C" // Image Separator
      
      , 40 // Image Left Position, 2 bytes
      , 20 // Image Top Position, 2 bytes
      , 400 // Image Width, 2 bytes
      , 200 // Image Height, 2 bytes

      , // <Packed Fields>, 1 byte
          ( 0b1 << 7 ) // Local Color Table Flag, bit OXXXXXXX
        + ( 0b0 << 6 ) // Interlace Flag, bit X0XXXXXX
        + ( 0b1 << 5 ) // Sort Flag, bit XXOXXXXX
        + ( 0b01 << 3 ) // Reserved, bits XXXOOXXX
        + ( 0b101 << 0 ) // Size of Local Color Table, bits XXXXXOOO
    ) );
    
    fclose( $handle );
  }
  
  
  /*
   * Test case 3: Invalid
   */
  public function test3()
  {
    // Missing Image Separator
    $handle = tmpfile();
    fwrite( $handle , 'xxxxx' ); // Image Separator must be present
    fseek( $handle , 0 );
    
    try
    {
      $pack = Pack_Factory::instance( 'Image Descriptor' );
      $pack->unpack( $handle );
      
      throw new \PHPUnit_Framework_UnintentionallyCoveredCodeError( 'An exception must be thrown' );
    }
    catch( Exception $e )
    {
      $this->assertSame( $e->getCode() , 'GIF010' );
    }
    
    fclose( $handle );
    
    
    // Too short
    $handle = tmpfile();
    fwrite( $handle , "\x2C".'xxxxx' ); // Image Descriptor requires Image Separator + 11 bytes
    fseek( $handle , 0 );
    
    try
    {
      $pack = Pack_Factory::instance( 'Image Descriptor' );
      $pack->unpack( $handle );
      
      throw new \PHPUnit_Framework_UnintentionallyCoveredCodeError( 'An exception must be thrown' );
    }
    catch( Exception $e )
    {
      $this->assertSame( $e->getCode() , 'GIF001' );
    }
    
    fclose( $handle );
  }
}
