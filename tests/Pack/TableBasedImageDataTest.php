<?php

/*
 * This file is part of the loops/gif package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Loops\Gif;

/**
 * Tests for Pack_TableBasedImageData
 *
 * @package    loops/gif
 * @author     Loops <pierrotevrard@gmail.com>
 */
class Pack_TableBasedImageDataTest extends \PHPUnit_Framework_TestCase
{
  /*
   * Test case 1: Unpack
   */
  public function test1()
  {
    // standard case
    $handle = tmpfile();
    fwrite( $handle , pack( 'C' , 128 ) ); // LZW Minimum Code Size
    fwrite( $handle , pack( 'CA255' , 255 , str_repeat( 'x' , 255 ) ) ); // Data Sub-Blocks
    fwrite( $handle , pack( 'CA255' , 255 , str_repeat( 'x' , 255 ) ) ); // Data Sub-Blocks
    fwrite( $handle , pack( 'CA15' , 15 , str_repeat( 'x' , 15 ) ) ); // Data Sub-Blocks
    fwrite( $handle , pack( 'C' , 0 ) ); // Block Terminator
    fseek( $handle , 0 );
    
    $pack = Pack_Factory::instance( 'Table Based Image Data' );
    $pack->unpack( $handle );
    
    fclose( $handle );
    
    $this->assertSame( $pack->getData( 'LZW Minimum Code Size' ) , 128 );
    $this->assertSame( $pack->getData( 'Image Data' ) , str_repeat( 'x' , 255 ).str_repeat( 'x' , 255 ).str_repeat( 'x' , 15 ) );
    
    
    // empty case
    $handle = tmpfile();
    fwrite( $handle , pack( 'C' , 64 ) ); // LZW Minimum Code Size
    fwrite( $handle , pack( 'C' , 0 ) ); // Block Terminator
    fseek( $handle , 0 );
    
    $pack = Pack_Factory::instance( 'Table Based Image Data' );
    $pack->unpack( $handle );
    
    fclose( $handle );
    
    $this->assertSame( $pack->getData( 'LZW Minimum Code Size' ) , 64 );
    $this->assertSame( $pack->getData( 'Image Data' ) , '' );
    
    
    // 255 case
    $handle = tmpfile();
    fwrite( $handle , pack( 'C' , 64 ) ); // LZW Minimum Code Size
    fwrite( $handle , pack( 'CA255' , 255 , str_repeat( 'x' , 255 ) ) ); // Data Sub-Blocks
    fwrite( $handle , pack( 'CA255' , 255 , str_repeat( 'x' , 255 ) ) ); // Data Sub-Blocks
    fwrite( $handle , pack( 'C' , 0 ) ); // Block Terminator
    fseek( $handle , 0 );
    
    $pack = Pack_Factory::instance( 'Table Based Image Data' );
    $pack->unpack( $handle );
    
    fclose( $handle );
    
    $this->assertSame( $pack->getData( 'LZW Minimum Code Size' ) , 64 );
    $this->assertSame( $pack->getData( 'Image Data' ) , str_repeat( 'x' , 255 ).str_repeat( 'x' , 255 ) );
  }
  
  /*
   * Test case 2: Pack
   */
  public function test2()
  {
    // standard case
    $handle = tmpfile();
    
    $pack = Pack_Factory::instance( 'Table Based Image Data' );
    $pack->setData( 'LZW Minimum Code Size' , 64 );
    $pack->setData( 'Image Data' , str_repeat( 'x' , 255 ).str_repeat( 'x' , 127 ) );
    $pack->pack( $handle );
    
    fseek( $handle , 0 );
    $this->assertSame( fread( $handle , 1024 ) , 
        pack( 'C' , 64 ) // LZW Minimum Code Size
      . pack( 'CA255' , 255 , str_repeat( 'x' , 255 ) ) // Data Sub-Block
      . pack( 'CA127' , 127 , str_repeat( 'x' , 127 ) ) // Data Sub-Block
      . pack( 'C' , 0 ) // Block Terminator
    );
    
    fclose( $handle );
    
    
    // empty case
    $handle = tmpfile();
    
    $pack = Pack_Factory::instance( 'Table Based Image Data' );
    $pack->setData( 'LZW Minimum Code Size' , 128 );
    $pack->setData( 'Image Data' , '' );
    $pack->pack( $handle );
    
    fseek( $handle , 0 );
    $this->assertSame( fread( $handle , 1024 ) , 
        pack( 'C' , 128 ) // LZW Minimum Code Size
      . pack( 'C' , 0 ) // Block Terminator
    );
    
    fclose( $handle );
    
    
    // 255 case
    $handle = tmpfile();
    
    $pack = Pack_Factory::instance( 'Table Based Image Data' );
    $pack->setData( 'LZW Minimum Code Size' , 64 );
    $pack->setData( 'Image Data' , str_repeat( 'x' , 255 ) );
    $pack->pack( $handle );
    
    fseek( $handle , 0 );
    $this->assertSame( fread( $handle , 1024 ) , 
        pack( 'C' , 64 ) // LZW Minimum Code Size
      . pack( 'CA255' , 255 , str_repeat( 'x' , 255 ) ) // Data Sub-Block
      . pack( 'C' , 0 ) // Block Terminator
    );
    
    fclose( $handle );
  }
  
  
  /*
   * Test case 3: Invalid
   */
  public function test3()
  {
    // Invalid Data Sub-Block
    $handle = tmpfile();
    fwrite( $handle , 'xxx' );
    fseek( $handle , 0 );
    
    try
    {
      $pack = Pack_Factory::instance( 'Table Based Image Data' );
      $pack->unpack( $handle );
      
      throw new \PHPUnit_Framework_UnintentionallyCoveredCodeError( 'An exception must be thrown' );
    }
    catch( Exception $e )
    {
      $this->assertSame( $e->getCode() , 'GIF001' ); // unexpected end-of-file
    }
    
    fclose( $handle );
    
    
    // Missing Block Terminator
    $handle = tmpfile();
    fwrite( $handle , "x\x01x" );
    fseek( $handle , 0 );
    
    try
    {
      $pack = Pack_Factory::instance( 'Table Based Image Data' );
      $pack->unpack( $handle );
      
      throw new \PHPUnit_Framework_UnintentionallyCoveredCodeError( 'An exception must be thrown' );
    }
    catch( Exception $e )
    {
      $this->assertSame( $e->getCode() , 'GIF001' ); // unexpected end-of-file
    }
    
    fclose( $handle );
  }
  
  /*
   * Test case 4: Missing
   */
  public function test4()
  {
    // Missing Image Data
    $handle = tmpfile();
    
    try
    {
      $pack = Pack_Factory::instance( 'Table Based Image Data' );
      $pack->setData( 'LZW Minimum Code Size' , 64 );
      $pack->pack( $handle );
      
      throw new \PHPUnit_Framework_UnintentionallyCoveredCodeError( 'An exception must be thrown' );
    }
    catch( Exception $e )
    {
      $this->assertSame( $e->getCode() , 'GIF002' ); // missing data
    }
    
    fclose( $handle );
    
    
    // Missing LZW Minimum Code Size
    $handle = tmpfile();
    
    try
    {
      $pack = Pack_Factory::instance( 'Table Based Image Data' );
      $pack->setData( 'Image Data' , 'xxx' );
      $pack->pack( $handle );
      
      throw new \PHPUnit_Framework_UnintentionallyCoveredCodeError( 'An exception must be thrown' );
    }
    catch( Exception $e )
    {
      $this->assertSame( $e->getCode() , 'GIF002' ); // missing data
    }
    
    fclose( $handle );
    
  }
}
