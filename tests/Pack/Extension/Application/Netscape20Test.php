<?php

/*
 * This file is part of the loops/gif package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Loops\Gif;

/**
 * Tests for Pack_Extension_Application_Netscape20
 *
 * @package    loops/gif
 * @author     Loops <pierrotevrard@gmail.com>
 */
class Pack_Extension_Application_Netscape20Test extends \PHPUnit_Framework_TestCase
{
  /*
   * Test case 1: Unpack
   */
  public function test1()
  {
    $handle = tmpfile();
    fwrite( $handle , pack( 'AACA8A3CCvC' 
      , "\x21" // Extension Introducer
      , "\xFF" // Extension Label
      
      , 11 // Block Size, 1 byte
      
      , 'NETSCAPE' // Application Identifier, 8 bytes
      , '2.0' // Application Authentication Code, 3 bytes
      
      , 3 // Block Size, 1 byte
      
      , 1 // Sub-Block Index, 1 byte
      , 200 // Number of Repetitions, 2 bytes
      
      , 0 // Block Terminator
    ) );
    
    fseek( $handle , 0 );
    
    $pack = Pack_Factory::instance( 'NETSCAPE2.0 Application Extension' );
    $pack->unpack( $handle );
    
    fclose( $handle );
    
    $this->assertSame( $pack->getData('Application Identifier') , 'NETSCAPE' );
    $this->assertSame( $pack->getData('Application Authentication Code') , '2.0' );
    
    $this->assertSame( $pack->getData('Sub-Block Index') , 1 );
    $this->assertSame( $pack->getData('Number of Repetitions') , 200 );
    
    $this->assertSame( $pack->getName() , 'NETSCAPE2.0 Application Extension' );
  }
  
  /*
   * Test case 2: Pack
   */
  public function test2()
  {
    $handle = tmpfile();
    
    $pack = Pack_Factory::instance( 'NETSCAPE2.0 Application Extension' );
    $pack->setData( 'Application Identifier' , 'NETSCAPE' );
    $pack->setData( 'Application Authentication Code' , '2.0' );
    $pack->setData( 'Sub-Block Index' , 1 );
    $pack->setData( 'Number of Repetitions' , 160 );
    $pack->pack( $handle );
    
    fseek( $handle , 0 );
    $this->assertSame( fread( $handle , 1024 ) , 
      pack( 'AACA8A3CCvC' 
        , "\x21" // Extension Introducer
        , "\xFF" // Extension Label

        , 11 // Block Size, 1 byte

        , 'NETSCAPE' // Application Identifier, 8 bytes
        , '2.0' // Application Authentication Code, 3 bytes

        , 3 // Block Size, 1 byte

        , 1 // Sub-Block Index, 1 byte
        , 160 // Number of Repetitions, 2 bytes

        , 0 // Block Terminator
      )
    );
    
    fclose( $handle );
  }
  
  
  /*
   * Test case 3: Invalid
   */
  public function test3()
  {
    // Invalid Extension Introducer
    $handle = tmpfile();
    fwrite( $handle , 'xxxxx' );
    fseek( $handle , 0 );
    
    try
    {
      $pack = Pack_Factory::instance( 'NETSCAPE2.0 Application Extension' );
      $pack->unpack( $handle );
      
      throw new \PHPUnit_Framework_UnintentionallyCoveredCodeError( 'An exception must be thrown' );
    }
    catch( Exception $e )
    {
      $this->assertSame( $e->getCode() , 'GIF020' );
    }
    
    fclose( $handle );
    
    
    // Invalid Extension Label
    $handle = tmpfile();
    fwrite( $handle , "\x21".'xxxxx' );
    fseek( $handle , 0 );
    
    try
    {
      $pack = Pack_Factory::instance( 'NETSCAPE2.0 Application Extension' );
      $pack->unpack( $handle );
      
      throw new \PHPUnit_Framework_UnintentionallyCoveredCodeError( 'An exception must be thrown' );
    }
    catch( Exception $e )
    {
      $this->assertSame( $e->getCode() , 'GIF021' );
    }
    
    fclose( $handle );
    
    
    // Invalid Block Size
    $handle = tmpfile();
    fwrite( $handle , "\x21\xFF".'xxxxx' );
    fseek( $handle , 0 );
    
    try
    {
      $pack = Pack_Factory::instance( 'NETSCAPE2.0 Application Extension' );
      $pack->unpack( $handle );
      
      throw new \PHPUnit_Framework_UnintentionallyCoveredCodeError( 'An exception must be thrown' );
    }
    catch( Exception $e )
    {
      $this->assertSame( $e->getCode() , 'GIF022' );
    }
    
    fclose( $handle );
    
    
    // Unexpected EOF
    $handle = tmpfile();
    fwrite( $handle , "\x21\xFF\x0B".'xxxx' );
    fseek( $handle , 0 );
    
    try
    {
      $pack = Pack_Factory::instance( 'NETSCAPE2.0 Application Extension' );
      $pack->unpack( $handle );
      
      throw new \PHPUnit_Framework_UnintentionallyCoveredCodeError( 'An exception must be thrown' );
    }
    catch( Exception $e )
    {
      $this->assertSame( $e->getCode() , 'GIF001' );
    }
    
    fclose( $handle );
    
    
    // Unexpected EOF
    $handle = tmpfile();
    fwrite( $handle , "\x21\xFF\x0B".'xxxxxxxxxxx' );
    fseek( $handle , 0 );
    
    try
    {
      $pack = Pack_Factory::instance( 'NETSCAPE2.0 Application Extension' );
      $pack->unpack( $handle );
      
      throw new \PHPUnit_Framework_UnintentionallyCoveredCodeError( 'An exception must be thrown' );
    }
    catch( Exception $e )
    {
      $this->assertSame( $e->getCode() , 'GIF001' );
    }
    
    fclose( $handle );
    
    
    // Invalid Block Size
    $handle = tmpfile();
    fwrite( $handle , "\x21\xFF\x0B".'xxxxxxxxxxxxxxx' );
    fseek( $handle , 0 );
    
    try
    {
      $pack = Pack_Factory::instance( 'NETSCAPE2.0 Application Extension' );
      $pack->unpack( $handle );
      
      throw new \PHPUnit_Framework_UnintentionallyCoveredCodeError( 'An exception must be thrown' );
    }
    catch( Exception $e )
    {
      $this->assertSame( $e->getCode() , 'GIF022' );
    }
    
    fclose( $handle );
    
    
    // Unexpected EOF
    $handle = tmpfile();
    fwrite( $handle , "\x21\xFF\x0B".'xxxxxxxxxxx'."\x03\x01" );
    fseek( $handle , 0 );
    
    try
    {
      $pack = Pack_Factory::instance( 'NETSCAPE2.0 Application Extension' );
      $pack->unpack( $handle );
      
      throw new \PHPUnit_Framework_UnintentionallyCoveredCodeError( 'An exception must be thrown' );
    }
    catch( Exception $e )
    {
      $this->assertSame( $e->getCode() , 'GIF001' );
    }
    
    fclose( $handle );
    
    
    // Invalid Application Identifier
    $handle = tmpfile();
    fwrite( $handle , "\x21\xFF\x0B".'xxxxxxxx2.0'."\x03\x01".'xx'."\x00" );
    fseek( $handle , 0 );
    
    try
    {
      $pack = Pack_Factory::instance( 'NETSCAPE2.0 Application Extension' );
      $pack->unpack( $handle );
      
      throw new \PHPUnit_Framework_UnintentionallyCoveredCodeError( 'An exception must be thrown' );
    }
    catch( Exception $e )
    {
      $this->assertSame( $e->getCode() , 'GIF030' );
    }
    
    fclose( $handle );
    
    
    // Invalid Application Authentication Code
    $handle = tmpfile();
    fwrite( $handle , "\x21\xFF\x0B".'NETSCAPExxx'."\x03\x01".'xx'."\x00" );
    fseek( $handle , 0 );
    
    try
    {
      $pack = Pack_Factory::instance( 'NETSCAPE2.0 Application Extension' );
      $pack->unpack( $handle );
      
      throw new \PHPUnit_Framework_UnintentionallyCoveredCodeError( 'An exception must be thrown' );
    }
    catch( Exception $e )
    {
      $this->assertSame( $e->getCode() , 'GIF031' );
    }
    
    fclose( $handle );
    
    
    // Invalid Sub-Block Index
    $handle = tmpfile();
    fwrite( $handle , "\x21\xFF\x0B".'NETSCAPE2.0'."\x03\x0A".'xx'."\x00" );
    fseek( $handle , 0 );
    
    try
    {
      $pack = Pack_Factory::instance( 'NETSCAPE2.0 Application Extension' );
      $pack->unpack( $handle );
      
      throw new \PHPUnit_Framework_UnintentionallyCoveredCodeError( 'An exception must be thrown' );
    }
    catch( Exception $e )
    {
      $this->assertSame( $e->getCode() , 'GIF032' );
    }
    
    fclose( $handle );
  }
}
