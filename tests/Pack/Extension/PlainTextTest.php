<?php

/*
 * This file is part of the loops/gif package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Loops\Gif;

/**
 * Tests for Pack_Extension_PlainText
 *
 * @package    loops/gif
 * @author     Loops <pierrotevrard@gmail.com>
 */
class Pack_Extension_PlainTextTest extends \PHPUnit_Framework_TestCase
{
  /*
   * Test case 1: Unpack
   */
  public function test1()
  {
    $handle = tmpfile();
    fwrite( $handle , pack( 'AACvvvvCCCC' 
      , "\x21" // Extension Introducer
      , "\x01" // Extension Label
      
      , 12 // Block Size, 1 byte
      
      , 20 // Text Grid Left Position, 2 bytes
      , 40 // Text Grid Top Position, 2 bytes
      , 200 // Text Grid Width, 2 bytes
      , 400 // Text Grid Height, 2 bytes
      , 1 // Character Cell Width, 1 byte
      , 2 // Character Cell Height, 1 byte
      , 4 // Text Foreground Color Index, 1 byte
      , 8 // Text Background Color Index, 1 byte
    ) );
    fwrite( $handle , pack( 'CA255' , 255 , str_repeat( 'x' , 255 ) ) );
    fwrite( $handle , pack( 'CA78' , 78 , str_repeat( 'x' , 78 ) ) );
    fwrite( $handle , pack( 'C' , 0 ) ); // Block Terminator
    fseek( $handle , 0 );
    
    $bin = fread( $handle , 1024 );
    fseek( $handle , 0 );
    
    $pack = Pack_Factory::instance( 'Plain Text Extension' );
    $pack->unpack( $handle );
    
    $this->assertSame( $pack->_raw , $bin );
    
    fclose( $handle );
    
    $this->assertSame( $pack->getData('Text Grid Left Position') , 20 );
    $this->assertSame( $pack->getData('Text Grid Top Position') , 40 );
    $this->assertSame( $pack->getData('Text Grid Width') , 200 );
    $this->assertSame( $pack->getData('Text Grid Height') , 400 );
    $this->assertSame( $pack->getData('Character Cell Width') , 1 );
    $this->assertSame( $pack->getData('Character Cell Height') , 2 );
    $this->assertSame( $pack->getData('Text Foreground Color Index') , 4 );
    $this->assertSame( $pack->getData('Text Background Color Index') , 8 );
    
    $this->assertSame( $pack->getData('Plain Text Data') , str_repeat( 'x' , 255 ).str_repeat( 'x' , 78 ) );
  }
  
  /*
   * Test case 2: Pack
   */
  public function test2()
  {
    $handle = tmpfile();
    
    $pack = Pack_Factory::instance( 'Plain Text Extension' );
    $pack->setData( 'Text Grid Left Position' , 30 );
    $pack->setData( 'Text Grid Top Position' , 35 );
    $pack->setData( 'Text Grid Width' , 300 );
    $pack->setData( 'Text Grid Height' , 350 );
    $pack->setData( 'Character Cell Width' , 3 );
    $pack->setData( 'Character Cell Height' , 6 );
    $pack->setData( 'Text Foreground Color Index' , 7 );
    $pack->setData( 'Text Background Color Index' , 9 );
    $pack->setData( 'Plain Text Data' , str_repeat( 'x' , 255 ).str_repeat( 'x' , 50 ) );
    $pack->pack( $handle );
    
    fseek( $handle , 0 );
    $this->assertSame( fread( $handle , 1024 ) , 
        pack( 'AACvvvvCCCC' 
          , "\x21" // Extension Introducer
          , "\x01" // Extension Label

          , 12 // Block Size, 1 byte

          , 30 // Text Grid Left Position, 2 bytes
          , 35 // Text Grid Top Position, 2 bytes
          , 300 // Text Grid Width, 2 bytes
          , 350 // Text Grid Height, 2 bytes
          , 3 // Character Cell Width, 1 byte
          , 6 // Character Cell Height, 1 byte
          , 7 // Text Foreground Color Index, 1 byte
          , 9 // Text Background Color Index, 1 byte
        )
      . pack( 'CA255' , 255 , str_repeat( 'x' , 255 ) )
      . pack( 'CA50' , 50 , str_repeat( 'x' , 50 ) )
      . pack( 'C' , 0 )
    );
    
    fclose( $handle );
  }
  
  
  /*
   * Test case 3: Invalid
   */
  public function test3()
  {
    // Invalid Extension Introducer
    $handle = tmpfile();
    fwrite( $handle , 'xxxxx' );
    fseek( $handle , 0 );
    
    try
    {
      $pack = Pack_Factory::instance( 'Plain Text Extension' );
      $pack->unpack( $handle );
      
      throw new \PHPUnit_Framework_UnintentionallyCoveredCodeError( 'An exception must be thrown' );
    }
    catch( Exception $e )
    {
      $this->assertSame( $e->getCode() , 'GIF020' );
    }
    
    fclose( $handle );
    
    
    // Invalid Extension Label
    $handle = tmpfile();
    fwrite( $handle , "\x21".'xxxxx' );
    fseek( $handle , 0 );
    
    try
    {
      $pack = Pack_Factory::instance( 'Plain Text Extension' );
      $pack->unpack( $handle );
      
      throw new \PHPUnit_Framework_UnintentionallyCoveredCodeError( 'An exception must be thrown' );
    }
    catch( Exception $e )
    {
      $this->assertSame( $e->getCode() , 'GIF021' );
    }
    
    fclose( $handle );
    
    
    // Invalid Block Size
    $handle = tmpfile();
    fwrite( $handle , "\x21\x01".'xxxxx' );
    fseek( $handle , 0 );
    
    try
    {
      $pack = Pack_Factory::instance( 'Plain Text Extension' );
      $pack->unpack( $handle );
      
      throw new \PHPUnit_Framework_UnintentionallyCoveredCodeError( 'An exception must be thrown' );
    }
    catch( Exception $e )
    {
      $this->assertSame( $e->getCode() , 'GIF022' );
    }
    
    fclose( $handle );
    
    
    // Unexpected EOF
    $handle = tmpfile();
    fwrite( $handle , "\x21\x01\x0C".'xxxx' );
    fseek( $handle , 0 );
    
    try
    {
      $pack = Pack_Factory::instance( 'Plain Text Extension' );
      $pack->unpack( $handle );
      
      throw new \PHPUnit_Framework_UnintentionallyCoveredCodeError( 'An exception must be thrown' );
    }
    catch( Exception $e )
    {
      $this->assertSame( $e->getCode() , 'GIF001' );
    }
    
    fclose( $handle );
    
    
    // Unexpected EOF
    $handle = tmpfile();
    fwrite( $handle , "\x21\x01\x0C".'xxxxxxxxxxx' );
    fseek( $handle , 0 );
    
    try
    {
      $pack = Pack_Factory::instance( 'Plain Text Extension' );
      $pack->unpack( $handle );
      
      throw new \PHPUnit_Framework_UnintentionallyCoveredCodeError( 'An exception must be thrown' );
    }
    catch( Exception $e )
    {
      $this->assertSame( $e->getCode() , 'GIF001' );
    }
    
    fclose( $handle );
  }
}
