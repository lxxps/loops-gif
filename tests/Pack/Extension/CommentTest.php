<?php

/*
 * This file is part of the loops/gif package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Loops\Gif;

/**
 * Tests for Pack_Extension_Comment
 *
 * @package    loops/gif
 * @author     Loops <pierrotevrard@gmail.com>
 */
class Pack_Extension_CommentTest extends \PHPUnit_Framework_TestCase
{
  /*
   * Test case 1: Unpack
   */
  public function test1()
  {
    $handle = tmpfile();
    fwrite( $handle , pack( 'AA' 
      , "\x21" // Extension Introducer
      , "\xFE" // Extension Label
    ) );
    fwrite( $handle , pack( 'CA255' , 255 , str_repeat( 'x' , 255 ) ) );
    fwrite( $handle , pack( 'CA78' , 78 , str_repeat( 'x' , 78 ) ) );
    fwrite( $handle , pack( 'C' , 0 ) ); // Block Terminator
    fseek( $handle , 0 );
    
    $bin = fread( $handle , 1024 );
    fseek( $handle , 0 );
    
    $pack = Pack_Factory::instance( 'Comment Extension' );
    $pack->unpack( $handle );
    
    $this->assertSame( $pack->_raw , $bin );
    
    fclose( $handle );
    
    $this->assertSame( $pack->getData('Comment Data') , str_repeat( 'x' , 255 ).str_repeat( 'x' , 78 ) );
  }
  
  /*
   * Test case 2: Pack
   */
  public function test2()
  {
    $handle = tmpfile();
    
    $pack = Pack_Factory::instance( 'Comment Extension' );
    $pack->setData( 'Comment Data' , str_repeat( 'x' , 255 ).str_repeat( 'x' , 50 ) );
    $pack->pack( $handle );
    
    fseek( $handle , 0 );
    $this->assertSame( fread( $handle , 1024 ) , 
        pack( 'AA' 
          , "\x21" // Extension Introducer
          , "\xFE" // Extension Label
        )
      . pack( 'CA255' , 255 , str_repeat( 'x' , 255 ) )
      . pack( 'CA50' , 50 , str_repeat( 'x' , 50 ) )
      . pack( 'C' , 0 )
    );
    
    fclose( $handle );
  }
  
  
  /*
   * Test case 3: Invalid
   */
  public function test3()
  {
    // Invalid Extension Introducer
    $handle = tmpfile();
    fwrite( $handle , 'xxxxx' );
    fseek( $handle , 0 );
    
    try
    {
      $pack = Pack_Factory::instance( 'Comment Extension' );
      $pack->unpack( $handle );
      
      throw new \PHPUnit_Framework_UnintentionallyCoveredCodeError( 'An exception must be thrown' );
    }
    catch( Exception $e )
    {
      $this->assertSame( $e->getCode() , 'GIF020' );
    }
    
    fclose( $handle );
    
    
    // Invalid Extension Label
    $handle = tmpfile();
    fwrite( $handle , "\x21".'xxxxx' );
    fseek( $handle , 0 );
    
    try
    {
      $pack = Pack_Factory::instance( 'Comment Extension' );
      $pack->unpack( $handle );
      
      throw new \PHPUnit_Framework_UnintentionallyCoveredCodeError( 'An exception must be thrown' );
    }
    catch( Exception $e )
    {
      $this->assertSame( $e->getCode() , 'GIF021' );
    }
    
    fclose( $handle );
    
    
    // Unexpected EOF
    $handle = tmpfile();
    fwrite( $handle , "\x21\xFE\x04".'xx' );
    fseek( $handle , 0 );
    
    try
    {
      $pack = Pack_Factory::instance( 'Comment Extension' );
      $pack->unpack( $handle );
      
      throw new \PHPUnit_Framework_UnintentionallyCoveredCodeError( 'An exception must be thrown' );
    }
    catch( Exception $e )
    {
      $this->assertSame( $e->getCode() , 'GIF001' );
    }
    
    fclose( $handle );
    
    
    // Unexpected EOF
    $handle = tmpfile();
    fwrite( $handle , "\x21\xFE\x04".'xxxx' );
    fseek( $handle , 0 );
    
    try
    {
      $pack = Pack_Factory::instance( 'Comment Extension' );
      $pack->unpack( $handle );
      
      throw new \PHPUnit_Framework_UnintentionallyCoveredCodeError( 'An exception must be thrown' );
    }
    catch( Exception $e )
    {
      $this->assertSame( $e->getCode() , 'GIF001' );
    }
    
    fclose( $handle );
  }
}
