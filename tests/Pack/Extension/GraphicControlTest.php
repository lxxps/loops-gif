<?php

/*
 * This file is part of the loops/gif package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Loops\Gif;

/**
 * Tests for Pack_Extension_GraphicControl
 *
 * @package    loops/gif
 * @author     Loops <pierrotevrard@gmail.com>
 */
class Pack_Extension_GraphicControlTest extends \PHPUnit_Framework_TestCase
{
  /*
   * Test case 1: Unpack
   */
  public function test1()
  {
    $handle = tmpfile();
    fwrite( $handle , pack( 'AACCvCC' 
      , "\x21" // Extension Introducer
      , "\xF9" // Extension Label
      
      , 4 // Block Size, 1 byte

      , // <Packed Fields>, 1 byte
          ( 0b0 << 5 ) // Reserved, bit OOOXXXXX
        + ( 0b10 << 2 ) // Disposal Method, bit XXXOOOXX
        + ( 0b0 << 1 ) // User Input Flag, bit XXXXXXOX
        + ( 0b1 << 0 ) // Transparent Color Flag, bits XXXXXXXO
      
      , 1000 // Delay Time, 2 bytes
      , 255 // Transparent Color Index, 1 byte
      
      , 0 // Block Terminator
    ) );
    fseek( $handle , 0 );
    
    $pack = Pack_Factory::instance( 'Graphic Control Extension' );
    $pack->unpack( $handle );
    
    fclose( $handle );
    
    $this->assertSame( $pack->getData('Reserved') , 0b0 );
    $this->assertSame( $pack->getData('Disposal Method') , 0b10 );
    $this->assertSame( $pack->getData('User Input Flag') , 0b0 );
    $this->assertSame( $pack->getData('Transparent Color Flag') , 0b1 );
    $this->assertSame( $pack->getData('Delay Time') , 1000 );
    $this->assertSame( $pack->getData('Transparent Color Index') , 255 );
  }
  
  /*
   * Test case 2: Pack
   */
  public function test2()
  {
    $handle = tmpfile();
    
    $pack = Pack_Factory::instance( 'Graphic Control Extension' );
    $pack->setData( 'Reserved' , 0b0 );
    $pack->setData( 'Disposal Method' , 0b111 );
    $pack->setData( 'User Input Flag' , 0b0 );
    $pack->setData( 'Transparent Color Flag' , 0b1 );
    $pack->setData( 'Delay Time' , 60 );
    $pack->setData( 'Transparent Color Index' , 127 );
    $pack->pack( $handle );
    
    fseek( $handle , 0 );
    $this->assertSame( fread( $handle , 1024 ) , pack( 'AACCvCC' 
      , "\x21" // Extension Introducer
      , "\xF9" // Extension Label
      
      , 4 // Block Size, 1 byte

      , // <Packed Fields>, 1 byte
          ( 0b0 << 5 ) // Reserved, bit OOOXXXXX
        + ( 0b111 << 2 ) // Disposal Method, bit XXXOOOXX
        + ( 0b0 << 1 ) // User Input Flag, bit XXXXXXOX
        + ( 0b1 << 0 ) // Transparent Color Flag, bits XXXXXXXO
      
      , 60 // Delay Time, 2 bytes
      , 127 // Transparent Color Index, 1 byte
      
      , 0 // Block Terminator
    ) );
    
    fclose( $handle );
  }
  
  
  /*
   * Test case 3: Invalid
   */
  public function test3()
  {
    // Missing Invalid Introducer
    $handle = tmpfile();
    fwrite( $handle , 'xxxxx' );
    fseek( $handle , 0 );
    
    try
    {
      $pack = Pack_Factory::instance( 'Graphic Control Extension' );
      $pack->unpack( $handle );
      
      throw new \PHPUnit_Framework_UnintentionallyCoveredCodeError( 'An exception must be thrown' );
    }
    catch( Exception $e )
    {
      $this->assertSame( $e->getCode() , 'GIF020' );
    }
    
    fclose( $handle );
    
    
    // Invalid Extension Label
    $handle = tmpfile();
    fwrite( $handle , "\x21".'xxxxx' );
    fseek( $handle , 0 );
    
    try
    {
      $pack = Pack_Factory::instance( 'Graphic Control Extension' );
      $pack->unpack( $handle );
      
      throw new \PHPUnit_Framework_UnintentionallyCoveredCodeError( 'An exception must be thrown' );
    }
    catch( Exception $e )
    {
      $this->assertSame( $e->getCode() , 'GIF021' );
    }
    
    fclose( $handle );
    
    
    // Invalid Block Size
    $handle = tmpfile();
    fwrite( $handle , "\x21\xF9".'xxxxx' );
    fseek( $handle , 0 );
    
    try
    {
      $pack = Pack_Factory::instance( 'Graphic Control Extension' );
      $pack->unpack( $handle );
      
      throw new \PHPUnit_Framework_UnintentionallyCoveredCodeError( 'An exception must be thrown' );
    }
    catch( Exception $e )
    {
      $this->assertSame( $e->getCode() , 'GIF022' );
    }
    
    fclose( $handle );
    
    
    // Missing Block Terminator
    $handle = tmpfile();
    fwrite( $handle , "\x21\xF9\x04".'xxxx' );
    fseek( $handle , 0 );
    
    try
    {
      $pack = Pack_Factory::instance( 'Graphic Control Extension' );
      $pack->unpack( $handle );
      
      throw new \PHPUnit_Framework_UnintentionallyCoveredCodeError( 'An exception must be thrown' );
    }
    catch( Exception $e )
    {
      $this->assertSame( $e->getCode() , 'GIF001' );
    }
    
    fclose( $handle );
  }
}
