<?php

/*
 * This file is part of the loops/gif package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Loops\Gif;

/**
 * Tests for Pack_Extension_Unknown
 *
 * @package    loops/gif
 * @author     Loops <pierrotevrard@gmail.com>
 */
class Pack_Extension_UnknownTest extends \PHPUnit_Framework_TestCase
{
  /**
   * Temporary storage for Pack_Factory map
   * 
   * @var array
   * @access protected
   * @static
   */
  public static $_previous_pack_factory_map;
  
  /**
   * Register Unknown Extension to Pack_Factory
   * 
   * @param  none
   * @return void
   * @throws InvalidArgumentException
   */
  public static function setUpBeforeClass()
  {
    // register previous Pack_Factory map
    self::$_previous_pack_factory_map = Config::getPackFactoryMap();
    
    // append Unkowwn Extension
    // if this pack name is already registered, nothing will happen
    Config::appendPackFactoryMap( array(
      'Unknown Extension' => 'Loops\\Gif\\Pack_Extension_Unknown' ,
    ) );
  }

  /**
   * Unregister Unknown Extension from Pack_Factory
   * 
   * @param  none
   * @return void
   */
  public static function tearDownAfterClass()
  {
    // restore previous configuration
    Config::setPackFactoryMap( self::$_previous_pack_factory_map );
    
    // remove it
    self::$_previous_pack_factory_map = null;
  }
  
  /*
   * Test case 1: Unpack
   */
  public function test1()
  {
    $handle = tmpfile();
    fwrite( $handle , pack( 'AA' 
      , "\x21" // Extension Introducer
      , "\xDD" // Extension Label
    ) );
    fwrite( $handle , pack( 'CA255' , 255 , str_repeat( 'x' , 255 ) ) ); // Data Sub-Block
    fwrite( $handle , pack( 'CA78' , 78 , str_repeat( 'x' , 78 ) ) ); // Data Sub-Block
    fwrite( $handle , pack( 'C' , 0 ) ); // Block Terminator
    
    fseek( $handle , 0 );
    
    $pack = Pack_Factory::instance( 'Unknown Extension' );
    $pack->unpack( $handle );
    
    fclose( $handle );
    
    $this->assertSame( $pack->getLabel() , "\xDD" );
    $this->assertSame( $pack->getData('Extension Data') , str_repeat( 'x' , 255 ).str_repeat( 'x' , 78 ) );
  }
  
  /*
   * Test case 2: Pack
   */
  public function test2()
  {
    $handle = tmpfile();
    
    $pack = Pack_Factory::instance( 'Unknown Extension' );
    $pack->setLabel( "\xDD" );
    $pack->setData( 'Extension Data' , str_repeat( 'x' , 255 ).str_repeat( 'x' , 50 ) );
    $pack->pack( $handle );
    
    fseek( $handle , 0 );
    $this->assertSame( fread( $handle , 1024 ) , 
        pack( 'AA' 
          , "\x21" // Extension Introducer
          , "\xDD" // Extension Label
        )
      . pack( 'CA255' , 255 , str_repeat( 'x' , 255 ) )
      . pack( 'CA50' , 50 , str_repeat( 'x' , 50 ) )
      . pack( 'C' , 0 )
    );
    
    fclose( $handle );
  }
  
  
  /*
   * Test case 3: Invalid
   */
  public function test3()
  {
    // Invalid Extension Introducer
    $handle = tmpfile();
    fwrite( $handle , 'xxxxx' );
    fseek( $handle , 0 );
    
    try
    {
      $pack = Pack_Factory::instance( 'Unknown Extension' );
      $pack->unpack( $handle );
      
      throw new \PHPUnit_Framework_UnintentionallyCoveredCodeError( 'An exception must be thrown' );
    }
    catch( Exception $e )
    {
      $this->assertSame( $e->getCode() , 'GIF020' );
    }
    
    fclose( $handle );
    
    
    // Unexpected EOF
    $handle = tmpfile();
    fwrite( $handle , "\x21\xFF\x0B".'xxxx' );
    fseek( $handle , 0 );
    
    try
    {
      $pack = Pack_Factory::instance( 'Unknown Extension' );
      $pack->unpack( $handle );
      
      throw new \PHPUnit_Framework_UnintentionallyCoveredCodeError( 'An exception must be thrown' );
    }
    catch( Exception $e )
    {
      $this->assertSame( $e->getCode() , 'GIF001' );
    }
    
    fclose( $handle );
    
    
    // Unexpected EOF
    $handle = tmpfile();
    fwrite( $handle , "\x21\xFF\x0B".'xxxxxxxxxx' );
    fseek( $handle , 0 );
    
    try
    {
      $pack = Pack_Factory::instance( 'Unknown Extension' );
      $pack->unpack( $handle );
      
      throw new \PHPUnit_Framework_UnintentionallyCoveredCodeError( 'An exception must be thrown' );
    }
    catch( Exception $e )
    {
      $this->assertSame( $e->getCode() , 'GIF001' );
    }
    
    fclose( $handle );
  }
  
  /**
   * Test case 4: Missing
   */
  public function test4()
  {
    // Missing Extension Data
    $handle = tmpfile();
    
    try
    {
      $pack = Pack_Factory::instance( 'Unknown Extension' );
      $pack->setLabel( "\x42" );
      $pack->pack( $handle );
      
      throw new \PHPUnit_Framework_UnintentionallyCoveredCodeError( 'An exception must be thrown' );
    }
    catch( Exception $e )
    {
      $this->assertSame( $e->getCode() , 'GIF002' );
    }
    
    fclose( $handle );
    
    
    // Missing Extension Label
    $handle = tmpfile();
    
    try
    {
      $pack = Pack_Factory::instance( 'Unknown Extension' );
      $pack->setData( 'Extension Data' , 'xxx' );
      $pack->pack( $handle );
      
      throw new \PHPUnit_Framework_UnintentionallyCoveredCodeError( 'An exception must be thrown' );
    }
    catch( Exception $e )
    {
      $this->assertSame( $e->getCode() , 'GIF035' );
    }
    
    fclose( $handle );
  }
}
