<?php

/*
 * This file is part of the loops/gif package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Loops\Gif;

/**
 * Tests for Pack_Extension_Application
 *
 * @package    loops/gif
 * @author     Loops <pierrotevrard@gmail.com>
 */
class Pack_Extension_ApplicationTest extends \PHPUnit_Framework_TestCase
{
  /*
   * Test case 1: Unpack
   */
  public function test1()
  {
    $handle = tmpfile();
    fwrite( $handle , pack( 'AACA8A3' 
      , "\x21" // Extension Introducer
      , "\xFF" // Extension Label
      
      , 11 // Block Size, 1 byte
      
      , 'xxxxxxxx' // Application Identifier, 8 bytes
      , '042' // Application Authentication Code, 3 bytes
    ) );
    fwrite( $handle , pack( 'CA255' , 255 , str_repeat( 'x' , 255 ) ) ); // Data Sub-Block
    fwrite( $handle , pack( 'CA78' , 78 , str_repeat( 'x' , 78 ) ) ); // Data Sub-Block
    fwrite( $handle , pack( 'C' , 0 ) ); // Block Terminator
    
    fseek( $handle , 0 );
    
    $pack = Pack_Factory::instance( 'Application Extension' );
    $pack->unpack( $handle );
    
    fclose( $handle );
    
    $this->assertSame( $pack->getData('Application Identifier') , 'xxxxxxxx' );
    $this->assertSame( $pack->getData('Application Authentication Code') , '042' );
    
    $this->assertSame( $pack->getData('Application Data') , str_repeat( 'x' , 255 ).str_repeat( 'x' , 78 ) );
    
    $this->assertSame( $pack->getName() , 'xxxxxxxx042 Application Extension' );
  }
  
  /*
   * Test case 2: Pack
   */
  public function test2()
  {
    $handle = tmpfile();
    
    $pack = Pack_Factory::instance( 'Application Extension' );
    $pack->setData( 'Application Identifier' , 'xxxxxxxx' );
    $pack->setData( 'Application Authentication Code' , '042' );
    $pack->setData( 'Application Data' , str_repeat( 'x' , 255 ).str_repeat( 'x' , 50 ) );
    $pack->pack( $handle );
    
    fseek( $handle , 0 );
    $this->assertSame( fread( $handle , 1024 ) , 
        pack( 'AACA8A3' 
          , "\x21" // Extension Introducer
          , "\xFF" // Extension Label

          , 11 // Block Size, 1 byte

          , 'xxxxxxxx' // Application Identifier, 8 bytes
          , '042' // Application Authentication Code, 3 bytes
        )
      . pack( 'CA255' , 255 , str_repeat( 'x' , 255 ) ) // Data Sub-Block
      . pack( 'CA50' , 50 , str_repeat( 'x' , 50 ) ) // Data Sub-Block
      . pack( 'C' , 0 ) // Block Terminator
    );
    
    fclose( $handle );
  }
  
  
  /*
   * Test case 3: Invalid
   */
  public function test3()
  {
    // Invalid Extension Introducer
    $handle = tmpfile();
    fwrite( $handle , 'xxxxx' );
    fseek( $handle , 0 );
    
    try
    {
      $pack = Pack_Factory::instance( 'Application Extension' );
      $pack->unpack( $handle );
      
      throw new \PHPUnit_Framework_UnintentionallyCoveredCodeError( 'An exception must be thrown' );
    }
    catch( Exception $e )
    {
      $this->assertSame( $e->getCode() , 'GIF020' );
    }
    
    fclose( $handle );
    
    
    // Invalid Extension Label
    $handle = tmpfile();
    fwrite( $handle , "\x21".'xxxxx' );
    fseek( $handle , 0 );
    
    try
    {
      $pack = Pack_Factory::instance( 'Application Extension' );
      $pack->unpack( $handle );
      
      throw new \PHPUnit_Framework_UnintentionallyCoveredCodeError( 'An exception must be thrown' );
    }
    catch( Exception $e )
    {
      $this->assertSame( $e->getCode() , 'GIF021' );
    }
    
    fclose( $handle );
    
    
    // Invalid Block Size
    $handle = tmpfile();
    fwrite( $handle , "\x21\xFF".'xxxxx' );
    fseek( $handle , 0 );
    
    try
    {
      $pack = Pack_Factory::instance( 'Application Extension' );
      $pack->unpack( $handle );
      
      throw new \PHPUnit_Framework_UnintentionallyCoveredCodeError( 'An exception must be thrown' );
    }
    catch( Exception $e )
    {
      $this->assertSame( $e->getCode() , 'GIF022' );
    }
    
    fclose( $handle );
    
    
    // Unexpected EOF
    $handle = tmpfile();
    fwrite( $handle , "\x21\xFF\x0B".'xxxx' );
    fseek( $handle , 0 );
    
    try
    {
      $pack = Pack_Factory::instance( 'Application Extension' );
      $pack->unpack( $handle );
      
      throw new \PHPUnit_Framework_UnintentionallyCoveredCodeError( 'An exception must be thrown' );
    }
    catch( Exception $e )
    {
      $this->assertSame( $e->getCode() , 'GIF001' );
    }
    
    fclose( $handle );
    
    
    // Unexpected EOF
    $handle = tmpfile();
    fwrite( $handle , "\x21\xFF\x0B".'xxxxxxxxxxx' );
    fseek( $handle , 0 );
    
    try
    {
      $pack = Pack_Factory::instance( 'Application Extension' );
      $pack->unpack( $handle );
      
      throw new \PHPUnit_Framework_UnintentionallyCoveredCodeError( 'An exception must be thrown' );
    }
    catch( Exception $e )
    {
      $this->assertSame( $e->getCode() , 'GIF001' );
    }
    
    fclose( $handle );
  }
}
