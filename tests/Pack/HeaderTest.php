<?php

/*
 * This file is part of the loops/gif package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Loops\Gif;

/**
 * Tests for Pack_Header
 *
 * @package    loops/gif
 * @author     Loops <pierrotevrard@gmail.com>
 */
class Pack_HeaderTest extends \PHPUnit_Framework_TestCase
{
  /*
   * Test case 1: Unpack
   */
  public function test1()
  {
    // GIF version 87a Header
    $handle = tmpfile();
    fwrite( $handle , 'GIF87a' );
    fseek( $handle , 0 );
    
    $pack = Pack_Factory::instance( 'Header' );
    $pack->unpack( $handle );
    
    fclose( $handle );
    
    $this->assertSame( $pack->getData('Signature') , 'GIF' );
    $this->assertSame( $pack->getData('Version') , '87a' );
    
    // GIF version 89a Header
    $handle = tmpfile();
    fwrite( $handle , 'GIF89a' );
    fseek( $handle , 0 );
    
    $pack = Pack_Factory::instance( 'Header' );
    $pack->unpack( $handle );
    
    fclose( $handle );
    
    $this->assertSame( $pack->getData('Signature') , 'GIF' );
    $this->assertSame( $pack->getData('Version') , '89a' );
  }
  
  /*
   * Test case 2: Pack
   */
  public function test2()
  {
    // GIF version 87a Header
    $handle = tmpfile();
    
    $pack = Pack_Factory::instance( 'Header' );
    $pack->setData('Signature' , 'GIF');
    $pack->setData('Version' , '87a');
    $pack->pack( $handle );
    
    fseek( $handle , 0 );
    $this->assertSame( fread( $handle , 1024 ) , 'GIF87a' );
    
    fclose( $handle );
    
    // GIF version 89a Header
    $handle = tmpfile();
    
    $pack = Pack_Factory::instance( 'Header' );
    $pack->setData('Signature' , 'GIF');
    $pack->setData('Version' , '89a');
    $pack->pack( $handle );
    
    fseek( $handle , 0 );
    $this->assertSame( fread( $handle , 1024 ) , 'GIF89a' );
    
    fclose( $handle );
  }
  
  
  /*
   * Test case 3: Invalid
   */
  public function test3()
  {
    // Too short
    $handle = tmpfile();
    fwrite( $handle , 'xxx' ); // Header requires 6 bytes
    fseek( $handle , 0 );
    
    try
    {
      $pack = Pack_Factory::instance( 'Header' );
      $pack->unpack( $handle );
      
      throw new \PHPUnit_Framework_UnintentionallyCoveredCodeError( 'An exception must be thrown' );
    }
    catch( Exception $e )
    {
      $this->assertSame( $e->getCode() , 'GIF001' );
    }
    
    fclose( $handle );
    
    // Invalid Signature
    $handle = tmpfile();
    fwrite( $handle , 'Gif89a' );
    fseek( $handle , 0 );
    
    try
    {
      $pack = Pack_Factory::instance( 'Header' );
      $pack->unpack( $handle );
      
      throw new \PHPUnit_Framework_UnintentionallyCoveredCodeError( 'An exception must be thrown' );
    }
    catch( Exception $e )
    {
      $this->assertSame( $e->getCode() , 'GIF006' );
    }
    
    fclose( $handle );
    
    // Invalid Version
    $handle = tmpfile();
    fwrite( $handle , 'GIF88a' );
    fseek( $handle , 0 );
    
    try
    {
      $pack = Pack_Factory::instance( 'Header' );
      $pack->unpack( $handle );
      
      throw new \PHPUnit_Framework_UnintentionallyCoveredCodeError( 'An exception must be thrown' );
    }
    catch( Exception $e )
    {
      $this->assertSame( $e->getCode() , 'GIF007' );
    }
    
    fclose( $handle );
  }
}
