<?php

/*
 * This file is part of the loops/gif package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Loops\Gif;

/**
 * Tests for Pack_LogicalScreenDescriptor
 *
 * @package    loops/gif
 * @author     Loops <pierrotevrard@gmail.com>
 */
class Pack_LogicalScreenDescriptorTest extends \PHPUnit_Framework_TestCase
{
  /*
   * Test case 1: Unpack
   */
  public function test1()
  {
    $handle = tmpfile();
    fwrite( $handle , pack( 'vvCCC' 
      , 200 // Logical Screen Width, 2 bytes
      , 250 // Logical Screen Height, 2 bytes

      , // <Packed Fields>, 1 byte
          ( 0b1 << 7 ) // Global Color Table Flag, bit OXXXXXXX
        + ( 0b111 << 4 ) // Color Resolution, bits XOOOXXXX
        + ( 0b0 << 3 ) // Sort Flag, bit XXXXOXXX
        + ( 0b10 << 0 ) // Size of Global Color Table, bits XXXXXOOO

      , 7 // Background Color Index, 1 byte
      , 0 // Pixel Aspect Ratio, 1 byte
    ) );
    fseek( $handle , 0 );
    
    $pack = Pack_Factory::instance( 'Logical Screen Descriptor' );
    $pack->unpack( $handle );
    
    fclose( $handle );
    
    $this->assertSame( $pack->getData('Logical Screen Width') , 200 );
    $this->assertSame( $pack->getData('Logical Screen Height') , 250 );
    $this->assertSame( $pack->getData('Global Color Table Flag') , 0b1 );
    $this->assertSame( $pack->getData('Color Resolution') , 0b111 );
    $this->assertSame( $pack->getData('Sort Flag') , 0b0 );
    $this->assertSame( $pack->getData('Size of Global Color Table') , 0b10 );
    $this->assertSame( $pack->getData('Background Color Index') , 7 );
    $this->assertSame( $pack->getData('Pixel Aspect Ratio') , 0 );
  }
  
  /*
   * Test case 2: Pack
   */
  public function test2()
  {
    $handle = tmpfile();
    
    $pack = Pack_Factory::instance( 'Logical Screen Descriptor' );
    $pack->setData( 'Logical Screen Width' , 400 );
    $pack->setData( 'Logical Screen Height' , 200 );
    $pack->setData( 'Global Color Table Flag' , 0b1 );
    $pack->setData( 'Color Resolution' , 0b11 );
    $pack->setData( 'Sort Flag' , 0b1 );
    $pack->setData( 'Size of Global Color Table' , 0b111 );
    $pack->setData( 'Background Color Index' , 255 );
    $pack->setData( 'Pixel Aspect Ratio' , 1 );
    $pack->pack( $handle );
    
    fseek( $handle , 0 );
    $this->assertSame( fread( $handle , 1024 ) , pack( 'vvCCC' 
      , 400 // Logical Screen Width, 2 bytes
      , 200 // Logical Screen Height, 2 bytes

      , // <Packed Fields>, 1 byte
          ( 0b1 << 7 ) // Global Color Table Flag, bit OXXXXXXX
        + ( 0b11 << 4 ) // Color Resolution, bits XOOOXXXX
        + ( 0b1 << 3 ) // Sort Flag, bit XXXXOXXX
        + ( 0b111 << 0 ) // Size of Global Color Table, bits XXXXXOOO

      , 255 // Background Color Index, 1 byte
      , 1 // Pixel Aspect Ratio, 1 byte
    ) );
    
    fclose( $handle );
  }
  
  
  /*
   * Test case 3: Invalid
   */
  public function test3()
  {
    // Too short
    $handle = tmpfile();
    fwrite( $handle , 'xxxxx' ); // Logical Screen Descriptor requires 7 bytes
    fseek( $handle , 0 );
    
    try
    {
      $pack = Pack_Factory::instance( 'Logical Screen Descriptor' );
      $pack->unpack( $handle );
      
      throw new \PHPUnit_Framework_UnintentionallyCoveredCodeError( 'An exception must be thrown' );
    }
    catch( Exception $e )
    {
      $this->assertSame( $e->getCode() , 'GIF001' );
    }
    
    fclose( $handle );
  }
}
