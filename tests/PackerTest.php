<?php

/*
 * This file is part of the loops/gif package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Loops\Gif;

/**
 * Tests for Packer
 *
 * @package    loops/gif
 * @author     Loops <pierrotevrard@gmail.com>
 */
class PackerTest extends \PHPUnit_Framework_TestCase
{
  /*
   * Test case 1: Pack GIF generated on the fly
   */
  public function test1()
  {
    $packs = array();
    
    // Header
    $packs[] = Pack_Factory::instance( 'Header' , array(
      'Signature' => 'GIF' ,
      'Version' => '89a' ,
    ) );
    
    // Logical Screen Descriptor
    $packs[] = Pack_Factory::instance( 'Logical Screen Descriptor' , array(
      'Logical Screen Width' => 400 ,
      'Logical Screen Height' => 200 ,
      'Global Color Table Flag' => 0b1 ,
      'Color Resolution' => 0b11 ,
      'Sort Flag' => 0b1 ,
      'Size of Global Color Table' => 0b1 ,
      'Background Color Index' => 255 ,
      'Pixel Aspect Ratio' => 1 ,
    ) );
    
    // Global Color Table
    // Size of Global Color Table: 0b1, means 4 colors (2 << size)
    $packs[] = Pack_Factory::instance( 'Global Color Table' , array(
      'Color Table' => array(
        array( 'Red' => 255 , 'Green' => 0 , 'Blue' => 0 ),
        array( 'Red' => 0 , 'Green' => 255 , 'Blue' => 0 ),
        array( 'Red' => 0 , 'Green' => 0 , 'Blue' => 255 ),
        array( 'Red' => 255 , 'Green' => 255 , 'Blue' => 255 ),
      ) , 
    ) );
    
    // Graphic Control Extension
    $packs[] = Pack_Factory::instance( 'Graphic Control Extension' , array(
      'Reserved' => 0b0 ,
      'Disposal Method' => 0b111 ,
      'User Input Flag' => 0b0 ,
      'Transparent Color Flag' => 0b1 ,
      'Delay Time' => 60 ,
      'Transparent Color Index' => 3 ,
    ) );
    
    // Image Descriptor
    $packs[] = Pack_Factory::instance( 'Image Descriptor' , array(
      'Image Left Position' => 40 ,
      'Image Top Position' => 20 ,
      'Image Width' => 400 ,
      'Image Height' => 200 ,
      'Local Color Table Flag' => 0b1 ,
      'Interlace Flag' => 0b0 ,
      'Sort Flag' => 0b1 ,
      'Reserved' => 0b01 ,
      'Size of Local Color Table' => 0b1 ,
    ) );
    
    // Local Color Table
    // Size of Local Color Table: 0b1, means 4 colors (2 << size)
    $packs[] = Pack_Factory::instance( 'Local Color Table' , array(
      'Color Table' => array(
        array( 'Red' => 255 , 'Green' => 0 , 'Blue' => 0 ),
        array( 'Red' => 0 , 'Green' => 255 , 'Blue' => 0 ),
        array( 'Red' => 0 , 'Green' => 0 , 'Blue' => 255 ),
        array( 'Red' => 255 , 'Green' => 255 , 'Blue' => 255 ),
      ) ,
    ) );
    
    // Table Based Image Data
    $packs[] = Pack_Factory::instance( 'Table Based Image Data' , array(
      'LZW Minimum Code Size' => 64 ,
      'Image Data' => str_repeat( 'x' , 255 ).str_repeat( 'x' , 127 ) ,
    ) );
    
    // Application Extension
    $packs[] = Pack_Factory::instance( 'Application Extension' , array(
      'Application Identifier' => 'xxxxxxxx' ,
      'Application Authentication Code' => '042' ,
      'Application Data' => str_repeat( 'x' , 255 ).str_repeat( 'x' , 50 ) ,
    ) );
    
    // Comment Extension
    $packs[] = Pack_Factory::instance( 'Comment Extension' , array(
      'Comment Data' => str_repeat( 'x' , 255 ).str_repeat( 'x' , 50 ) ,
    ) );
    
    // Graphic Control Extension (2nd occurrence)
    $packs[] = Pack_Factory::instance( 'Graphic Control Extension' , array(
      'Reserved' => 0b0 ,
      'Disposal Method' => 0b111 ,
      'User Input Flag' => 0b0 ,
      'Transparent Color Flag' => 0b1 ,
      'Delay Time' => 120 ,
      'Transparent Color Index' => 3 ,
    ) );
    
    // Plain Text Extension
    $packs[] = Pack_Factory::instance( 'Plain Text Extension' , array(
      'Text Grid Left Position' => 30 ,
      'Text Grid Top Position' => 35 ,
      'Text Grid Width' => 300 ,
      'Text Grid Height' => 350 ,
      'Character Cell Width' => 3 ,
      'Character Cell Height' => 6 ,
      'Text Foreground Color Index' => 7 ,
      'Text Background Color Index' => 9 ,
      'Plain Text Data' => str_repeat( 'x' , 255 ).str_repeat( 'x' , 50 ) ,
    ) );
    
    // NETSCAPE2.0 Application Extension
    $packs[] = Pack_Factory::instance( 'NETSCAPE2.0 Application Extension' , array(
      'Application Identifier' => 'NETSCAPE' ,
      'Application Authentication Code' => '2.0' ,
      'Sub-Block Index' => 1 ,
      'Number of Repetitions' => 160 ,
    ) );
    
    // Trailer
    $packs[] = Pack_Factory::instance( 'Trailer' );
    
    // extra useless pack
    $packs[] = Pack_Factory::instance( 'Comment Extension' , array(
      'Comment Data' => str_repeat( 'x' , 255 ).str_repeat( 'x' , 255 ) ,
    ) );
    
    
    // now process check
    $packer = new Packer( $packs );
    
    
    // before to run the process, let's do some POOP check
    
    // first check: packs should be identical
    $this->assertTrue( $packer->_packs === $packs );
    
    // second check: we should be at first position of the stream
    $this->assertTrue( ftell( $packer->__handle ) === 0 );
    
    // third check: positions should be empty
    $this->assertTrue( count( $packer->__positions ) === 0 );
    
    // fourth check: packs pointer should be different
    end( $packs );
    $this->assertTrue( key($packer->_packs) !== key($packs) );
    
    // initialize counter
    $counter = 0;
    
    
    // sounds good
    
    // let do an iteration, but without foreach
    
    
    // Header
    $bin = $packer->current();
    $this->assertSame( $bin , 'GIF89a' );
    
    // go to next pack
    $packer->next();
    // make sure positions counter is ok
    $this->assertTrue( count( $packer->__positions ) === ++$counter );
    
    
    // Logical Screen Descriptor
    $this->assertTrue( $packer->valid() );
    $bin = $packer->current();
    
    $this->assertSame( $bin , pack( 'vvCCC' 
      , 400 // Logical Screen Width, 2 bytes
      , 200 // Logical Screen Height, 2 bytes

      , // <Packed Fields>, 1 byte
          ( 0b1 << 7 ) // Global Color Table Flag, bit OXXXXXXX
        + ( 0b11 << 4 ) // Color Resolution, bits XOOOXXXX
        + ( 0b1 << 3 ) // Sort Flag, bit XXXXOXXX
        + ( 0b1 << 0 ) // Size of Global Color Table, bits XXXXXOOO

      , 255 // Background Color Index, 1 byte
      , 1 // Pixel Aspect Ratio, 1 byte
    ) );
    
    // go to next pack
    $packer->next();
    // make sure positions counter is ok
    $this->assertTrue( count( $packer->__positions ) === ++$counter );
       
    
    // Global Color Table
    $this->assertTrue( $packer->valid() );
    $bin = $packer->current();
    
    $this->assertSame( $bin , 
        pack( 'CCC' , 255 , 0 , 0 )
      . pack( 'CCC' , 0 , 255 , 0 )
      . pack( 'CCC' , 0 , 0 , 255 )
      . pack( 'CCC' , 255 , 255 , 255 )
    );
    
    // go to next pack
    $packer->next();
    // make sure positions counter is ok
    $this->assertTrue( count( $packer->__positions ) === ++$counter );
    
    
    // Graphic Control Extension (1st occurrence)
    $this->assertTrue( $packer->valid() );
    $bin = $packer->current();
    
    $this->assertSame( $bin , pack( 'AACCvCC' 
      , "\x21" // Extension Introducer
      , "\xF9" // Extension Label
      
      , 4 // Block Size, 1 byte

      , // <Packed Fields>, 1 byte
          ( 0b0 << 5 ) // Reserved, bit OOOXXXXX
        + ( 0b111 << 2 ) // Disposal Method, bit XXXOOOXX
        + ( 0b0 << 1 ) // User Input Flag, bit XXXXXXOX
        + ( 0b1 << 0 ) // Transparent Color Flag, bits XXXXXXXO
      
      , 60 // Delay Time, 2 bytes
      , 3 // Transparent Color Index, 1 byte
      
      , 0 // Block Terminator
    ) );
    
    // go to next pack
    $packer->next();
    // make sure positions counter is ok
    $this->assertTrue( count( $packer->__positions ) === ++$counter );
    
    
    // Image Descriptor
    $this->assertTrue( $packer->valid() );
    $bin = $packer->current();
    
    $this->assertSame( $bin , pack( 'AvvvvC'
      , "\x2C" // Image Separator
      
      , 40 // Image Left Position, 2 bytes
      , 20 // Image Top Position, 2 bytes
      , 400 // Image Width, 2 bytes
      , 200 // Image Height, 2 bytes

      , // <Packed Fields>, 1 byte
          ( 0b1 << 7 ) // Local Color Table Flag, bit OXXXXXXX
        + ( 0b0 << 6 ) // Interlace Flag, bit X0XXXXXX
        + ( 0b1 << 5 ) // Sort Flag, bit XXOXXXXX
        + ( 0b01 << 3 ) // Reserved, bits XXXOOXXX
        + ( 0b1 << 0 ) // Size of Local Color Table, bits XXXXXOOO
    ) );
    
    // go to next pack
    $packer->next();
    // make sure positions counter is ok
    $this->assertTrue( count( $packer->__positions ) === ++$counter );
    
    
    // Local Color Table
    // Size of Local Color Table: 0b1, means 4 colors (2 << size)
    $this->assertTrue( $packer->valid() );
    $bin = $packer->current();
    
    $this->assertSame( $bin , 
        pack( 'CCC' , 255 , 0 , 0 )
      . pack( 'CCC' , 0 , 255 , 0 )
      . pack( 'CCC' , 0 , 0 , 255 )
      . pack( 'CCC' , 255 , 255 , 255 )
    );
    
    // go to next pack
    $packer->next();
    // make sure positions counter is ok
    $this->assertTrue( count( $packer->__positions ) === ++$counter );
    
    
    // Table Based Image Data
    $this->assertTrue( $packer->valid() );
    $bin = $packer->current();
    
    $this->assertSame( $bin , 
        pack( 'C' , 64 ) // LZW Minimum Code Size
      . pack( 'CA255' , 255 , str_repeat( 'x' , 255 ) ) // Data Sub-Block
      . pack( 'CA127' , 127 , str_repeat( 'x' , 127 ) ) // Data Sub-Block
      . pack( 'C' , 0 ) // Block terminator
    );
    
    // go to next pack
    $packer->next();
    // make sure positions counter is ok
    $this->assertTrue( count( $packer->__positions ) === ++$counter );

    
    // Application Extension
    $this->assertTrue( $packer->valid() );
    $bin = $packer->current();
    
    $this->assertSame( $bin , 
        pack( 'AACA8A3' 
          , "\x21" // Extension Introducer
          , "\xFF" // Extension Label

          , 11 // Block Size, 1 byte

          , 'xxxxxxxx' // Application Identifier, 8 bytes
          , '042' // Application Authentication Code, 3 bytes
        )
      . pack( 'CA255' , 255 , str_repeat( 'x' , 255 ) ) // Data Sub-Block
      . pack( 'CA50' , 50 , str_repeat( 'x' , 50 ) ) // Data Sub-Block
      . pack( 'C' , 0 ) // Block Terminator
    );
    
    // go to next pack
    $packer->next();
    // make sure positions counter is ok
    $this->assertTrue( count( $packer->__positions ) === ++$counter );

    
    // Comment Extension
    $this->assertTrue( $packer->valid() );
    $bin = $packer->current();
    
    $this->assertSame( $bin , 
        pack( 'AA' 
          , "\x21" // Extension Introducer
          , "\xFE" // Extension Label
        )
      . pack( 'CA255' , 255 , str_repeat( 'x' , 255 ) )
      . pack( 'CA50' , 50 , str_repeat( 'x' , 50 ) )
      . pack( 'C' , 0 )
    );
    
    // go to next pack
    $packer->next();
    // make sure positions counter is ok
    $this->assertTrue( count( $packer->__positions ) === ++$counter );

    
    // Graphic Control Extension (2nd occurrence)
    $this->assertTrue( $packer->valid() );
    $bin = $packer->current();
    
    $this->assertSame( $bin , pack( 'AACCvCC' 
      , "\x21" // Extension Introducer
      , "\xF9" // Extension Label
      
      , 4 // Block Size, 1 byte

      , // <Packed Fields>, 1 byte
          ( 0b0 << 5 ) // Reserved, bit OOOXXXXX
        + ( 0b111 << 2 ) // Disposal Method, bit XXXOOOXX
        + ( 0b0 << 1 ) // User Input Flag, bit XXXXXXOX
        + ( 0b1 << 0 ) // Transparent Color Flag, bits XXXXXXXO
      
      , 120 // Delay Time, 2 bytes
      , 3 // Transparent Color Index, 1 byte
      
      , 0 // Block Terminator
    ) );
    
    // go to next pack
    $packer->next();
    // make sure positions counter is ok
    $this->assertTrue( count( $packer->__positions ) === ++$counter );

        
    // Plain Text Extension
    $this->assertTrue( $packer->valid() );
    $bin = $packer->current();
    
    $this->assertSame( $bin , 
        pack( 'AACvvvvCCCC' 
          , "\x21" // Extension Introducer
          , "\x01" // Extension Label

          , 12 // Block Size, 1 byte

          , 30 // Text Grid Left Position, 2 bytes
          , 35 // Text Grid Top Position, 2 bytes
          , 300 // Text Grid Width, 2 bytes
          , 350 // Text Grid Height, 2 bytes
          , 3 // Character Cell Width, 1 byte
          , 6 // Character Cell Height, 1 byte
          , 7 // Text Foreground Color Index, 1 byte
          , 9 // Text Background Color Index, 1 byte
        )
      . pack( 'CA255' , 255 , str_repeat( 'x' , 255 ) )
      . pack( 'CA50' , 50 , str_repeat( 'x' , 50 ) )
      . pack( 'C' , 0 )
    );
    
    // go to next pack
    $packer->next();
    // make sure positions counter is ok
    $this->assertTrue( count( $packer->__positions ) === ++$counter );

    
    // NETSCAPE2.0 Application Extension
    $this->assertTrue( $packer->valid() );
    $bin = $packer->current();
    
    $this->assertSame( $bin , 
      pack( 'AACA8A3CCvC' 
        , "\x21" // Extension Introducer
        , "\xFF" // Extension Label

        , 11 // Block Size, 1 byte

        , 'NETSCAPE' // Application Identifier, 8 bytes
        , '2.0' // Application Authentication Code, 3 bytes

        , 3 // Block Size, 1 byte

        , 1 // Sub-Block Index, 1 byte
        , 160 // Number of Repetitions, 2 bytes

        , 0 // Block Terminator
      )
    );
    
    // go to next pack
    $packer->next();
    // make sure positions counter is ok
    $this->assertTrue( count( $packer->__positions ) === ++$counter );

    
    // Trailer
    $this->assertTrue( $packer->valid() );
    $bin = $packer->current();
    
    $this->assertSame( $bin , "\x3B" );
    
    // go to next pack
    $packer->next();
    // make sure positions counter is ok
    $this->assertTrue( count( $packer->__positions ) === ++$counter );
   
    
    // finally, this one should be false
    $this->assertFalse( $packer->valid() );
    
    
    // after the process, let's do some POOP check on extra useless pack
    
    // first check: the last pack should not have been proceeded
    $this->assertTrue( count( $packer->__positions ) === count($packs) - 1 );
    
  }
  
  /**
   * Test 2: Missing pack failures
   */
  public function test2()
  {
    $allpacks = array();
    
    // Header
    $allpacks[] = Pack_Factory::instance( 'Header' , array(
      'Signature' => 'GIF' ,
      'Version' => '89a' ,
    ) );
    
    // Logical Screen Descriptor
    $allpacks[] = Pack_Factory::instance( 'Logical Screen Descriptor' , array(
      'Logical Screen Width' => 400 ,
      'Logical Screen Height' => 200 ,
      'Global Color Table Flag' => 0b1 ,
      'Color Resolution' => 0b11 ,
      'Sort Flag' => 0b1 ,
      'Size of Global Color Table' => 0b1 ,
      'Background Color Index' => 255 ,
      'Pixel Aspect Ratio' => 1 ,
    ) );
    
    // Global Color Table
    // Size of Global Color Table: 0b1, means 4 colors (2 << size)
    $allpacks[] = Pack_Factory::instance( 'Global Color Table' , array(
      'Color Table' => array(
        array( 'Red' => 255 , 'Green' => 0 , 'Blue' => 0 ),
        array( 'Red' => 0 , 'Green' => 255 , 'Blue' => 0 ),
        array( 'Red' => 0 , 'Green' => 0 , 'Blue' => 255 ),
        array( 'Red' => 255 , 'Green' => 255 , 'Blue' => 255 ),
      ) , 
    ) );
    
    // Graphic Control Extension
    $allpacks[] = Pack_Factory::instance( 'Graphic Control Extension' , array(
      'Reserved' => 0b0 ,
      'Disposal Method' => 0b111 ,
      'User Input Flag' => 0b0 ,
      'Transparent Color Flag' => 0b1 ,
      'Delay Time' => 60 ,
      'Transparent Color Index' => 3 ,
    ) );
    
    // Image Descriptor
    $allpacks[] = Pack_Factory::instance( 'Image Descriptor' , array(
      'Image Left Position' => 40 ,
      'Image Top Position' => 20 ,
      'Image Width' => 400 ,
      'Image Height' => 200 ,
      'Local Color Table Flag' => 0b1 ,
      'Interlace Flag' => 0b0 ,
      'Sort Flag' => 0b1 ,
      'Reserved' => 0b01 ,
      'Size of Local Color Table' => 0b1 ,
    ) );
    
    // Local Color Table
    // Size of Local Color Table: 0b1, means 4 colors (2 << size)
    $allpacks[] = Pack_Factory::instance( 'Local Color Table' , array(
      'Color Table' => array(
        array( 'Red' => 255 , 'Green' => 0 , 'Blue' => 0 ),
        array( 'Red' => 0 , 'Green' => 255 , 'Blue' => 0 ),
        array( 'Red' => 0 , 'Green' => 0 , 'Blue' => 255 ),
        array( 'Red' => 255 , 'Green' => 255 , 'Blue' => 255 ),
      ) ,
    ) );
    
    // Table Based Image Data
    $allpacks[] = Pack_Factory::instance( 'Table Based Image Data' , array(
      'LZW Minimum Code Size' => 64 ,
      'Image Data' => str_repeat( 'x' , 255 ).str_repeat( 'x' , 127 ) ,
    ) );
    
    // Trailer
    $allpacks[] = Pack_Factory::instance( 'Trailer' );
    
    
    // remove Header
    $packs = $allpacks;
    unset( $packs[0] );
    
    try
    {
      // create packer
      $packer = new Packer( $packs );
      // this should failed
      $packer();
      
      throw new \PHPUnit_Framework_UnintentionallyCoveredCodeError( 'An exception must be thrown' );
    }
    catch( Exception $e )
    {
      $this->assertSame( $e->getCode() , 'GIF061' );
    }
    
    
    // remove Logical Screen Descriptor
    $packs = $allpacks;
    unset( $packs[1] );
    
    try
    {
      // create packer
      $packer = new Packer( $packs );
      // this should failed
      $packer();
      
      throw new \PHPUnit_Framework_UnintentionallyCoveredCodeError( 'An exception must be thrown' );
    }
    catch( Exception $e )
    {
      $this->assertSame( $e->getCode() , 'GIF061' );
    }
    
    
    // remove Global Color Table
    $packs = $allpacks;
    unset( $packs[2] );
    
    try
    {
      // create packer
      $packer = new Packer( $packs );
      // this should failed
      $packer();
      
      throw new \PHPUnit_Framework_UnintentionallyCoveredCodeError( 'An exception must be thrown' );
    }
    catch( Exception $e )
    {
      $this->assertSame( $e->getCode() , 'GIF061' );
    }
    
    
    // remove Local Color Table
    $packs = $allpacks;
    unset( $packs[5] );
    
    // create packer
    $packer = new Packer( $packs );
    
    try
    {
      // create packer
      $packer = new Packer( $packs );
      // this should failed
      $packer();
      
      throw new \PHPUnit_Framework_UnintentionallyCoveredCodeError( 'An exception must be thrown' );
    }
    catch( Exception $e )
    {
      $this->assertSame( $e->getCode() , 'GIF061' );
    }
    
    
    // remove Table Based Image Data
    $packs = $allpacks;
    unset( $packs[6] );
    
    try
    {
      // create packer
      $packer = new Packer( $packs );
      // this should failed
      $packer();
      
      throw new \PHPUnit_Framework_UnintentionallyCoveredCodeError( 'An exception must be thrown' );
    }
    catch( Exception $e )
    {
      $this->assertSame( $e->getCode() , 'GIF061' );
    }
    
  }
  
  /**
   * Test 3: Invalid pack order failures
   */
  public function test3()
  {
    $allpacks = array();
    
    // Header
    $allpacks[] = Pack_Factory::instance( 'Header' , array(
      'Signature' => 'GIF' ,
      'Version' => '89a' ,
    ) );
    
    // Logical Screen Descriptor
    $allpacks[] = Pack_Factory::instance( 'Logical Screen Descriptor' , array(
      'Logical Screen Width' => 400 ,
      'Logical Screen Height' => 200 ,
      'Global Color Table Flag' => 0b1 ,
      'Color Resolution' => 0b11 ,
      'Sort Flag' => 0b1 ,
      'Size of Global Color Table' => 0b1 ,
      'Background Color Index' => 255 ,
      'Pixel Aspect Ratio' => 1 ,
    ) );
    
    // Global Color Table
    // Size of Global Color Table: 0b1, means 4 colors (2 << size)
    $allpacks[] = Pack_Factory::instance( 'Global Color Table' , array(
      'Color Table' => array(
        array( 'Red' => 255 , 'Green' => 0 , 'Blue' => 0 ),
        array( 'Red' => 0 , 'Green' => 255 , 'Blue' => 0 ),
        array( 'Red' => 0 , 'Green' => 0 , 'Blue' => 255 ),
        array( 'Red' => 255 , 'Green' => 255 , 'Blue' => 255 ),
      ) , 
    ) );
    
    // Graphic Control Extension
    $allpacks[] = Pack_Factory::instance( 'Graphic Control Extension' , array(
      'Reserved' => 0b0 ,
      'Disposal Method' => 0b111 ,
      'User Input Flag' => 0b0 ,
      'Transparent Color Flag' => 0b1 ,
      'Delay Time' => 60 ,
      'Transparent Color Index' => 3 ,
    ) );
    
    // Image Descriptor
    $allpacks[] = Pack_Factory::instance( 'Image Descriptor' , array(
      'Image Left Position' => 40 ,
      'Image Top Position' => 20 ,
      'Image Width' => 400 ,
      'Image Height' => 200 ,
      'Local Color Table Flag' => 0b1 ,
      'Interlace Flag' => 0b0 ,
      'Sort Flag' => 0b1 ,
      'Reserved' => 0b01 ,
      'Size of Local Color Table' => 0b1 ,
    ) );
    
    // Local Color Table
    // Size of Local Color Table: 0b1, means 4 colors (2 << size)
    $allpacks[] = Pack_Factory::instance( 'Local Color Table' , array(
      'Color Table' => array(
        array( 'Red' => 255 , 'Green' => 0 , 'Blue' => 0 ),
        array( 'Red' => 0 , 'Green' => 255 , 'Blue' => 0 ),
        array( 'Red' => 0 , 'Green' => 0 , 'Blue' => 255 ),
        array( 'Red' => 255 , 'Green' => 255 , 'Blue' => 255 ),
      ) ,
    ) );
    
    // Table Based Image Data
    $allpacks[] = Pack_Factory::instance( 'Table Based Image Data' , array(
      'LZW Minimum Code Size' => 64 ,
      'Image Data' => str_repeat( 'x' , 255 ).str_repeat( 'x' , 127 ) ,
    ) );
    
    // Trailer
    $allpacks[] = Pack_Factory::instance( 'Trailer' );
    
    
    // inject Header
    $packs = $allpacks;
    array_splice( $packs , 3 , 0 , array( clone $packs[0] ) );
    
    try
    {
      // create packer
      $packer = new Packer( $packs );
      // this should failed
      $packer();
      
      throw new \PHPUnit_Framework_UnintentionallyCoveredCodeError( 'An exception must be thrown' );
    }
    catch( Exception $e )
    {
      $this->assertSame( $e->getCode() , 'GIF062' );
    }
    
    
    // inject Local Color Table
    $packs = $allpacks;
    array_splice( $packs , 3 , 0 , array( clone $packs[5] ) );
    
    try
    {
      // create packer
      $packer = new Packer( $packs );
      // this should failed
      $packer();
      
      throw new \PHPUnit_Framework_UnintentionallyCoveredCodeError( 'An exception must be thrown' );
    }
    catch( Exception $e )
    {
      $this->assertSame( $e->getCode() , 'GIF062' );
    }
    
  }
}