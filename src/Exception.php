<?php

/*
 * This file is part of the loops/gif package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Loops\Gif;

use \Loops\Exception\Base;

/**
 * Exception class for loops/gif package.
 *
 * @package    loops/gif
 * @author     Loops <pierrotevrard@gmail.com>
 */
class Exception extends Base
{
  /**
   * Array of exceptions messages by code.
   * Range of message are:
   *  - from GIF000 to GIF049: Pack exceptions
   *  - from GIF050 to GIF099: Unpacker/Packer exceptions
   * 
   * @var array
   * @access protected
   */
  public $_messages = array(
    // From 6000 to 6049: Pack exceptions
    // Pack
    'GIF001' => '%s: Unexpected end-of-file (%d bytes expected, %d found).' ,
    'GIF002' => '%s: Missing data "%s".' ,
    // Header
    'GIF006' => '%s: Invalid Header Signature "%s" ("%s" expected).' ,
    'GIF007' => '%s: Invalid Header Version "%s" ("%s" expected).' ,
    // Image Descriptor
    'GIF010' => '%s: Invalid Image Separator "\\x%02X" ("\\x%02X" expected).' ,
    // Color Table
    'GIF015' => '%s: Size of Color Table must be set.' ,
    // Trailer
    'GIF018' => '%s: Invalid GIF Trailer "\\x%02X" ("\\x%02X" expected).' ,
    // common Extension
    'GIF020' => '%s: Invalid Extension Introducer "\\x%02X" ("\\x%02X" expected).' ,
    'GIF021' => '%s: Invalid Extension Label "\\x%02X" ("\\x%02X" expected).' ,
    'GIF022' => '%s: Invalid Block Size "\\x%02X" ("\\x%02X" expected).' ,
    'GIF023' => '%s: Invalid Block Terminator "\\x%02X" ("\\x%02X" expected).' ,
    // Application Extension
    'GIF030' => '%s: Invalid Application Identifier "%s" ("%s" expected).' ,
    'GIF031' => '%s: Invalid Application Authentication Code "%s" ("%s" expected).' ,
    'GIF032' => '%s: Invalid Sub-Block Index %d (%d expected).' ,
    // Unknown Extension
    'GIF035' => '%s: Extension Label must be set.' ,
    // Factory 
    'GIF040' => '%s: Unknown pack name "%s".' ,
    'GIF041' => '%s: %s must implements %s.' ,
    // Unpacker 
    'GIF050' => '%s: Unable to create stream from "%s".' ,
    // Packer
    'GIF060' => '%s: %s must implements %s.' ,
    'GIF061' => '%s: Invalid pack name "%s" ("%s" expected, position: %d).' ,
    'GIF062' => '%s: Pack name "%s" must not occurs after "%s" (position: %d).' ,
  );
}
