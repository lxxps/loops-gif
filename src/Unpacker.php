<?php

/*
 * This file is part of the loops/gif package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Loops\Gif;

/**
 * Buffer size to copy stream.
 */
if( ! Config::hasUnpackerStreamBufferLength() )
{
  // initialize this setting from memory limit setting
  Config::defineUnpackerStreamBufferLength( call_user_func( function( $ini ) {

    $v = (int)$ini;
    // make sure it is not -1 or 0 (?!?)
    if( $v < 1 ) return 16777216; // consider 16M

    // if not, do it classic
    switch( substr( $ini , -1 ) )
    {
      case 'M': case 'm': return $v * 1048576;
      case 'K': case 'k': return $v * 1024;
      case 'G': case 'g': return $v * 1073741824;
      default: return $ini;
    }

  } , ini_get('memory_limit') ) );
}

/**
 * Class to unpack GIF and especially animated GIF (AGIF).
 * 
 * Note that specifications use term Block when we use term Pack.
 * 
 * For convenience, most of keys' name are the same than GIF specifications.
 * 
 * So, a strong knowledge of GIF specifications is required to understand
 * and exploit the resulting packs that the class generates.
 * 
 * This class follow POOP pattern, for further information see POOP file.
 *
 * @package    loops/gif
 * @author     Loops <pierrotevrard@gmail.com>
 * @implements \Iterator
 */
class Unpacker implements \Iterator
{
  
  /**
   * Create a Unpacker instance from a file, stream or binary data.
   * In case of stream provided, a safe copy of the stream will be done.
   * 
   * @param mixed $filepath_or_stream_or_binary Filepath or stream or binary data
   * @return \Loops\Gif\Unpacker
   * @access public
   * @throws \Loops\Gif\Exception
   * @static
   */
  static public function instance( $filepath_or_stream_or_binary )
  {
    // create unpacker instance
    return new static( $filepath_or_stream_or_binary );
  }
  
  /**
   * Create a Unpacker instance from a file, stream or binary data, unpack GIF
   * block and return an array of Pack_Interface.
   * In case of stream provided, a safe copy of the stream will be done.
   * 
   * @param mixed $filepath_or_stream_or_binary Filepath or stream or binary data
   * @return array Array of Pack_Interface
   * @access public
   * @throws \Loops\Gif\Exception
   * @static
   */
  static public function unpack( $filepath_or_stream_or_binary )
  {
    // create unpacker instance
    $unpacker = static::instance( $filepath_or_stream_or_binary );
    // invoke it
    return $unpacker();
  }
  
  /**
   * Intermediate file handle
   * 
   * Note that this handle must be able to read forward and backward, so we 
   * never use directly the stream provided, if any.
   * 
   * We using internal pointer of this stream over Iterator 
   * implementation, so be careful, please.
   * 
   * @var resource
   * @access protected
   */
  public $__handle;
  
  /**
   * Packs array
   * 
   * Populated on demand
   * 
   * We using internal pointer of this array over Iterator 
   * implementation, so be careful, please.
   * 
   * @var array
   * @access protected
   */
  public $__packs = array();
  
  /**
   * On construction, we want to create a temporary file handle
   * 
   * @param mixed $mix Filepath or stream or binary
   * @return void
   * @access public
   */
  public function __construct( $mix )
  {    
    // file path, create a source stream
    if( is_string( $mix ) && ( $src = @fopen( $mix , 'rb' ) ) )
    {
      // create an handle from this file
      $this->__handle = tmpfile();
      
      while( ! feof( $src ) )
      {
        stream_copy_to_stream( $src , $this->__handle , Config::getUnpackerStreamBufferLength() );
      }
      
      // reset pointer
      rewind( $this->__handle );
      
      // close source stream
      fclose( $src );
    }
    
    // binary
    elseif( is_string( $mix ) )
    {
      // create an handle from these data
      $this->__handle = tmpfile();
      
      // write data
      fwrite( $this->__handle , $mix );
      
      // reset pointer
      rewind( $this->__handle );
    }
    
    // stream
    elseif( is_resource( $mix ) && get_resource_type( $mix ) === 'stream' )
    {
      // we can copy the handle to a temporary file
      // just to be sure we can seek the stream
      $this->__handle = tmpfile();
      
      // register current stream position to restore it
      $ppp = ftell( $mix );
      
      // if the stream is not seekable, we may encounter issues
      if( ! rewind( $mix ) )
      {
        // the stream may be unusable and will not be reusable
        // trigger_error( sprintf( '%s::%s() Stream is not seekable in %s at line %d' , __CLASS__ , __METHOD__ , __FILE__ , __LINE__ ) , E_USER_WARNING );
        trigger_error( sprintf( 'Stream <%s> is not seekable' , (string)$mix ) , E_USER_WARNING );
      }
      
      while( ! feof( $mix ) )
      {
        stream_copy_to_stream( $mix , $this->__handle , Config::getUnpackerStreamBufferLength() );
      }
      
      // restore pointer, do not care if it fails
      fseek( $mix , $ppp , \SEEK_SET );
      
      // reset pointer
      rewind( $this->__handle );
    }
    
    if( ! $this->__handle )
    {
      // unable to create stream
      throw new Exception( array( get_class( $this ) , substr( (string)$mix , 0 , 50 ) ) , 'GIF050' );
    }
  }
  
  /**
   * On destructor, we want to remove the temporary file handle
   * 
   * @param none
   * @return void
   * @access public
   */
  public function __destruct()
  {    
    fclose( $this->__handle );
  }
  
  /**
   * Return packs
   * 
   * @param none
   * @return array Packs
   * @access public
   */
  public function getPacks()
  {    
    return $this->__invoke();
  }
  
  /**
   * If expected, the class can process the entire stream to return an 
   * array of GIF packs
   * 
   * @param none
   * @return array Packs
   * @access public
   */
  public function __invoke()
  {    
    foreach( $this as $pack );// nothing to do
    
    return $this->__packs;
  }
  
  /**
   * Process next pack
   * 
   * Return true on success
   * 
   * @param none
   * @return boolean
   * @access protected
   * @throws \Loops\Gif\Exception
   */
  public function _run()
  {
    // not any pack for now
    if( ! count($this->__packs) )
    {
      // we must have an header
      $pack = Pack_Factory::instance('Header');
      $pack->unpack( $this->__handle ); // unpack and validate
      $this->__packs[] = $pack;
      
      // ok
      return true;
    }
    
    // look for last registered pack
    $prev = $this->__packs[count($this->__packs) - 1];
    
    if( $prev->getName() === 'Trailer' )
    {
      // nothing else to read
      // note that some data may be present after the trailer, but we do not care about them
      return false;
    }
    
    if( $prev->getName() === 'Header' )
    {
      // we must have a Logical Screen Descriptor
      $pack = Pack_Factory::instance('Logical Screen Descriptor');
      $pack->unpack( $this->__handle ); // unpack and validate
      $this->__packs[] = $pack;
      
      // ok
      return true;
    }
    
    if( $prev->getName() === 'Logical Screen Descriptor' )
    {
      // we may have a Global Color Table
      if( $prev->getData( 'Global Color Table Flag' ) )
      {
        $pack = Pack_Factory::instance('Global Color Table');
        // set color table size
        $pack->setColorTableSize( $prev->getData( 'Size of Global Color Table' ) );
        $pack->unpack( $this->__handle ); // unpack and validate
        $this->__packs[] = $pack;

        // ok
        return true;
      }
      
      // in other case, continue
    }
    
    if( $prev->getName() === 'Image Descriptor' )
    {
      // we may have a Local Color Table
      if( $prev->getData( 'Local Color Table Flag' ) )
      {
        $pack = Pack_Factory::instance('Local Color Table');
        // set color table size
        $pack->setColorTableSize( $prev->getData( 'Size of Local Color Table' ) );
        $pack->unpack( $this->__handle ); // unpack and validate
        $this->__packs[] = $pack;

        // ok
        return true;
      }
      
      // if not, we must have a Table Based Image Data
      $pack = Pack_Factory::instance('Table Based Image Data');
      $pack->unpack( $this->__handle ); // unpack and validate
      $this->__packs[] = $pack;

      // ok
      return true;
    }
    
    if( $prev->getName() === 'Local Color Table' )
    {
      // we must have a TableBasedImageData
      $pack = Pack_Factory::instance('Table Based Image Data');
      $pack->unpack( $this->__handle ); // unpack and validate
      $this->__packs[] = $pack;

      // ok
      return true;
    }
    
    // at this point, the next byte can determine if we have an image separator
    // or an extension introducer
    
    $byte1 = fgetc( $this->__handle );
    
    if( $byte1 === Pack_ImageDescriptor::IMAGE_SEPARATOR )
    {
      // before to read the Image Descriptor, rewind 1 byte
      fseek( $this->__handle , -1 , \SEEK_CUR );
      
      // we must have a Image Descriptor
      $pack = Pack_Factory::instance('Image Descriptor');
      $pack->unpack( $this->__handle ); // unpack and validate
      $this->__packs[] = $pack;

      // ok
      return true;
    }
    
    if( $byte1 === Pack_Trailer::TRAILER )
    {
      // before to read the Trailer, rewind 1 byte
      fseek( $this->__handle , -1 , \SEEK_CUR );
      
      // we must have a Trailer
      $pack = Pack_Factory::instance('Trailer');
      $pack->unpack( $this->__handle ); // unpack and validate
      $this->__packs[] = $pack;

      // ok
      return true;
    }
    
    if( $byte1 === Pack_Extension_Abstract::EXTENSION_INTRODUCER )
    {
      // the next byte will determine which extension to use
      $byte2 = fgetc( $this->__handle );
      
      if( $byte2 === Pack_Extension_Comment::EXTENSION_LABEL )
      {
        // before to read the Comment Extension, rewind 2 bytes
        fseek( $this->__handle , -2 , \SEEK_CUR );

        // we must have a Comment Extension
        $pack = Pack_Factory::instance('Comment Extension');
        $pack->unpack( $this->__handle ); // unpack and validate
        $this->__packs[] = $pack;

        // ok
        return true;
      }
      
      if( $byte2 === Pack_Extension_PlainText::EXTENSION_LABEL )
      {
        // before to read the Plain Text Extension, rewind 2 bytes
        fseek( $this->__handle , -2 , \SEEK_CUR );

        // we must have a Plain Text Extension
        $pack = Pack_Factory::instance('Plain Text Extension');
        $pack->unpack( $this->__handle ); // unpack and validate
        $this->__packs[] = $pack;

        // ok
        return true;
      }
      
      if( $byte2 === Pack_Extension_GraphicControl::EXTENSION_LABEL )
      {
        // before to read the Graphic Control Extension, rewind 2 bytes
        fseek( $this->__handle , -2 , \SEEK_CUR );

        // we must have a Graphic Control Extension
        $pack = Pack_Factory::instance('Graphic Control Extension');
        $pack->unpack( $this->__handle ); // unpack and validate
        $this->__packs[] = $pack;

        // ok
        return true;
      }
      
      if( $byte2 === Pack_Extension_Application::EXTENSION_LABEL )
      {
        // we need a specific care of the NETSCAPE 2.0 Application Extension
        // this one has special meaning
        
        // get application name
        
        fgetc( $this->__handle ); // this byte should be 11 (fixed Block Size)
        $app = fread( $this->__handle , 11 ); // this 11 bytes represent the application name
        
        // then we can rewind 14 bytes
        fseek( $this->__handle , -14 , \SEEK_CUR );
        
        try
        {
          // look for a XXXXXXXXXXX Application Extension
          // if this class is unmapped, we will get an Exception with code GIF040
          $pack = Pack_Factory::instance($app.' Application Extension');
          $pack->unpack( $this->__handle ); // unpack and validate
          $this->__packs[] = $pack;
        }
        catch( Exception $e )
        {
          // look for unmapped application code
          if( $e->getCode() !== 'GIF040' )
          {
            // not this exception, rethrow
            throw $e;
          }

          // unmapped, use Application Extension
          
          // we must have a Application Extension
          $pack = Pack_Factory::instance('Application Extension');
          $pack->unpack( $this->__handle ); // unpack and validate
          $this->__packs[] = $pack;
        }
        
        // ok
        return true;
      }
      
      // in other case, consider Unknown Extension
      // note that Unknown Extension are not allowed by default
      // but can be enabled using Config::setPackFactoryMap()
      
      // before to read the Unknown Extension, rewind 2 bytes
      fseek( $this->__handle , -2 , \SEEK_CUR );

      // we have an Unknown Extension
      $pack = Pack_Factory::instance('Unknown Extension');
      $pack->unpack( $this->__handle ); // unpack and validate
      $this->__packs[] = $pack;

      // ok
      return true;
    }
    
    
    // if we reach this line, that's mean that we cannot determine the usage 
    // of the previous byte, so rewind 1 byte
    fseek( $this->__handle , -1 , \SEEK_CUR );
    
    return false;
  }
  
  /**
   * Iterator implementation
   * Return current pack, if any
   * 
   * @inheritdoc
   */
  public function current()
  {
    // if current pointer is out of the box
    if( key( $this->__packs ) === null )
    {
      // attemp to fetch next pack
      $this->_run();
    }
    
    // let this function determines if we have a pack to return
    return current( $this->__packs );
  }
  
  /**
   * Iterator implementation
   * Return current key, if any
   * 
   * @inheritdoc
   */
  public function key()
  {
    // if current pointer is out of the box
    if( key( $this->__packs ) === null )
    {
      // attemp to fetch next pack
      $this->_run();
    }
    
    // let this function determines if we have a key to return
    return key( $this->__packs );
  }
  
  /**
   * Iterator implementation
   * 
   * @inheritdoc
   */
  public function next()
  {
    // if current pointer is out of the box
    if( key( $this->__packs ) === null )
    {
      // attemp to fetch next pack
      $this->_run();
    }
    
    // move internal pointer
    next( $this->__packs );
  }
  
  /**
   * Iterator implementation
   * Reset pointer to first pack
   * 
   * @inheritdoc
   */
  public function rewind()
  {
    // reset internal pointer
    reset( $this->__packs );
  }
  
  /**
   * Iterator implementation
   * If returns false, the foreach() loop will be terminated.
   * 
   * @inheritdoc
   */
  public function valid()
  {
    // if current pointer is out of the box
    if( key( $this->__packs ) === null )
    {
      // attempt to fetch next pack
      $this->_run();
      
      // check again
      return key( $this->__packs ) !== null;
    }
    
    // if not, we have a pack to return
    return true;
  }
  
  /**
   * Return true if the pack is found
   * 
   * Second argument can be submitted to found a specific occurrence of the pack
   * 
   * If the pack does not implements Pack_Trait_Interface_Occurrence, the second 
   * argument is ignored
   * 
   * @param string $pack_name
   * @param [integer] $occ
   * @return boolean
   * @access public
   */
  public function hasPack( $pack_name , $occ = null )
  {
    if( $occ > 0 )
    {
      // do it with counter
      $count = 0; 
      
      foreach( $this as $pack )
      {
        if( $pack->getName() === $pack_name )
        {
          if( $count === $occ )
          {
            return true;
          }
          
          $count++;
        }
      }
      
      // not found
      return false;
    }
    
    // classic way
    foreach( $this as $pack )
    {
      if( $pack->getName() === $pack_name )
      {
        return true;
      }
    }

    // not found
    return false;
  }
  
  /**
   * Return the expected pack, if found
   * 
   * Second argument can be submitted to found a specific occurrence of the pack
   * 
   * If the pack does not implements Pack_Trait_Interface_Occurrence, the second 
   * argument is ignored
   * 
   * @param string $pack_name
   * @param [integer] $occ
   * @return \Loops\Gif\Pack_Abstract
   * @access public
   */
  public function getPack( $pack_name , $occ = null )
  {
    if( $occ > 0 )
    {
      // do it with counter
      $count = 0; 
      
      foreach( $this as $pack )
      {
        if( $pack->getName() === $pack_name )
        {
          if( $count === $occ )
          {
            return $pack;
          }
          
          $count++;
        }
      }
      
      // not found
      return null;
    }
    
    // classic way
    foreach( $this as $pack )
    {
      if( $pack->getName() === $pack_name )
      {
        return $pack;
      }
    }

    // not found
    return null;
  }
  
}
