<?php

/*
 * This file is part of the loops/gif package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Loops\Gif;

/**
 * Buffer size to copy stream.
 */
Config::defineUnpackerStreamBufferLength( 16777216 ); // 16M

/**
 * Class to pack GIF Block to a file or binary string.
 * 
 * Note that specifications use term Block when we use term Pack.
 * 
 * For convenience, most of keys' name are the same than GIF specifications.
 * 
 * So, a strong knowledge of GIF specifications is required to understand
 * packs and GIF generation.
 * 
 * This class follow POOP pattern, for further information see POOP file.
 *
 * @package    loops/gif
 * @author     Loops <pierrotevrard@gmail.com>
 */
class Packer implements \Iterator
{
  
  /**
   * Create a Packer instance from an array of packs.
   * 
   * @param array $packs
   * @return \Loops\Gif\Packer
   * @access public
   * @throws \Loops\Gif\Exception
   * @static
   */
  static public function instance( $packs )
  {
    // create packer instance
    return new static( $packs );
  }
  
  /**
   * Create a Packer instance from an array of packs and
   * return a safe handle to a temporary file with binary data.
   * 
   * @param array $packs
   * @return string
   * @access public
   * @throws \Loops\Gif\Exception
   * @static
   */
  static public function pack( $packs )
  {
    // create packer instance
    $packer = static::instance( $packs );
    
    return $packer(); // invoke it
  }
  
  /**
   * Temporary file handle where to write data.
   * Less expensive that a huge string.
   * 
   * Populated on demand
   * 
   * We using internal pointer of this stream over Iterator 
   * implementation, so be careful, please.
   * 
   * @var resource
   * @access protected
   */
  public $__handle;
  
  /**
   * Packs array, once a pack is proceeded
   * 
   * @var array
   * @access protected
   */
  public $_packs = array();
  
  /**
   * Array of position of pack's data in the handle
   * 
   * Populated on demand
   * 
   * We using internal pointer of this array over Iterator 
   * implementation, so be careful, please.
   * 
   * @var array
   * @access protected
   */
  public $__positions = array();
  
  /**
   * On construction, we want to register the output
   * 
   * @param array $packs
   * @return void
   * @access public
   */
  public function __construct( $packs )
  {
//    // validate Pack_Interface implementation
//    foreach( $packs as $pack )
//    {
//      // each pack must implements this interface
//      if( ! $pack instanceof Pack_Interface )
//      {
//        throw new Exception( array( get_class( $this ) , get_class( $pack ) , '\\Loops\\Gif\\Pack_Interface' ) , 'GIF060' );
//      }
//      
//      // make sure keys are integer and clone the pack to be safe
//      $this->packs[] = clone $pack;
//    }
    
    // in fact, the part above is useless: POOP trusts developer
    $this->_packs = array_values( $packs );
    
    // create tmp handle
    $this->__handle = tmpfile();
  }
  
  /**
   * On destructor, we want to remove the temporary file handle
   * 
   * @param none
   * @return void
   * @access public
   */
  public function __destruct()
  {    
    fclose( $this->__handle );
  }
  
  /**
   * Return binary data
   * 
   * @param none
   * @return array Packs
   * @access public
   */
  public function getBinary()
  {    
    return $this->__invoke();
  }
  
  /**
   * Process packs to create the GIF binary data.
   * 
   * @param none
   * @return string
   * @access public
   */
  public function __invoke()
  {
    $data = '';
    
    foreach( $this as $bin ) $data .= $bin;
    
    return $data;
  }
  
  /**
   * Process next pack
   * 
   * Return true on success
   * 
   * @param none
   * @return boolean
   * @access protected
   * @throws \Loops\Gif\Exception
   */
  public function _run()
  {
    // not any positions for now
    if( ! count($this->__positions) )
    {
      $pack = $this->_packs[0];
      
      // we must have a Header
      if( $pack->getName() !== 'Header' )
      {
        // failure
        throw new Exception( array( get_class( $this ) , $pack->getName() , 'Header' , count($this->__positions) ) , 'GIF061' );
      }
    
      $this->__positions[] = 0;
      $pack->pack( $this->__handle );
      
      // ok
      return true;
    }
    
    // look for last registered pack
    $prev = $this->_packs[count($this->__positions) - 1];
    
    if( $prev->getName() === 'Trailer' )
    {
      // nothing else to read
      // note that some extra packs may be present after the trailer, but we do not care about them
      return false;
    }
    
    // current pack
    $pack = $this->_packs[count($this->__positions)];
    
    if( $prev->getName() === 'Header' )
    {
      // we must have a Logical Screen Descriptor
      if( $pack->getName() !== 'Logical Screen Descriptor' )
      {
        // failure
        throw new Exception( array( get_class( $this ) , $pack->getName() , 'Logical Screen Descriptor' , count($this->__positions) ) , 'GIF061' );
      }
      
      $this->__positions[] = ftell( $this->__handle );
      $pack->pack( $this->__handle );
      
      // ok
      return true;
    }
    
    if( $prev->getName() === 'Logical Screen Descriptor' )
    {
      // we may have a Global Color Table
      if( $prev->getData( 'Global Color Table Flag' ) )
      {
        // we must have a Global Color Table
        if( $pack->getName() !== 'Global Color Table' )
        {
          // failure
          throw new Exception( array( get_class( $this ) , $pack->getName() , 'Global Color Table' , count($this->__positions) ) , 'GIF061' );
        }
        
        $this->__positions[] = ftell( $this->__handle );
        // before to pack the Global Color Table, make sure sizes are identical
        $pack->setColorTableSize( $prev->getData( 'Size of Global Color Table' ) );
        $pack->pack( $this->__handle );

        // ok
        return true;
      }
      
      // in other case, continue
    }
    
    if( $prev->getName() === 'Image Descriptor' )
    {
      // we may have a Local Color Table
      if( $prev->getData( 'Local Color Table Flag' ) )
      {
        // we must have a Local Color Table
        if( $pack->getName() !== 'Local Color Table' )
        {
          // failure
          throw new Exception( array( get_class( $this ) , $pack->getName() , 'Local Color Table' , count($this->__positions) ) , 'GIF061' );
        }
        
        $this->__positions[] = ftell( $this->__handle );
        // before to pack the Local Color Table, make sure sizes are identical
        $pack->setColorTableSize( $prev->getData( 'Size of Local Color Table' ) );
        $pack->pack( $this->__handle );

        // ok
        return true;
      }
      
      // if not, we must have a Table Based Image Data
      if( $pack->getName() !== 'Table Based Image Data' )
      {
        // failure
        throw new Exception( array( get_class( $this ) , $pack->getName() , 'Table Based Image Data' , count($this->__positions) ) , 'GIF061' );
      }
        
      $this->__positions[] = ftell( $this->__handle );
      $pack->pack( $this->__handle );

      // ok
      return true;
    }
    
    if( $prev->getName() === 'Local Color Table' )
    {
      // we must have a TableBasedImageData
      if( $pack->getName() !== 'Table Based Image Data' )
      {
        // failure
        throw new Exception( array( get_class( $this ) , $pack->getName() , 'Table Based Image Data' , count($this->__positions) ) , 'GIF061' );
      }
        
      $this->__positions[] = ftell( $this->__handle );
      $pack->pack( $this->__handle );

      // ok
      return true;
    }
    
    // at this point, the next pack can be anything except some specifics packs
    if( in_array( $pack->getName() , array( 
      'Header' , 
      'Logical Screen Descriptor' , 
      'Global Color Table' , 
      'Local Color Table' , 
      'Table Based Image Data' , 
    ) ) )
    {
      // failure
      throw new Exception( array( get_class( $this ) , $pack->getName() , $prev->getName() , count($this->__positions) ) , 'GIF062' );
    }
        
    $this->__positions[] = ftell( $this->__handle );
    $pack->pack( $this->__handle );

    // ok
    return true;
  }
  
  /**
   * Iterator implementation
   * Return binary data for the current pack, if any
   * 
   * @inheritdoc
   */
  public function current()
  {
    // if current pointer is out of the box
    if( key( $this->__positions ) === null )
    {
      // attemp to compile next pack
      $this->_run();
    }
      
    // are we ok with the expected pack position?
    if( ( $epp = current( $this->__positions ) ) === false )
    {
      // euh... not
      return false;
    }

    // now we can play with the handle
    // is there any next position?
    $npp = @$this->__positions[ key( $this->__positions ) + 1 ];

    // register current pointer position to restore it later
    $cpp = ftell( $this->__handle );

    // if there is no next position, we should be at the end of the file
    if( $npp === null ) $npp = $cpp;

    // seek to expected position
    fseek( $this->__handle , $epp , \SEEK_SET );

    // read data
    $bin = fread( $this->__handle , $npp - $epp );

    // restore current pointer position
    fseek( $this->__handle , $cpp , \SEEK_SET );

    // that's all forks
    return $bin;
  }
  
  /**
   * Iterator implementation
   * Return current key, if any
   * 
   * @inheritdoc
   */
  public function key()
  {
    // if current pointer is out of the box
    if( key( $this->__positions ) === null )
    {
      // attemp to compile next pack
      $this->_run();
    }
    
    // let this function determines if we have a key to return
    return key( $this->__positions );
  }
  
  /**
   * Iterator implementation
   * 
   * @inheritdoc
   */
  public function next()
  {
    // if current pointer is out of the box
    if( key( $this->__positions ) === null )
    {
      // attemp to compile next pack
      $this->_run();
    }
    
    // move internal pointer
    next( $this->__positions );
  }
  
  /**
   * Iterator implementation
   * Reset pointer to first pack
   * 
   * @inheritdoc
   */
  public function rewind()
  {
    // reset internal pointer
    reset( $this->__positions );
  }
  
  /**
   * Iterator implementation
   * If returns false, the foreach() loop will be terminated.
   * 
   * @inheritdoc
   */
  public function valid()
  {
    // if current pointer is out of the box
    if( key( $this->__positions ) === null )
    {
      // attempt to compile next pack
      $this->_run();
      
      // check again
      return key( $this->__positions ) !== null;
    }
    
    // if not, we have binary data to return
    return true;
  }
}
