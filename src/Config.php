<?php

/*
 * This file is part of the loops/gif package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Loops\Gif;

use \Loops\Config\Base;

/**
 * Configuration settings for loops/gif package.
 *
 * @package    loops/gif
 * @author     Loops <pierrotevrard@gmail.com>
 */
class Config extends Base {}

// nothing else, configuration settings will be defined when necessary
