<?php

/*
 * This file is part of the loops/gif package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Loops\Gif;

/**
 * Array of Pack_* class to use.
 * Key are name of pack, value are class name to use.
 * Note that we do not enforce all class to implement \Loops\Gif\Pack_Interface,
 * see Config::definePackFactoryInterface() below.
 */
Config::definePackFactoryMap( array(
  'Header' => 'Loops\\Gif\\Pack_Header' ,
  'Logical Screen Descriptor' => 'Loops\\Gif\\Pack_LogicalScreenDescriptor' ,
  'Global Color Table' => 'Loops\\Gif\\Pack_GlobalColorTable' ,
  'Image Descriptor' => 'Loops\\Gif\\Pack_ImageDescriptor' ,
  'Local Color Table' => 'Loops\\Gif\\Pack_LocalColorTable' ,
  'Table Based Image Data' => 'Loops\\Gif\\Pack_TableBasedImageData' ,
  'Trailer' => 'Loops\\Gif\\Pack_Trailer' ,
  
  'Comment Extension' => 'Loops\\Gif\\Pack_Extension_Comment' ,
  'Plain Text Extension' => 'Loops\\Gif\\Pack_Extension_PlainText' ,
  'Application Extension' => 'Loops\\Gif\\Pack_Extension_Application' ,
  'Graphic Control Extension' => 'Loops\\Gif\\Pack_Extension_GraphicControl' ,
  // disallow this extension
  // 'Unknown Extension' => 'Loops\\Gif\\Pack_Extension_Unknown' ,
  
  'NETSCAPE2.0 Application Extension' => 'Loops\\Gif\\Pack_Extension_Application_Netscape20' ,
) );

/**
 * Array of special interfaces to look for specific pack.
 * Key are name of pack, value are interfaces to verify.
 */
Config::definePackFactoryInterface( array(
  'Header' => 'Loops\\Gif\\Pack_Interface' ,
  'Logical Screen Descriptor' => 'Loops\\Gif\\Pack_Interface' ,
  'Global Color Table' => array( 'Loops\\Gif\\Pack_Interface' , 'Loops\\Gif\\Pack_Trait_Interface_ColorTable' ) ,
  'Image Descriptor' => 'Loops\\Gif\\Pack_Interface' ,
  'Local Color Table' => array( 'Loops\\Gif\\Pack_Interface' , 'Loops\\Gif\\Pack_Trait_Interface_ColorTable' ) ,
  'Table Based Image Data' => 'Loops\\Gif\\Pack_Interface' ,
  'Trailer' => 'Loops\\Gif\\Pack_Interface' ,
  
  'Comment Extension' => 'Loops\\Gif\\Pack_Interface' ,
  'Plain Text Extension' => 'Loops\\Gif\\Pack_Interface' ,
  'Application Extension' => 'Loops\\Gif\\Pack_Interface' ,
  'Graphic Control Extension' => 'Loops\\Gif\\Pack_Interface' ,
  
  'Unknown Extension' => 'Loops\\Gif\\Pack_Interface' ,
  
  'NETSCAPE2.0 Application Extension' => 'Loops\\Gif\\Pack_Interface' ,
) );

/**
 * Factory class for Pack_* extension.
 * 
 * Note that specification use term Block where we use terme Pack.
 *
 * @package    loops/gif
 * @author     Loops <pierrotevrard@gmail.com>
 */
class Pack_Factory
{
  /**
   * Creat an Pack_* instance from its name and some data
   * 
   * @param string $name
   * @param [array] $data
   * @return object \Loops\Gif\Pack_Interface instance
   * @access public
   * @static
   */
  static public function instance( $name , $data = array() )
  {
    $class = static::classname( $name );
    
    $pack = new $class();
    
    foreach( $data as $key => $value )
    {
      $pack->setData( $key , $value );
    }
    
    return $pack;
  }
  
  /**
   * Return class name to use for pack name.
   * 
   * @param string $name
   * @return string \Loops\Gif\Pack_Interface class name to use
   * @access public
   * @static
   */
  static public function classname( $name )
  {
    // class to use
    
    $map = Config::getPackFactoryMap();
    
    if( empty( $map[$name] ) )
    {
      // unknown pack name
      throw new Exception( array( get_called_class() , $name ) , 'GIF040' );
    }
    
    $class = $map[$name];
    
    
    // interface
    
    $map = Config::getPackFactoryInterface();
    
    if( empty( $map[$name] ) )
    {
      // nothing to check
      return $class;
    }
    
    $interfaces = (array)$map[$name];
    
    if( $missing = array_diff( $interfaces , class_implements( $class , true ) ) )
    {
      // missing interface
      throw new Exception( array( get_called_class() , $class , current( $missing ) ) , 'GIF041' );
    }
    
    // everything looks OK
    return $class;
  }
}
