<?php

/*
 * This file is part of the loops/gif package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Loops\Gif;

/**
 * Base class for Pack_* extension.
 * 
 * Note that specifications use term Block when we use term Pack.
 *
 * @see http://www.w3.org/Graphics/GIF/spec-gif89a.txt
 * @package    loops/gif
 * @author     Loops <pierrotevrard@gmail.com>
 * @abstract
 * @implements Pack_Interface
 */
abstract class Pack_Abstract implements Pack_Interface
{
  /**
   * Raw data of the pack.
   * 
   * @var string
   * @access public
   */
  public $_raw;
  
  /**
   * Name of the block
   * 
   * @var string
   * @access public
   */
  public $_name;
  
  /**
   * Data contained in the block
   * 
   * @var array
   * @access public
   */
  public $_data = array();
  
  /**
   * Data part that should be present for unpack/pack
   * 
   * @var array
   * @access public
   */
  public $_parts = array();
  
//  /**
//   * Get raw data
//   * 
//   * @param void
//   * @return string
//   * @access public
//   */
//  public function getRaw()
//  {
//    if( $this->_raw === null )
//    {
//      return $this->pack();
//    }
//  }
  
  /**
   * Get name
   * 
   * @param void
   * @return string
   * @access public
   */
  public function getName()
  {
    return $this->_name;
  }
  
  /**
   * Get all data or a specific data key
   * 
   * @param [string] $key
   * @return mixed
   * @access public
   */
  public function getData( $key = null )
  {
    if( $key === null )
    {
      return $this->_data;
    }
    
    if( isset( $this->_data[$key] ) )
    {
      return $this->_data[$key];
    }
    
    return null;
  }
  
  /**
   * Set all data or data for a specific data key
   * 
   * @param [mixed] Data to assign or a key to use
   * @param [mixed] Data to assign for a specific key
   * @return void
   * @access public
   */
  public function setData( $x ) // just to make one argument mandatory
  {
    // once any data is set, we can undo raw
    $this->_raw = null;
    
    if( func_num_args() > 1 )
    {
      $this->_data[ func_get_arg(0) ] = func_get_arg(1);
    }
    else
    {
      $this->_data = (array)func_get_arg(0);
    }
  }
  
  /**
   * Unpack data from file handle
   * 
   * @param resource $handle File handle
   * @return void
   * @throws \Loops\Gif\Exception
   * @access public
   * @abstract
   */
  abstract public function unpack( $handle );
  
  /**
   * Pack data to file handle
   * 
   * @param resource $handle File handle
   * @return void
   * @throws \Loops\Gif\Exception
   * @access public
   * @abstract
   */
  abstract public function pack( $handle );
  
  /**
   * Validate data parts
   * 
   * @param none
   * @return void
   * @access protected
   * @throws \Loops\Gif\Exception
   */
  public function _validate()
  {
    $missing = array_diff( $this->_parts , array_keys( $this->_data ) );
    
    if( count( $missing ) )
    {
      // missing data
      throw new Exception( array( get_class( $this ) , current( $missing ) ) , 'GIF002' );
    }
  }
  
}
