<?php

/*
 * This file is part of the loops/gif package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Loops\Gif;

/**
 * Object to represent Table Based Image Data pack
 * 
 * Table Based Image Data must come immediately after 
 * Local Color Table or Image Descriptor.
 *
 * @see http://www.w3.org/Graphics/GIF/spec-gif89a.txt 22
 * @package    loops/gif
 * @author     Loops <pierrotevrard@gmail.com>
 * @extends    Pack_Abstract
 * @implements Pack_Trait_Occurrence
 * @implements Pack_Trait_DataSubBlock
 */
class Pack_TableBasedImageData extends Pack_Abstract implements Pack_Trait_Interface_DataSubBlock
{
  // this pack may have multiple Data Sub-Blocks
  use Pack_Trait_DataSubBlock { 
    Pack_Trait_DataSubBlock::_unpack as _unpackDataSubBlockValues;
    Pack_Trait_DataSubBlock::_pack as _packDataSubBlockValues;
  }
  
  /**
   * Name of the block
   * 
   * @var string
   * @access public
   */
  public $_name = 'Table Based Image Data';
  
  /**
   * Data part that must be present for unpack/pack
   * 
   * @var array
   * @access public
   */
  public $_parts = array(
    'LZW Minimum Code Size' , 'Image Data' , 
  );
  
  /**
   * Get the concatenated Data Values
   * Used to know where to retrieve Data Sub-Blocks Values
   * 
   * @param none
   * @return string
   * @access public
   * @implements \Loops\Gif\Pack_Trait_DataSubBlock::_fetchDataSubBlockValues()
   */
  public function _fetchDataSubBlockValues()
  {
    return $this->_data['Image Data'];
  }
  
  /**
   * Set Data Values
   * Used to know where to store Data Sub-Blocks Values
   * 
   * @param string $values
   * @return void
   * @access public
   * @implements \Loops\Gif\Pack_Trait_DataSubBlock::_storeDataSubBlockValues()
   */
  public function _storeDataSubBlockValues( $values )
  {
    // do not use setData() to avoid $this->_raw reset
    $this->_data['Image Data'] = $values;
  }
  
  /**
   * Unpack data from file handle
   * 
   * @inheritdoc
   */
  public function unpack( $handle )
  {
    // first step: LZW Minimum Code Size
    // http://www.w3.org/Graphics/GIF/spec-gif89a.txt 22
    
    $buffer = fgetc( $handle );
    
    if( $buffer === false )
    {
      // unexpected end-of-file, it cannot be anything else
      throw new Exception( array( get_class( $this ) , 1 , strlen( $buffer ) ) , 'GIF001' );
    }
    
    // raw
    $this->_raw = $buffer;
    // data
    $this->_data['LZW Minimum Code Size'] = ord( $buffer );
    
    
    // second step: Image Data
    // multiple Data Sub-Blocks ended by a Block Terminator
    $this->_raw .= $this->_unpackDataSubBlockValues( $handle );

    // validate data to be safe
    $this->_validate();
  }
  
  /**
   * Pack data to file handle
   * 
   * @inheritdoc
   */
  public function pack( $handle )
  {
    if( $this->_raw !== null )
    {
      fwrite( $handle , $this->_raw );
    }
    else
    {
      // validate data to be safe
      $this->_validate();
    
      // first step: pack LZW Minimum Code Size
      // http://www.w3.org/Graphics/GIF/spec-gif89a.txt 22
      fwrite( $handle , pack( 'C' 
        , $this->_data['LZW Minimum Code Size'] // 1 byte: integer to character
      ) );
    
      // second step: Image Data
      // multiple Data Sub-Blocks ended by a Block Terminator
      $this->_packDataSubBlockValues( $handle );
    }
  }
}
