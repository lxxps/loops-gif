<?php

/*
 * This file is part of the loops/gif package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Loops\Gif;

/**
 * Object to represent Logical Screen Descriptor pack
 * 
 * Logical Screen Descriptor must come immediately after Header.
 *
 * @see        http://www.w3.org/Graphics/GIF/spec-gif89a.txt 18
 * @package    loops/gif
 * @author     Loops <pierrotevrard@gmail.com>
 * @extends    Pack_Abstract
 */
class Pack_LogicalScreenDescriptor extends Pack_Abstract
{
  /**
   * Name of the block
   * 
   * @var string
   * @access public
   */
  public $_name = 'Logical Screen Descriptor';
  
  /**
   * Data part that must be present for unpack/pack
   * 
   * @var array
   * @access public
   */
  public $_parts = array(
    'Logical Screen Width' , 'Logical Screen Height' , 'Background Color Index' , 'Pixel Aspect Ratio' ,
    // <Packed Fields>
    'Global Color Table Flag' , 'Color Resolution' , 'Sort Flag' , 'Size of Global Color Table' ,
  );
  
  /**
   * Unpack data from file handle
   * 
   * @inheritdoc
   */
  public function unpack( $handle )
  {
    // Logical Screen Descriptor
    // @see http://www.w3.org/Graphics/GIF/spec-gif89a.txt 18
    $buffer = fread( $handle , 7 );
    
    // assign raw data
    $this->_raw = $buffer;
    
    // unpack
    $format = 'vLogical Screen Width' // 2 bytes
            .'/vLogical Screen Height' // 2 bytes
            .'/C<Packed Fields>' // 1 byte
            .'/CBackground Color Index' // 1 byte
            .'/CPixel Aspect Ratio' // 1 byte
            ;
    $this->_data = @unpack( $format , $buffer );
    
    if( $this->_data === false )
    {
      // unpack failed, unexpected end-of-file, it cannot be anything else
      throw new Exception( array( get_class( $this ) , 7 , strlen( $buffer ) ) , 'GIF001' );
    }

    // Packed Fields
    
    // shortcut
    $packed = $this->_data['<Packed Fields>'];
    
    // extract Packed data
    $this->_data += array(
      'Global Color Table Flag' => ( $packed >> 7 ) & 1 , // read bit OXXXXXXX
      'Color Resolution' => ( $packed >> 4 ) & 7 , // read bits XOOOXXXX
      'Sort Flag' => ( $packed >> 3 ) & 1 , // read bit XXXXOXXX
      'Size of Global Color Table' => ( $packed >> 0 ) & 7 , // read bits XXXXXOOO
      
      '<Packed Fields>_binary' => sprintf( '%08b' , $packed ) , // may help
    );
    
    // validate data to be safe
    $this->_validate();
  }
  
  /**
   * Pack data to file handle
   * 
   * @inheritdoc
   */
  public function pack( $handle )
  {
    if( $this->_raw !== null )
    {
      fwrite( $handle , $this->_raw );
    }
    else
    {
      // validate data to be safe
      $this->_validate();
      
      // pack
      fwrite( $handle , pack( 'vvCCC' 
        , $this->_data['Logical Screen Width'] // 2 bytes
        , $this->_data['Logical Screen Height'] // 2 bytes
              
        , // <Packed Fields>, 1 byte
            ( ( $this->_data['Global Color Table Flag'] & 1 ) << 7 ) // write bit OXXXXXXX
          + ( ( $this->_data['Color Resolution'] & 7 ) << 4 ) // write bits XOOOXXXX
          + ( ( $this->_data['Sort Flag'] & 1 ) << 3 ) // write bit XXXXOXXX
          + ( ( $this->_data['Size of Global Color Table'] & 7 ) << 0 ) // write bits XXXXXOOO
        
        , $this->_data['Background Color Index'] // 1 byte
        , $this->_data['Pixel Aspect Ratio'] // 1 byte
      ) );
    }
  }
}
