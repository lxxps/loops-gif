<?php

/*
 * This file is part of the loops/gif package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Loops\Gif;

/**
 * Object to represent Image Descriptor pack
 *
 * @see http://www.w3.org/Graphics/GIF/spec-gif89a.txt 20
 * @package    loops/gif
 * @author     Loops <pierrotevrard@gmail.com>
 * @extends    Pack_Abstract
 */
class Pack_Trailer extends Pack_Abstract
{
  /**
   * Name of the block
   * 
   * @var string
   * @access public
   */
  public $_name = 'Trailer';
  
  /**
   * Image Separator
   * 
   * @var string
   * @const
   */
  const TRAILER = "\x3B";
  
  /**
   * Unpack data from file handle
   * 
   * @inheritdoc
   */
  public function unpack( $handle )
  {
    // first step: GIF Trailer
    $buffer = fgetc( $handle );
    
    if( $buffer !== self::TRAILER )
    {
      if( $buffer === false )
      {
        // unexpected end-of-file, it cannot be anything else
        throw new Exception( array( get_class( $this ) , 1 , strlen( $buffer ) ) , 'GIF001' );
      }
      
      // invalid trailer
      throw new Exception( array( get_class( $this ) , ord( $buffer )  , ord( self::TRAILER ) ) , 'GIF018' );
    }
    
    // raw
    $this->_raw = $buffer;
    // trailer is not part of data
    
    // validate data to be safe
    $this->_validate();
  }
  
  /**
   * Pack data to file handle
   * 
   * @inheritdoc
   */
  public function pack( $handle )
  {
    if( $this->_raw !== null )
    {
      fwrite( $handle , $this->_raw );
    }
    else
    {
      // validate data to be safe
      $this->_validate();
      
      // write GIF Trailer
      fwrite( $handle , self::TRAILER );
    }
  }
  
  /**
   * Validate data
   * 
   * @inheritdoc
   */
  public function _validate()
  {
    // nothing to validate for trailer
  }
}
