<?php

/*
 * This file is part of the loops/gif package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Loops\Gif;

/**
 * Object to represent Header pack
 *
 * @see http://www.w3.org/Graphics/GIF/spec-gif89a.txt 17
 * @package    loops/gif
 * @author     Loops <pierrotevrard@gmail.com>
 * @extends    Pack_Abstract
 */
class Pack_Header extends Pack_Abstract
{
  /**
   * Name of the block
   * 
   * @var string
   * @access public
   */
  public $_name = 'Header';
  
  /**
   * Data part that must be present for unpack/pack
   * 
   * @var array
   * @access public
   */
  public $_parts = array(
    'Signature' , 'Version' ,
  );
  
  /**
   * Unpack data from file handle
   * 
   * @inheritdoc
   */
  public function unpack( $handle )
  {
    // Header
    // @see http://www.w3.org/Graphics/GIF/spec-gif89a.txt 17
    $buffer = fread( $handle , 6 );
    
    // raw data
    $this->_raw = $buffer;

    // unpack
    $format = 'A3Signature' // 3 bytes
            .'/A3Version' // 3 bytes
            ;
    $this->_data = @unpack( $format , $buffer );
    
    if( $this->_data === false )
    {
      // unpack failed, unexpected end-of-file, it cannot be anything else
      throw new Exception( array( get_class( $this ) , 6 , strlen( $buffer ) ) , 'GIF001' );
    }
    
    // validate data to be safe
    $this->_validate();
  }
  
  /**
   * Pack data to file handle
   * 
   * @inheritdoc
   */
  public function pack( $handle )
  {
    if( $this->_raw !== null )
    {
      fwrite( $handle , $this->_raw );
    }
    else
    {
      // validate data to be safe
      $this->_validate();
    
      // pack Signature and Version
      // http://www.w3.org/Graphics/GIF/spec-gif89a.txt 22
      fwrite( $handle , pack( 'A3A3' 
        , $this->_data['Signature'] // 3 characters
        , $this->_data['Version'] // 3 characters
      ) );
    }
  }
  
  /**
   * Validate data
   * 
   * @inheritdoc
   */
  public function _validate()
  {
    // call parent validation
    parent::_validate();
    
    // verify Signature
    if( $this->_data['Signature'] !== 'GIF' )
    {
      // invalid Header Signature
      throw new Exception( array( get_class( $this ) , $this->_data['Signature'] , 'GIF' ) , 'GIF006' );
    }

    // verify Version
    if( $this->_data['Version'] !== '87a' && $this->_data['Version'] !== '89a' )
    {
      // invalid Header Version
      throw new Exception( array( get_class( $this ) , $this->_data['Version'] , '87a" or "89a'  ) , 'GIF007' );
    }
  }
}
