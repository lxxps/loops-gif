<?php

/*
 * This file is part of the loops/gif package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Loops\Gif;

/**
 * Interface for Pack_*.
 *
 * @package    loops/gif
 * @author     Loops <pierrotevrard@gmail.com>
 * @abstract
 */
interface Pack_Interface
{
  
  /**
   * Get name
   * 
   * @param void
   * @return string
   * @access public
   */
  public function getName();
  
  /**
   * Get all data or a specific data key
   * 
   * @param [string] $key
   * @return mixed
   * @access public
   */
  public function getData( $key = null );
  
  /**
   * Set all data or data for a specific data key
   * 
   * @param [mixed] Data to assign or a key to use
   * @param [mixed] Data to assign for a specific key
   * @return void
   * @access public
   */
  public function setData( $x );
  
  /**
   * Unpack data from file handle
   * 
   * @param resource $handle File handle
   * @return void
   * @throws \Loops\Gif\Exception
   * @access public
   */
  public function unpack( $handle );
  
  /**
   * Pack data to file handle
   * 
   * @param resource $handle File handle
   * @return void
   * @throws \Loops\Gif\Exception
   * @access public
   * @abstract
   */
  public function pack( $handle );
  
}
