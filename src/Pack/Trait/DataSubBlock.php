<?php

/*
 * This file is part of the loops/gif package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Loops\Gif;

/**
 * Trait for Pack_* classes.
 * 
 * Used on Block that has Data Sub-Blocks.
 * Note that Data Sub-Blocks allways end by a Block Terminator
 *
 * @see http://www.w3.org/Graphics/GIF/spec-gif89a.txt 15
 * @see http://www.w3.org/Graphics/GIF/spec-gif89a.txt 16
 * @package    loops/gif
 * @author     Loops <pierrotevrard@gmail.com>
 * @trait
 */
trait Pack_Trait_DataSubBlock
{
  /**
   * Get the concatenated Data Values
   * Used to know where to retrieve Data Sub-Blocks Values
   * 
   * @param none
   * @return string
   * @access public
   * @abstract
   */
  abstract public function _fetchDataSubBlockValues();
  
  /**
   * Set Data Values
   * Used to know where to retrieve 
   * Used to know where to store Data Sub-Blocks Values
   * 
   * @param string $values
   * @return void
   * @access public
   * @abstract
   */
  abstract public function _storeDataSubBlockValues( $values );
  
  /**
   * Unpack expected Data Sub-Blocks part from current file handle
   * 
   * @param resource $handle
   * @return string
   * @access protected
   * @throws \Loops\Gif\Exception
   */
  public function _unpack( $handle )
  {
    // initialize
    $values = '';
    $buffer = '';
    
    // multiple Data Sub-Blocks ended by a Block Terminator
    // Data Sub-block
    // @see http://www.w3.org/Graphics/GIF/spec-gif89a.txt 15
    // Block Terminator 
    // @see http://www.w3.org/Graphics/GIF/spec-gif89a.txt 16
    
    // not a Block Terminator?
    while( ( $len = fgetc( $handle ) ) !== "\x00" )
    {
      if( $len === false )
      {
        // unexpected end-of-file
        throw new Exception( array( get_class( $this ) , 1 , strlen( $buffer ) ) , 'GIF001' );
      }
      
      // so it is the Block Size of a Data Sub-Block
      $part = fread( $handle , ord( $len ) );
    
      // not sure if we want that...
      if( strlen( $part ) !== ord( $len ) )
      {
        // unexpected end-of-file
        throw new Exception( array( get_class( $this ) , ord( $len ) , strlen( $part ) ) , 'GIF001' );
      }
      
      // update buffer, with Block Size
      $buffer .= $len.$part;
      
      // add Data Values, without Block Size
      $values .= $part;
    }
    
    // finally assign values
    $this->_storeDataSubBlockValues( $values );
    
    // add Block Terminator to buffer
    $buffer .= "\x00";
    
    return $buffer;
  }
  
  /**
   * Pack current color table to current file handle
   * 
   * @param resource $handle
   * @return string
   * @access protected
   * @throws \Loops\Gif\Exception
   */
  public function _pack( $handle )
  {
    // fetch values
    $values = $this->_fetchDataSubBlockValues();
    
    // multiple Data Sub-Blocks ended by a Block Terminator
    // Data Sub-block
    // @see http://www.w3.org/Graphics/GIF/spec-gif89a.txt 15
    // Block Terminator 
    // @see http://www.w3.org/Graphics/GIF/spec-gif89a.txt 16

    // 255 (0xFF) is the maximum number of characters 
    // that we can store in one Data Sub-block
    for( $i = 0xFF, $imax = strlen( $values ); $i <= $imax; $i += 0xFF )
    {
      // append a fixed Block Size of 255 (0xFF)
      fwrite( $handle , "\xFF" );
      // append corresponding Values part, AS IS
      fwrite( $handle , substr( $values , $i - 0xFF , 0xFF ) );
    }

    // now look for other caracters
    $k = $imax - $i + 0xFF;
    if( $k > 0 )
    {
      // append expected Block Size
      fwrite( $handle , pack( 'C' 
        , $k // 1 byte: integer to character
      ) );
      // append residual Values part, AS IS
      fwrite( $handle , substr( $values , $i - 0xFF ) );
    }

    // finally append Block Terminator
    fwrite( $handle , "\x00" );
  }
  
}
