<?php

/*
 * This file is part of the loops/gif package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Loops\Gif;

/**
 * Trait for Pack_* classes.
 * 
 * Used on Block that has Color Table.
 * Note that size has to correspond to GIF Size of Color Table
 *   nb colors = 2 << size
 *   max size 7 means 256 colors
 *   min size 0 means 2 colors
 *
 * @see http://www.w3.org/Graphics/GIF/spec-gif89a.txt
 * @package    loops/gif
 * @author     Loops <pierrotevrard@gmail.com>
 * @trait
 */
trait Pack_Trait_ColorTable
{
  
  /**
   * Get color table
   * Used to know where to retrieve the color table
   * 
   * @param none
   * @return array
   * @access public
   * @abstract
   */
  abstract public function _fetchColorTable();
  
  /**
   * Set color table
   * Used to know where to store the color table
   * 
   * @param array $table
   * @return void
   * @access public
   * @abstract
   */
  abstract public function _storeColorTable( $table );
  
  /**
   * Color table size
   * Represents the size of color table:
   *  nb colors = 2 << size
   * 
   * @var integer
   * @access protected
   */
  public $_size;
  
  /**
   * Get color table size
   * 
   * @param none
   * @return integer
   * @access public
   */
  public function getColorTableSize()
  {
    if( $this->_size === null )
    {
      // determine size from color table
      $this->setColorTableCount( count( $this->_fetchColorTable() ) );
    }
    
    // then return it
    return $this->_size;
  }
  
  /**
   * For Color Table, we need to be able to set size from previous Block
   * 
   * @param integer $size
   * @return void
   * @access public
   */
  public function setColorTableSize( $size )
  {
    // maximum value is 7
    $this->_size = (int)$size;
    
    if( $this->_size > 7 ) $this->_size = 7; // means 256 colors
    if( $this->_size < 0 ) $this->_size = 0; // means 2 colors
  }
  
  /**
   * Get maximum number of colors
   * 
   * @param none
   * @return integer
   * @access public
   */
  public function getColorTableCount()
  {
    if( $this->_size === null )
    {
      // determine size from color table
      $this->setColorTableCount( count( $this->_fetchColorTable() ) );
    }
    
    return ( 2 << $this->_size );
  }
  
  /**
   * Set maximum number of colors expected
   * This will adjust size property to handle this amount
   * 
   * @param integer $count
   * @return void
   * @access public
   */
  public function setColorTableCount( $count )
  {
    // maximum size, means 256 colors
    $this->_size = 7;
    
    // note that the table may contains unused colors
    while( $count <= ( 1 << $this->_size ) )
    {
      $this->_size--;
    }
    
    // minimum size, means 2 colors
    if( $this->_size < 0 ) $this->_size = 0; 
  }
  
  /**
   * Unpack expected Color Table part from current file handle
   * 
   * @param resource $handle
   * @return string
   * @access protected
   * @throws \Loops\Gif\Exception
   */
  public function _unpack( $handle )
  {
    if( $this->_size === null )
    {
      // no size
      throw new Exception( array( get_class( $this ) ) , 'GIF015' );
    }
    
    // number of bits to read
    $len = 3 * $this->getColorTableCount();
    
    $buffer = fread( $handle , $len );
    
    // we want to extract color table
    // @see http://www.w3.org/Graphics/GIF/spec-gif89a.txt 19
    // @see http://www.w3.org/Graphics/GIF/spec-gif89a.txt 21

    $table = array();

    // unpack
    $raw = @unpack( 'C*' 
      , $buffer // all bytes are integer
    );
    // note that unpack( 'C*' , $buffer ) 
    // returns an array that starts at index 1
    
    // check number of bits readed
    if( $raw === false || count($raw) !== $len )
    {
      // unexpected end-of-file
      throw new Exception( array( get_class( $this ) , $len , strlen( $buffer ) ) , 'GIF001' );
    }

    // each 3 bits represents a RGB color
    $i = $this->getColorTableCount();
    while( $i-- )
    {
      // get color position in the array
      $j = 3*$i;

      // remember that unpack( 'C*' , $buffer ) 
      // returns an array that starts at index 1
      $table[$i] = array( 
        'Red' => $raw[$j+1] , 
        'Green' => $raw[$j+2] , 
        'Blue' => $raw[$j+3] 
      );
    }
    
    // finally assign table
    $this->_storeColorTable( $table );
    
    return $buffer;
  }
  
  /**
   * pack current color table to current file handle
   * 
   * @param resource $handle
   * @return string
   * @access protected
   * @throws \Loops\Gif\Exception
   */
  public function _pack( $handle )
  {
    // at first fetch the real count
    // this will also set size if necessary
    $max = $this->getColorTableCount();
    
    // fetch color table
    $table = $this->_fetchColorTable();

    // look for number of colors in the table
    $imax = count( $table );
    
    // make sure number of colors does not exceed expected size
    if( $imax > $max ) $imax = $max;

    // pack colors
    for( $i = 0; $i < $imax; $i++ )
    {
      fwrite( $handle , pack( 'CCC'
        , $table[$i]['Red']
        , $table[$i]['Green']
        , $table[$i]['Blue']
      ) );
    }

    // if there is not enough colors to fit color table size
    // fill it with black color
    for( ; $i < $max; $i++ )
    {
      fwrite( $handle , "\x00\x00\x00" );
    }
  }
}
