<?php

/*
 * This file is part of the loops/gif package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Loops\Gif;

/**
 * Interface to know when a class implements Data Sub-Blocks ability.
 * 
 * To use with Pack_Trait_DataSubBlock
 *
 * @package    loops/gif
 * @author     Loops <pierrotevrard@gmail.com>
 * @interface
 */
interface Pack_Trait_Interface_DataSubBlock
{
}
