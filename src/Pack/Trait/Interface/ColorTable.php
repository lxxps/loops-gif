<?php

/*
 * This file is part of the loops/gif package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Loops\Gif;

/**
 * Interface to know when a class implements ColorTable ability.
 * 
 * To use with Pack_Trait_ColorTable
 *
 * @package    loops/gif
 * @author     Loops <pierrotevrard@gmail.com>
 * @interface
 */
interface Pack_Trait_Interface_ColorTable
{
  /**
   * Get color table size
   * 
   * @param none
   * @return integer
   * @access public
   */
  public function getColorTableSize();
  
  /**
   * For Color Table, we need to be able to set size from previous Block
   * 
   * @param integer $size
   * @return void
   * @access public
   */
  public function setColorTableSize( $size );
  
}
