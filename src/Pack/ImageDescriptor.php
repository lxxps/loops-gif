<?php

/*
 * This file is part of the loops/gif package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Loops\Gif;

/**
 * Object to represent Image Descriptor pack
 *
 * @see http://www.w3.org/Graphics/GIF/spec-gif89a.txt 20
 * @package    loops/gif
 * @author     Loops <pierrotevrard@gmail.com>
 * @extends    Pack_Abstract
 * @implements Pack_Trait_Interface_Occurrence
 */
class Pack_ImageDescriptor extends Pack_Abstract
{
  /**
   * Image Separator
   * 
   * @var string
   * @const
   */
  const IMAGE_SEPARATOR = "\x2C";
  
  /**
   * Name of the block
   * 
   * @var string
   * @access public
   */
  public $_name = 'Image Descriptor';
  
  /**
   * Data part that must be present for unpack/pack
   * 
   * @var array
   * @access public
   */
  public $_parts = array(
    'Image Left Position' , 'Image Top Position' , 'Image Width' , 'Image Height' ,
    // <Packed Fields>
    'Local Color Table Flag' , 'Interlace Flag' , 'Sort Flag' , 'Reserved' , 'Size of Local Color Table' ,
  );
  
  /**
   * Unpack data from file handle
   * 
   * @inheritdoc
   */
  public function unpack( $handle )
  {
    // first step: Image Separator
    $buffer = fgetc( $handle );
    
    if( $buffer !== self::IMAGE_SEPARATOR )
    {
      if( $buffer === false )
      {
        // unexpected end-of-file
        throw new Exception( array( get_class( $this ) , 1 , strlen( $buffer ) ) , 'GIF001' );
      }
      
      // invalid introducer
      throw new Exception( array( get_class( $this ) , ord( $buffer ) , ord( self::IMAGE_SEPARATOR ) ) , 'GIF010' );
    }
    
    // raw
    $this->_raw = $buffer;
    // image separator is not part of data
    
    
    // Image Descriptor
    // @see http://www.w3.org/Graphics/GIF/spec-gif89a.txt 20
    $buffer = fread( $handle , 9 );
    
    // assign raw data
    $this->_raw .= $buffer;
    
    // unpack
    $format = 'vImage Left Position' // 2 bytes
            .'/vImage Top Position' // 2 bytes
            .'/vImage Width' // 2 bytes
            .'/vImage Height' // 2 bytes
            .'/C<Packed Fields>' // 1 byte
            ;
    $this->_data = @unpack( $format , $buffer );
    
    if( $this->_data === false )
    {
      // unpack failed, unexpected end-of-file, it cannot be anything else
      throw new Exception( array( get_class( $this ) , 9 , strlen( $buffer ) ) , 'GIF001' );
    }

    // Packed Fields
    
    // shortcut
    $packed = $this->_data['<Packed Fields>'];
    
    // extract Packed data
    $this->_data += array(
      'Local Color Table Flag' => ( $packed >> 7 ) & 1 , // read bit OXXXXXXX
      'Interlace Flag' => ( $packed >> 6 ) & 1 , // read bit XOXXXXXX
      'Sort Flag' => ( $packed >> 5 ) & 1 , // read bit XXOXXXXX
      'Reserved' => ( $packed >> 3 ) & 3 , // read bits XXXOOXXX
      'Size of Local Color Table' => ( $packed >> 0 ) & 7 , // read bits XXXXXOOO
      
      '<Packed Fields>_binary' => sprintf( '%08b' , $packed ) , // may help
    );
    
    // validate data to be safe
    $this->_validate();
  }
  
  /**
   * Pack data to file handle
   * 
   * @inheritdoc
   */
  public function pack( $handle )
  {
    if( $this->_raw !== null )
    {
      fwrite( $handle , $this->_raw );
    }
    else
    {
      // validate data to be safe
      $this->_validate();
      
      // write Image Separator
      fwrite( $handle , self::IMAGE_SEPARATOR );
      
      // pack Data
      fwrite( $handle , pack( 'vvvvC' 
        , $this->_data['Image Left Position'] // 2 bytes
        , $this->_data['Image Top Position'] // 2 bytes
        , $this->_data['Image Width'] // 2 bytes
        , $this->_data['Image Height'] // 2 bytes
              
        , // <Packed Fields>, 1 byte
            ( ( $this->_data['Local Color Table Flag'] & 1 ) << 7 ) // write bit OXXXXXXX
          + ( ( $this->_data['Interlace Flag'] & 1 ) << 6 ) // write bit XOXXXXXX
          + ( ( $this->_data['Sort Flag'] & 1 ) << 5 ) // write bit XXOXXXXX
          + ( ( $this->_data['Reserved'] & 3 ) << 3 ) // write bits XXXOOXXX
          + ( ( $this->_data['Size of Local Color Table'] & 7 ) << 0 ) // write bits XXXXXOOO
      ) );
    }
  }
}
