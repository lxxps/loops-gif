<?php

/*
 * This file is part of the loops/gif package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Loops\Gif;

/**
 * Object to represent Global Color Table pack
 * 
 * Global Color Table must come immediately after Logical Screen Descriptor.
 * Size of the color table is determined by the Logical Screen Descriptor.
 *
 * @see        http://www.w3.org/Graphics/GIF/spec-gif89a.txt 21
 * @package    loops/gif
 * @author     Loops <pierrotevrard@gmail.com>
 * @extends    Pack_Abstract
 * @implements Pack_Trait_Interface_ColorTable
 */
class Pack_GlobalColorTable extends Pack_Abstract implements Pack_Trait_Interface_ColorTable
{
  // this pack has color table (obviously)
  use Pack_Trait_ColorTable { 
    Pack_Trait_ColorTable::_unpack as _unpackColorTable;
    Pack_Trait_ColorTable::_pack as _packColorTable;
  }
  
  /**
   * Name of the block
   * 
   * @var string
   * @access public
   */
  public $_name = 'Global Color Table';
  
  /**
   * Data part that must be present for unpack/pack
   * 
   * @var array
   * @access public
   */
  public $_parts = array(
    'Color Table' , // this is a custom part used to store all colors in an array
  );
  
  /**
   * Get color table
   * Used to know where to retrieve the color table
   * 
   * @param none
   * @return array
   * @access public
   * @implements \Loops\Gif\Pack_Trait_ColorTable::_fetchColorTable()
   */
  public function _fetchColorTable()
  {
    return $this->_data['Color Table'];
  }
  
  /**
   * Set color table
   * Used to know where to store the color table
   * 
   * @param array $table
   * @return void
   * @access public
   * @abstract
   * @implements \Loops\Gif\Pack_Trait_ColorTable::_storeColorTable()
   */
  public function _storeColorTable( $table )
  {
    // do not use setData() to avoid $this->_raw reset
    $this->_data['Color Table'] = $table;
  }
  
  /**
   * Unpack data from file handle
   * 
   * @inheritdoc
   */
  public function unpack( $handle )
  {
    // extract binary data from expected size
    $buffer = $this->_unpackColorTable( $handle );
    
    // raw data
    $this->_raw = $buffer;
    
    // validate data to be safe
    $this->_validate();
  }
  
  /**
   * Pack data to file handle
   * 
   * @inheritdoc
   */
  public function pack( $handle )
  {
    if( $this->_raw !== null )
    {
      fwrite( $handle , $this->_raw );
    }
    else
    {
      // validate data to be safe
      $this->_validate();
    
      // append color table
      $this->_packColorTable( $handle );
    }
  }
}
