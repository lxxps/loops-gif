<?php

/*
 * This file is part of the loops/gif package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Loops\Gif;

/**
 * Object to represent Plain Text Extension pack
 *
 * @see http://www.w3.org/Graphics/GIF/spec-gif89a.txt 25
 * @package    loops/gif
 * @author     Loops <pierrotevrard@gmail.com>
 * @extends    Pack_Extension_Abstract
 */
class Pack_Extension_PlainText extends Pack_Extension_Abstract
{
  
  /**
   * Label of the Extension
   * 
   * @var string
   * @const
   */
  const EXTENSION_LABEL = "\x01";
  
  /**
   * Name of the block
   * 
   * @var string
   * @access public
   */
  public $_name = 'Plain Text Extension';
  
  /**
   * Data part that must be present for unpack/pack
   * 
   * @var array
   * @access public
   */
  public $_parts = array(
    'Text Grid Left Position' , 'Text Grid Top Position' , 'Text Grid Width' , 'Text Grid Height' ,
    'Character Cell Width' , 'Character Cell Height' , 'Text Foreground Color Index' , 'Text Background Color Index' ,
    
    'Plain Text Data' ,
  );
  
  /**
   * Get label
   * 
   * @param none
   * @return char
   * @access public
   */
  public function getLabel()
  {
    return self::EXTENSION_LABEL;
  }
  
  /**
   * Get the concatenated Data Values
   * Used to know where to retrieve Data Sub-Blocks Values
   * 
   * @param none
   * @return string
   * @access public
   * @implements \Loops\Gif\Pack_Trait_DataSubBlock::_fetchDataSubBlockValues()
   */
  public function _fetchDataSubBlockValues()
  {
    return $this->_data['Plain Text Data'];
  }
  
  /**
   * Set Data Values
   * Used to know where to store Data Sub-Blocks Values
   * 
   * @param string $values
   * @return void
   * @access public
   * @implements \Loops\Gif\Pack_Trait_DataSubBlock::_storeDataSubBlockValues()
   */
  public function _storeDataSubBlockValues( $values )
  {
    // do not use setData() to avoid $this->_raw reset
    $this->_data['Plain Text Data'] = $values;
  }
  
  /**
   * Unpack data from file handle
   * 
   * @inheritdoc
   */
  public function unpack( $handle )
  {
    // first step: Extension Introducer
    $intro = fgetc( $handle );
    
    if( $intro !== self::EXTENSION_INTRODUCER )
    {
      if( $intro === false )
      {
        // unexpected end-of-file
        throw new Exception( array( get_class( $this ) , 1 , strlen( $intro ) ) , 'GIF001' );
      }
      
      // invalid introducer
      throw new Exception( array( get_class( $this ) , ord( $intro ) , ord( self::EXTENSION_INTRODUCER ) ) , 'GIF020' );
    }
    
    // raw
    $this->_raw = $intro;
    // introducer is not part of data
    
    
    // second step: Extension Label
    $label = fgetc( $handle );
    
    if( $label !== $this->getLabel() )
    {
      if( $label === false )
      {
        // unexpected end-of-file
        throw new Exception( array( get_class( $this ) , 1 , strlen( $label ) ) , 'GIF001' );
      }
      
      // invalid label
      throw new Exception( array( get_class( $this ) , ord( $label ) , ord( $this->getLabel() ) ) , 'GIF021' );
    }
    
    // raw
    $this->_raw .= $label;
    // label is not part of data
    
    
    // third step: Plain Text special Data Sub-Block
     
    // Plain Text Extension starts with one special Data Sub-Block
    // that must be treated differently than unknown extensions
    
    // third step A: Block Size
    // must be 12
    $buffer = fgetc( $handle );

    // validate
    if( $buffer !== "\x0C" ) // 12
    {
      if( $buffer === false )
      {
        // unexpected end-of-file
        throw new Exception( array( get_class( $this ) , 1 , strlen( $buffer ) ) , 'GIF001' );
      }
      
      // invalid block size
      throw new Exception( array( get_class( $this ) , ord( $buffer ) , 12 ) , 'GIF022' );
    }
    
    // raw
    $this->_raw .= $buffer;

    // third step B: Block Data
    $buffer = fread( $handle , 12 );
    
    // one Data Sub-Blocks WITHOUT Block Terminator
    $this->_raw .= $buffer;

    // unpack
    $format = 'vText Grid Left Position' // 2 bytes
            .'/vText Grid Top Position' // 2 bytes
            .'/vText Grid Width' // 2 bytes
            .'/vText Grid Height' // 2 bytes
            .'/CCharacter Cell Width' // 1 byte
            .'/CCharacter Cell Height' // 1 byte
            .'/CText Foreground Color Index' // 1 byte
            .'/CText Background Color Index' // 1 byte
            ;
    $this->_data = @unpack( $format , $buffer );
    
    if( $this->_data === false )
    {
      // unpack fails, unexpected end-of-file, it cannot be anything else
      throw new Exception( array( get_class( $this ) , 12 , strlen( $buffer ) ) , 'GIF001' );
    }
    
    
    // fourth step: Plain Text Data Sub-Blocks
     
    // then, Plain Text Extension has multiple Data Sub-Blocks
    // that represents Plain Text Data
    
    // multiple Data Sub-Blocks ended by a Block Terminator
    $this->_raw .= $this->_unpackDataSubBlockValues( $handle );

    
    // validate data to be safe
    $this->_validate();
  }
  
  /**
   * Pack data to file handle
   * 
   * @inheritdoc
   */
  public function pack( $handle )
  {
    if( $this->_raw !== null )
    {
      fwrite( $handle , $this->_raw );
    }
    else
    {
      // validate data to be safe
      $this->_validate();
      
    
      // first step: pack Extension Introducer and Label
      // http://www.w3.org/Graphics/GIF/spec-gif89a.txt 25
      fwrite( $handle , self::EXTENSION_INTRODUCER.$this->getLabel() );
      
    
      // second step: pack Plain Text special Data Sub-Block
      // but WIHTOUT Block Terminator
      
      // pack
      fwrite( $handle , pack( 'CvvvvCCCC'
        , 12 // Block Size, 1 byte
              
        , $this->_data['Text Grid Left Position'] // 2 bytes
        , $this->_data['Text Grid Top Position'] // 2 bytes
        , $this->_data['Text Grid Width'] // 2 bytes
        , $this->_data['Text Grid Height'] // 2 bytes
        , $this->_data['Character Cell Width'] // 1 byte
        , $this->_data['Character Cell Height'] // 1 byte
        , $this->_data['Text Foreground Color Index'] // 1 byte
        , $this->_data['Text Background Color Index'] // 1 byte
      ) );
      
      
      // third step: pack Plain Text Data
      // multiple Data Sub-Blocks ended by a Block Terminator
      $this->_packDataSubBlockValues( $handle );
    }
  }
}
