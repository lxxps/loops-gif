<?php

/*
 * This file is part of the loops/gif package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Loops\Gif;

/**
 * Object to represent an unknown Extension pack.
 * 
 * Each extension has the same structure:
 *  - Extension Introducer          Byte
 *  - Extension Label               Byte
 *  - Data                          Data Sub-blocks
 *  - Block Terminator              Byte
 *
 * @see http://www.w3.org/Graphics/GIF/spec-gif89a.txt
 * @package    loops/gif
 * @author     Loops <pierrotevrard@gmail.com>
 * @extends    Pack_Abstract
 * @implements Pack_Trait_Occurrence
 * @implements Pack_Trait_DataSubBlock
 * @abstract
 */
abstract class Pack_Extension_Abstract extends Pack_Abstract implements Pack_Trait_Interface_DataSubBlock
{
  // an extension may have multiple Data Sub-Blocks
  use Pack_Trait_DataSubBlock { 
    Pack_Trait_DataSubBlock::_unpack as _unpackDataSubBlockValues;
    Pack_Trait_DataSubBlock::_pack as _packDataSubBlockValues;
  }
  
  /**
   * Extension Introducer
   * 
   * @var string
   * @const
   */
  const EXTENSION_INTRODUCER = "\x21";
  
  /**
   * Data part that must be present for unpack/pack
   * 
   * @var array
   * @access public
   */
  public $_parts = array(
    'Extension Data' , // this field will be used to store Data Sub-blocks values
  );
  
  /**
   * Get label
   * 
   * @param void
   * @return char
   * @access public
   * @abstract
   */
  abstract public function getLabel();
  
  /**
   * Get the concatenated Data Values
   * Used to know where to retrieve Data Sub-Blocks Values
   * 
   * @param none
   * @return string
   * @access public
   * @implements \Loops\Gif\Pack_Trait_DataSubBlock::_fetchDataSubBlockValues()
   */
  public function _fetchDataSubBlockValues()
  {
    return $this->_data['Extension Data'];
  }
  
  /**
   * Set Data Values
   * Used to know where to store Data Sub-Blocks Values
   * 
   * @param string $values
   * @return void
   * @access public
   * @implements \Loops\Gif\Pack_Trait_DataSubBlock::_storeDataSubBlockValues()
   */
  public function _storeDataSubBlockValues( $values )
  {
    // do not use setData() to avoid $this->_raw reset
    $this->_data['Extension Data'] = $values;
  }
  
  /**
   * Unpack data from file handle
   * 
   * @inheritdoc
   */
  public function unpack( $handle )
  {
    // determine occurrence
    $this->setOccurrence();
    
    
    // first step: Extension Introducer
    $intro = fgetc( $handle );
    
    if( $intro !== self::EXTENSION_INTRODUCER )
    {
      if( $intro === false )
      {
        // unexpected end-of-file
        throw new Exception( array( get_class( $this ) , 1 , strlen( $intro ) ) , 'GIF001' );
      }
      
      // invalid introducer
      throw new Exception( array( get_class( $this ) , ord( $intro ) , ord( self::EXTENSION_INTRODUCER ) ) , 'GIF020' );
    }
    
    // raw
    $this->_raw = $intro;
    // introducer is not part of data
    
    
    // second step: Extension Label
    $label = fgetc( $handle );
    
    if( $label !== $this->getLabel() )
    {
      if( $label === false )
      {
        // unexpected end-of-file
        throw new Exception( array( get_class( $this ) , 1 , strlen( $label ) ) , 'GIF001' );
      }
      
      // invalid label
      throw new Exception( array( get_class( $this ) , ord( $label ) , ord( $this->getLabel() ) ) , 'GIF021' );
    }
    
    // raw
    $this->_raw .= $label;
    // label is not part of data
    
    
    // third step: Extension Data
    // multiple Data Sub-Blocks ended by a Block Terminator
    $this->_raw .= $this->_unpackDataSubBlockValues( $handle );

    // validate data to be safe
    $this->_validate();
  }
  
  /**
   * Pack data to file handle
   * 
   * @inheritdoc
   */
  public function pack( $handle )
  {
    if( $this->_raw !== null )
    {
      fwrite( $handle , $this->_raw );
    }
    else
    {
      // validate data to be safe
      $this->_validate();
    
      // first step: write Extension Introducer and Label
      fwrite( $handle , self::EXTENSION_INTRODUCER.$this->getLabel() );
    
      // second step: pack Extension Data
      // multiple Data Sub-Blocks ended by a Block Terminator
      $this->_packDataSubBlockValues( $handle );
    }
  }
}
