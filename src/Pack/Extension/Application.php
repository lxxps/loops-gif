<?php

/*
 * This file is part of the loops/gif package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Loops\Gif;

/**
 * Object to represents Application Extension pack
 * 
 * Application Extension may have special meaning and comportment.
 * 
 * For Application Extension that are not relevant, the name is build 
 * dynamically using this pattern:
 * %Application Identifier%%Application Authentication Code% Application Extension
 * 
 * @see http://www.w3.org/Graphics/GIF/spec-gif89a.txt 26
 * @package    loops/gif
 * @author     Loops <pierrotevrard@gmail.com>
 * @extends    Pack_Extension_Abstract
 */
class Pack_Extension_Application extends Pack_Extension_Abstract
{
  
  /**
   * Label of the Extension
   * 
   * @var string
   * @const
   */
  const EXTENSION_LABEL = "\xFF";
  
  /**
   * Data part that must be present for unpack/pack
   * 
   * @var array
   * @access public
   */
  public $_parts = array(
    'Application Identifier' , 'Application Authentication Code' ,
    
    'Application Data' ,
  );
  
  /**
   * Get label
   * 
   * @param none
   * @return char
   * @access public
   */
  public function getLabel()
  {
    return self::EXTENSION_LABEL;
  }
  
  /**
   * Get the concatenated Data Values
   * Used to know where to retrieve Data Sub-Blocks Values
   * 
   * @param none
   * @return string
   * @access public
   * @implements \Loops\Gif\Pack_Trait_DataSubBlock::_fetchDataSubBlockValues()
   */
  public function _fetchDataSubBlockValues()
  {
    return $this->_data['Application Data'];
  }
  
  /**
   * Set Data Values
   * Used to know where to store Data Sub-Blocks Values
   * 
   * @param string $values
   * @return void
   * @access public
   * @implements \Loops\Gif\Pack_Trait_DataSubBlock::_storeDataSubBlockValues()
   */
  public function _storeDataSubBlockValues( $values )
  {
    // do not use setData() to avoid $this->_raw reset
    $this->_data['Application Data'] = $values;
  }
  
  /**
   * Unpack data from file handle
   * 
   * @inheritdoc
   */
  public function unpack( $handle )
  {
    // first step: Extension Introducer
    $intro = fgetc( $handle );
    
    if( $intro !== self::EXTENSION_INTRODUCER )
    {
      if( $intro === false )
      {
        // unexpected end-of-file
        throw new Exception( array( get_class( $this ) , 1 , strlen( $intro ) ) , 'GIF001' );
      }
      
      // invalid introducer
      throw new Exception( array( get_class( $this ) , ord( $intro ) , ord( self::EXTENSION_INTRODUCER ) ) , 'GIF020' );
    }
    
    // raw
    $this->_raw = $intro;
    // introducer is not part of data
    
    
    // second step: Extension Label
    $label = fgetc( $handle );
    
    if( $label !== $this->getLabel() )
    {
      if( $label === false )
      {
        // unexpected end-of-file
        throw new Exception( array( get_class( $this ) , 1 , strlen( $label ) ) , 'GIF001' );
      }
      
      // invalid label
      throw new Exception( array( get_class( $this ) , ord( $label ) , ord( $this->getLabel() ) ) , 'GIF021' );
    }
    
    // raw
    $this->_raw .= $label;
    // label is not part of data
    
    
    // third step: Application special Data Sub-Block
     
    // Application Extension starts with one special Data Sub-Block
    // that must be treated differently than unknown extensions
    
    // third step A: Block Size
    // must be 11
    $buffer = fgetc( $handle );

    // validate
    if( $buffer !== "\x0B" ) // 11
    {
      if( $buffer === false )
      {
        // unexpected end-of-file
        throw new Exception( array( get_class( $this ) , ord( $len ) , strlen( $part ) ) , 'GIF001' );
      }
      
      // invalid block size
      throw new Exception( array( get_class( $this ) , ord( $buffer ) , 11 ) , 'GIF022' );
    }
    
    // raw
    $this->_raw .= $buffer;

    
    // third step B: Block Data
    $buffer = fread( $handle , 11 );
    
    // one Data Sub-Blocks WITHOUT Block Terminator
    $this->_raw .= $buffer;

    // unpack
    $format = 'A8Application Identifier' // 8 bytes
            .'/A3Application Authentication Code' // 3 bytes
            ;
    $this->_data = @unpack( $format , $buffer );
    
    if( $this->_data === false )
    {
      // unexpected end-of-file
      throw new Exception( array( get_class( $this ) , 11 , strlen( $buffer ) ) , 'GIF001' );
    }
    
    
    // fourth step: Application Data Sub-Blocks
     
    // then, Application Extension has multiple Data Sub-Blocks
    // that represents Application Data
    
    // multiple Data Sub-Blocks ended by a Block Terminator
    $this->_raw .= $this->_unpackDataSubBlockValues( $handle );

    
    // validate data to be safe
    $this->_validate();
  }
  
  /**
   * Pack data to file handle
   * 
   * @inheritdoc
   */
  public function pack( $handle )
  {
    if( $this->_raw !== null )
    {
      fwrite( $handle , $this->_raw );
    }
    else
    {
      // validate data to be safe
      $this->_validate();
      
    
      // first step: pack Extension Introducer and Label
      fwrite( $handle , self::EXTENSION_INTRODUCER.$this->getLabel() );
      
    
      // second step: pack Application special Data Sub-Block
      // but WIHTOUT Block Terminator
      
      // pack
      fwrite( $handle , pack( 'CA8A3'
        , 11 // Block Size, 1 byte
              
        , $this->_data['Application Identifier'] // 8 bytes
        , $this->_data['Application Authentication Code'] // 3 bytes
      ) );
      
      
      // third step: pack Application Data
      // multiple Data Sub-Blocks ended by a Block Terminator
      $this->_packDataSubBlockValues( $handle );
    }
  }
  
  /**
   * Validate data parts
   * 
   * @inheritdoc
   */
  public function _validate()
  {
    // call parent validation
    parent::_validate();
    
    // at this point, we can assign Application Extension Name
    $this->_name = $this->_data['Application Identifier']
                 . $this->_data['Application Authentication Code']
                 . ' Application Extension';
  }
}
