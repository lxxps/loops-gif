<?php

/*
 * This file is part of the loops/gif package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Loops\Gif;

/**
 * Object to represents Application Extension for Netscape 2.0 pack
 * 
 * This Application Extension is particular beause it contains information
 * about delay time.
 * 
 * Not all animated GIF has this pack.
 * 
 *
 * @see http://en.wikipedia.org/wiki/Graphics_Interchange_Format#Animated_GIF
 * @package    loops/gif
 * @author     Loops <pierrotevrard@gmail.com>
 * @extends    Pack_Extension_Application
 */
class Pack_Extension_Application_Netscape20 extends Pack_Extension_Application
{
  
  /**
   * Name of the block
   * 
   * The name will be overwrote in _validate() method
   * 
   * @var string
   * @access public
   */
  public $_name = 'NETSCAPE2.0 Application Extension';
  
  /**
   * Data part that must be present for unpack/pack
   * 
   * @var array
   * @access public
   */
  public $_parts = array(
    'Application Identifier' , 'Application Authentication Code' ,
    
    'Sub-Block Index' , 'Number of Repetitions' ,
  );
  
  /**
   * Unpack data from file handle
   * 
   * @inheritdoc
   */
  public function unpack( $handle )
  {
    // first step: Extension Introducer
    $intro = fgetc( $handle );
    
    if( $intro !== self::EXTENSION_INTRODUCER )
    {
      if( $intro === false )
      {
        // unexpected end-of-file
        throw new Exception( array( get_class( $this ) , 1 , strlen( $intro ) ) , 'GIF001' );
      }
      
      // invalid introducer
      throw new Exception( array( get_class( $this ) , ord( $intro ) , ord( self::EXTENSION_INTRODUCER ) ) , 'GIF020' );
    }
    
    // raw
    $this->_raw = $intro;
    // introducer is not part of data
    
    
    // second step: Extension Label
    $label = fgetc( $handle );
    
    if( $label !== $this->getLabel() )
    {
      if( $label === false )
      {
        // unexpected end-of-file
        throw new Exception( array( get_class( $this ) , 1 , strlen( $label ) ) , 'GIF001' );
      }
      
      // invalid label
      throw new Exception( array( get_class( $this ) , ord( $label ) , ord( $this->getLabel() ) ) , 'GIF021' );
    }
    
    // raw
    $this->_raw .= $label;
    // label is not part of data
    
    
    // third step: Application special Data Sub-Block
     
    // Application Extension starts with one special Data Sub-Block
    // that must be treated differently than unknown extensions
    
    // third step A: Block Size
    // must be 11
    $buffer = fgetc( $handle );

    // validate
    if( $buffer !== "\x0B" ) // 11
    {
      if( $buffer === false )
      {
        // unexpected end-of-file
        throw new Exception( array( get_class( $this ) , 1 , strlen( $buffer ) ) , 'GIF001' );
      }
      
      // invalid block size
      throw new Exception( array( get_class( $this ) , ord( $buffer ) , 12 ) , 'GIF022' );
    }
    
    // raw
    $this->_raw .= $buffer;

    // third step B: Block Data
    $buffer = fread( $handle , 11 );
    
    // one Data Sub-Blocks WITHOUT Block Terminator
    $this->_raw .= $buffer;

    // unpack
    $format = 'A8Application Identifier' // 2 bytes
            .'/A3Application Authentication Code' // 2 bytes
            ;
    $this->_data = @unpack( $format , $buffer );
    
    if( $this->_data === false )
    {
      // unpack failed, unexpected end-of-file, it cannot be anything else
      throw new Exception( array( get_class( $this ) , 11 , strlen( $buffer ) ) , 'GIF001' );
    }
    
    
    // fourth step: Application Extension for Netscape 2.0 special Data Sub-Block
    
    // fourth step A: Block Size
    // must be 3
    $buffer = fgetc( $handle );

    // validate
    if( $buffer !== "\x03" ) // 3
    {
      if( $buffer === false )
      {
        // unexpected end-of-file
        throw new Exception( array( get_class( $this ) , 1 , strlen( $buffer ) ) , 'GIF001' );
      }
      
      // invalid block size
      throw new Exception( array( get_class( $this ) , ord( $buffer ) , 3 ) , 'GIF022' );
    }
    
    // raw
    $this->_raw .= $buffer;

    // fourth step B: Block Data
    $buffer = fread( $handle , 3 );
    
    // one Data Sub-Blocks WITHOUT Block Terminator
    $this->_raw .= $buffer;

    // unpack
    $format = 'CSub-Block Index' // 1 byte
            .'/vNumber of Repetitions' // 2 bytes
            ;
    $tmp = @unpack( $format , $buffer );
    
    if( $tmp === false )
    {
      // unpack failed, unexpected end-of-file, it cannot be anything else
      throw new Exception( array( get_class( $this ) , 3 , strlen( $buffer ) ) , 'GIF001' );
    }
    
    // update data
    $this->_data += $tmp;
    
    
    // now, we must have a Block Terminator 
    // @see http://www.w3.org/Graphics/GIF/spec-gif89a.txt 16
    if( ( $buffer = fgetc( $handle ) ) !== "\x00" )
    {
      if( $buffer === false )
      {
        // unexpected end-of-file
        throw new Exception( array( get_class( $this ) , 1 , strlen( $buffer ) ) , 'GIF001' );
      }
      
      // invalid block terminator
      throw new Exception( array( get_class( $this ) , ord( $buffer ) , 0 ) , 'GIF023' );
    }
    
    // add Block Terminator to raw data
    $this->_raw .= "\x00";

    
    // validate data to be safe
    $this->_validate();
  }
  
  /**
   * Pack data to file handle
   * 
   * @inheritdoc
   */
  public function pack( $handle )
  {
    if( $this->_raw !== null )
    {
      fwrite( $handle , $this->_raw );
    }
    else
    {
      // validate data to be safe
      $this->_validate();
      
    
      // first step: pack Extension Introducer and Label
      fwrite( $handle , self::EXTENSION_INTRODUCER.$this->getLabel() );
      
    
      // second step: pack Application special Data Sub-Block
      // but WIHTOUT Block Terminator
      
      // pack
      fwrite( $handle , pack( 'CA8A3'
        , 11 // Block Size, 1 byte
              
        , $this->_data['Application Identifier'] // 8 bytes
        , $this->_data['Application Authentication Code'] // 3 bytes
      ) );
      
    
      // third step: pack Application for Netscape 2.0 special Data Sub-Block
      // and Block Terminator
      
      // pack
      fwrite( $handle , pack( 'CCvC'
        , 3 // Block Size, 1 byte
              
        , $this->_data['Sub-Block Index'] // 1 byte
        , $this->_data['Number of Repetitions'] // 2 bytes
        
        , 0 // Block Terminator
      ) );
    }
  }
  
  /**
   * Validate data parts
   * 
   * @inheritdoc
   */
  public function _validate()
  {
    // call parent validation
    parent::_validate();
    
    // verify Application Identifier
    if( $this->_data['Application Identifier'] !== 'NETSCAPE' )
    {
      // invalid Application Identifier
      throw new Exception( array( get_class( $this ) , $this->_data['Application Identifier'] , 'NETSCAPE' ) , 'GIF030' );
    }
    
    // verify Application Authentication Code
    if( $this->_data['Application Authentication Code'] !== '2.0' )
    {
      // invalid Application Authentication Code
      throw new Exception( array( get_class( $this ) , $this->_data['Application Authentication Code'] , '2.0' ) , 'GIF031' );
    }

    // verify Sub-Block Index
    if( $this->_data['Sub-Block Index'] !== 1 )
    {
      // invalid Sub-Block Index
      throw new Exception( array( get_class( $this ) , $this->_data['Sub-Block Index'] , 1  ) , 'GIF032' );
    }
  }
}
