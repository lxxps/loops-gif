<?php

/*
 * This file is part of the loops/gif package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Loops\Gif;

/**
 * Object to represent Graphic Control Extension pack
 *
 * @see http://www.w3.org/Graphics/GIF/spec-gif89a.txt 23
 * @package    loops/gif
 * @author     Loops <pierrotevrard@gmail.com>
 * @extends    Pack_Extension_Abstract
 */
class Pack_Extension_GraphicControl extends Pack_Extension_Abstract
{
  
  /**
   * Label of the Extension
   * 
   * @var string
   * @const
   */
  const EXTENSION_LABEL = "\xF9";
  
  /**
   * Name of the block
   * 
   * @var string
   * @access public
   */
  public $_name = 'Graphic Control Extension';
  
  /**
   * Data part that must be present for unpack/pack
   * 
   * @var array
   * @access public
   */
  public $_parts = array(
    'Delay Time' , 'Transparent Color Index' ,
    // <Packed Fields>
    'Reserved' , 'Disposal Method' , 'User Input Flag' , 'Transparent Color Flag' ,
  );
  
  /**
   * Get label
   * 
   * @param none
   * @return char
   * @access public
   */
  public function getLabel()
  {
    return self::EXTENSION_LABEL;
  }
  
  /**
   * Unpack data from file handle
   * 
   * @inheritdoc
   */
  public function unpack( $handle )
  {
    // first step: Extension Introducer
    $intro = fgetc( $handle );
    
    if( $intro !== self::EXTENSION_INTRODUCER )
    {
      if( $intro === false )
      {
        // unexpected end-of-file
        throw new Exception( array( get_class( $this ) , 1 , strlen( $intro ) ) , 'GIF001' );
      }
      
      // invalid introducer
      throw new Exception( array( get_class( $this ) , ord( $intro ) , ord( self::EXTENSION_INTRODUCER ) ) , 'GIF020' );
    }
    
    // raw
    $this->_raw = $intro;
    // introducer is not part of data
    
    
    // second step: Extension Label
    $label = fgetc( $handle );
    
    if( $label !== $this->getLabel() )
    {
      if( $label === false )
      {
        // unexpected end-of-file
        throw new Exception( array( get_class( $this ) , 1 , strlen( $label ) ) , 'GIF001' );
      }
      
      // invalid label
      throw new Exception( array( get_class( $this ) , ord( $label ) , ord( $this->getLabel() ) ) , 'GIF021' );
    }
    
    // raw
    $this->_raw .= $label;
    // label is not part of data
    
    
    // third step: Graphic Control special Data Sub-Block
     
    // Graphic Control Extension has only one special Data Sub-Block
    // that must be treated differently than unknown extensions
    
    // third step A: Block Size
    // must be 4
    $buffer = fgetc( $handle );

    // validate
    if( $buffer !== "\x04" ) // 4
    {
      if( $buffer === false )
      {
        // unexpected end-of-file
        throw new Exception( array( get_class( $this ) , 1 , strlen( $buffer ) ) , 'GIF001' );
      }
      
      // invalid block size
      throw new Exception( array( get_class( $this ) , ord( $buffer ) , 4 ) , 'GIF022' );
    }
    
    // raw
    $this->_raw .= $buffer;

    // third step B: Block Data
    $buffer = fread( $handle , 4 );
    
    // one Data Sub-Blocks WITHOUT Block Terminator
    $this->_raw .= $buffer;

    // unpack
    $format = 'C<Packed Fields>' // 1 bytes
            .'/vDelay Time' // 2 bytes
            .'/CTransparent Color Index' // 1 byte
            ;
    $this->_data = @unpack( $format , $buffer );
    
    if( $this->_data === false )
    {
      // unpack failed, invalid buffer length, it cannot be anything else
      throw new Exception( array( get_class( $this ) , 4 , strlen( $buffer ) ) , 'GIF001' );
    }

    // Packed Fields
    
    // shortcut
    $packed = $this->_data['<Packed Fields>'];
    
    // extract Packed data
    $this->_data += array(
      'Reserved' => ( $packed >> 5 ) & 7 , // read bits OOOXXXXX
      'Disposal Method' => ( $packed >> 2 ) & 7 , // read bits XXXOOOXX
      'User Input Flag' => ( $packed >> 1 ) & 1 , // read bit XXXXXXOX
      'Transparent Color Flag' => ( $packed >> 0 ) & 1 , // read bit XXXXXXXO
      
      '<Packed Fields>_binary' => sprintf( '%08b' , $packed ) , // may help
    );
    
    
    // now, we must have a Block Terminator 
    // @see http://www.w3.org/Graphics/GIF/spec-gif89a.txt 16
    if( ( $buffer = fgetc( $handle ) ) !== "\x00" )
    {
      if( $buffer === false )
      {
        // unexpected end-of-file
        throw new Exception( array( get_class( $this ) , 1 , strlen( $buffer ) ) , 'GIF001' );
      }
      
      // invalid block terminator
      throw new Exception( array( get_class( $this ) , ord( $buffer ) , 0 ) , 'GIF023' );
    }
    
    // add Block Terminator to raw data
    $this->_raw .= "\x00";

    
    // validate data to be safe
    $this->_validate();
  }
  
  /**
   * Pack data to file handle
   * 
   * @inheritdoc
   */
  public function pack( $handle )
  {
    if( $this->_raw !== null )
    {
      fwrite( $handle , $this->_raw );
    }
    else
    {
      // validate data to be safe
      $this->_validate();
      
    
      // first step: pack Extension Introducer and Label
      // http://www.w3.org/Graphics/GIF/spec-gif89a.txt 23
      fwrite( $handle , self::EXTENSION_INTRODUCER.$this->getLabel() );
      
    
      // second step: pack Graphic Control special Data Sub-Block
      // and Block Terminator
      
      // pack
      fwrite( $handle , pack( 'CCvCC'
        , 4 // Block Size, 1 byte
        
        , // <Packed Fields>, 1 byte
            ( ( $this->_data['Reserved'] & 1 ) << 5 ) // write bits OOOXXXXX
          + ( ( $this->_data['Disposal Method'] & 7 ) << 2 ) // write bits XXXOOOXX
          + ( ( $this->_data['User Input Flag'] & 1 ) << 1 ) // write bit XXXXXXOX
          + ( ( $this->_data['Transparent Color Flag'] & 1 ) << 0 ) // write bit XXXXXXXO

        , $this->_data['Delay Time'] // 2 bytes
        , $this->_data['Transparent Color Index'] // 1 byte
        
        , 0 // Block Terminator
      ) );
    }
  }
}
