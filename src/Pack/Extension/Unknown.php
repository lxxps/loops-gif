<?php

/*
 * This file is part of the loops/gif package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Loops\Gif;

/**
 * Object to represent an unknown Extension pack.
 * 
 * Each extension has the same structure:
 *  - Extension Introducer          Byte
 *  - Extension Label               Byte
 *  - Data                          Data Sub-blocks
 *  - Block Terminator              Byte
 *
 * @see http://www.w3.org/Graphics/GIF/spec-gif89a.txt
 * @package    loops/gif
 * @abstract
 * @author     Loops <pierrotevrard@gmail.com>
 * @extends    Pack_Extension_Abstract
 */
class Pack_Extension_Unknown extends Pack_Extension_Abstract
{
  
  /**
   * Name of the block
   * 
   * @var string
   * @access public
   */
  public $_name = 'Unknown Extension';
  
  /**
   * Label of the Extension
   * 
   * @var string
   * @access protected
   */
  public $_label;
  
  /**
   * Get label
   * 
   * @param none
   * @return char
   * @access public
   */
  public function getLabel()
  {
    return $this->_label;
  }
  
  /**
   * Set label
   * 
   * @param char $label
   * @return void
   * @access public
   */
  public function setLabel( $label )
  {
    $this->_label = $label;
  }
  
  /**
   * Unpack data from file handle
   * 
   * @inheritdoc
   */
  public function unpack( $handle )
  {
    // first step: Extension Introducer
    $intro = fgetc( $handle );
    
    if( $intro !== self::EXTENSION_INTRODUCER )
    {
      if( $intro === false )
      {
        // unexpected end-of-file
        throw new Exception( array( get_class( $this ) , 1 , strlen( $intro ) ) , 'GIF001' );
      }
      
      // invalid introducer
      throw new Exception( array( get_class( $this ) , ord( $intro ) , ord( self::EXTENSION_INTRODUCER ) ) , 'GIF020' );
    }
    
    // raw
    $this->_raw = $intro;
    // introducer is not part of data
    
    
    // second step: Extension Label
    $this->_label = fgetc( $handle );
    
    if( $this->_label === false )
    {
      // unexpected end-of-file
      throw new Exception( array( get_class( $this ) , 1 , strlen( $this->_label ) ) , 'GIF001' );
    }
    
    // raw
    $this->_raw .= $this->_label;
    // label is not part of data
    
    
    // third step: Extension Data
    // multiple Data Sub-Blocks ended by a Block Terminator
    $this->_raw .= $this->_unpackDataSubBlockValues( $handle );

    // validate data to be safe
    $this->_validate();
  }
  
  /**
   * Validate data
   * 
   * @inheritdoc
   */
  public function _validate()
  {
    // call parent validation
    parent::_validate();
    
    // verify label
    if( $this->_label === null )
    {
      // label must be set
      throw new Exception( array( get_class( $this ) ) , 'GIF035' );
    }
  }
}
