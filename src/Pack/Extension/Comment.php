<?php

/*
 * This file is part of the loops/gif package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Loops\Gif;

/**
 * Object to represent Comment Extension pack
 *
 * @see http://www.w3.org/Graphics/GIF/spec-gif89a.txt 24
 * @package    loops/gif
 * @author     Loops <pierrotevrard@gmail.com>
 * @extends    Pack_Extension_Abstract
 */
class Pack_Extension_Comment extends Pack_Extension_Abstract
{
  
  /**
   * Label of the Extension
   * 
   * @var string
   * @const
   */
  const EXTENSION_LABEL = "\xFE";
  
  /**
   * Name of the block
   * 
   * @var string
   * @access public
   */
  public $_name = 'Comment Extension';
  
  /**
   * Data part that must be present for unpack/pack
   * 
   * @var array
   * @access public
   */
  public $_parts = array(
    'Comment Data' , // this field will be used to store Data Sub-blocks values
  );
  
  /**
   * Get label
   * 
   * @param none
   * @return char
   * @access public
   */
  public function getLabel()
  {
    return self::EXTENSION_LABEL;
  }
  
  /**
   * Get the concatenated Data Values
   * Used to know where to retrieve Data Sub-Blocks Values
   * 
   * @param none
   * @return string
   * @access public
   * @implements \Loops\Gif\Pack_Trait_DataSubBlock::_fetchDataSubBlockValues()
   */
  public function _fetchDataSubBlockValues()
  {
    return $this->_data['Comment Data'];
  }
  
  /**
   * Set Data Values
   * Used to know where to store Data Sub-Blocks Values
   * 
   * @param string $values
   * @return void
   * @access public
   * @implements \Loops\Gif\Pack_Trait_DataSubBlock::_storeDataSubBlockValues()
   */
  public function _storeDataSubBlockValues( $values )
  {
    // do not use setData() to avoid $this->_raw reset
    $this->_data['Comment Data'] = $values;
  }
  
  /**
   * Unpack data from file handle
   * 
   * @inheritdoc
   */
  public function unpack( $handle )
  {
    // first step: Extension Introducer
    $intro = fgetc( $handle );
    
    if( $intro !== self::EXTENSION_INTRODUCER )
    {
      if( $intro === false )
      {
        // unexpected end-of-file
        throw new Exception( array( get_class( $this ) , 1 , strlen( $intro ) ) , 'GIF001' );
      }
      
      // invalid introducer
      throw new Exception( array( get_class( $this ) , ord( $intro ) , ord( self::EXTENSION_INTRODUCER ) ) , 'GIF020' );
    }
    
    // raw
    $this->_raw = $intro;
    // introducer is not part of data
    
    
    // second step: Extension Label
    $label = fgetc( $handle );
    
    if( $label !== $this->getLabel() )
    {
      if( $label === false )
      {
        // unexpected end-of-file
        throw new Exception( array( get_class( $this ) , 1 , strlen( $label ) ) , 'GIF001' );
      }
      
      // invalid label
      throw new Exception( array( get_class( $this ) , ord( $label ) , ord( $this->getLabel() ) ) , 'GIF021' );
    }
    
    // raw
    $this->_raw .= $label;
    // label is not part of data
    
    
    // third step: Comment Data
    // multiple Data Sub-Blocks ended by a Block Terminator
    $this->_raw .= $this->_unpackDataSubBlockValues( $handle );

    // validate data to be safe
    $this->_validate();
  }
  
  /**
   * Pack data to file handle
   * 
   * @inheritdoc
   */
  public function pack( $handle )
  {
    if( $this->_raw !== null )
    {
      fwrite( $handle , $this->_raw );
    }
    else
    {
      // validate data to be safe
      $this->_validate();
    
      // first step: pack Extension Introducer and Label
      fwrite( $handle , self::EXTENSION_INTRODUCER.$this->getLabel() );
    
      // second step: pack Comment Data
      // multiple Data Sub-Blocks ended by a Block Terminator
      $this->_packDataSubBlockValues( $handle );
    }
  }
}
