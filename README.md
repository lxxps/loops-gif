**Loops\Gif**
================

Library used to parse/compile GIF files.



Requirements:
-------------

- At least PHP 5.3.
- Loops\Autoloader package.
- Loops\Exception package.
- Loops\Config package.



How to use it:
--------------


### Autoloading

If you do not have composer, call bootstrap:

    :::php
      require $path_to_package.'/bootstrap.inc.php';

*This library use package-oriented autoloading with a directory for package 
namespace (PSR-4) and underscore as directory separator for class names (PSR-0).*


### GIF packs

A GIF pack represents a single part of GIF protocol block and sub-block. Pack
names come from the [GIF89a specifications](https://www.w3.org/Graphics/GIF/spec-gif89a.txt), 
so be sure to be familiar with these specifications if you want to use this 
library efficiently.

Currently, this library can manage these kinds of GIF blocks:

- Header
- Logical Screen Descriptor
- Global Color Table
- Image Descriptor
- Local Color Table
- Table Based Image Data
- Graphic Control Extension
- Comment Extension
- Plain Text Extension
- Application Extension
- Unknown Extension
- Trailer
- NETSCAPE 2.0 Application Extension (specific Application Extension)

Each pack can be parse or compile to a stream resource. Each pack may contains 
data, data names also come from the [GIF89a specifications](https://www.w3.org/Graphics/GIF/spec-gif89a.txt).

    :::php
      // parse a Graphic Control Extension 
      $pack = new \Loops\Gif\Pack_GraphicControlExtension();
      // unpack from stream
      $pack->unpack( $stream );
      
      // then we can fetch data
      $pack->getData( 'Reserved' );
      $pack->getData( 'Disposal Method' );
      $pack->getData( 'User Input Flag' );
      $pack->getData( 'Transparent Color Flag' );
      $pack->getData( 'Delay Time' );
      $pack->getData( 'Transparent Color Index' );
      
      // compile a Image Descriptor
      $pack = new \Loops\Gif\Pack_ImageDescriptor();
      // assign all required data
      $pack->setData( 'Image Left Position' , 40 );
      $pack->setData( 'Image Top Position' , 20 );
      $pack->setData( 'Image Width' , 400 );
      $pack->setData( 'Image Height' , 200 );
      $pack->setData( 'Local Color Table Flag' , 0b1 );
      $pack->setData( 'Interlace Flag' , 0b0 );
      $pack->setData( 'Sort Flag' , 0b1 );
      $pack->setData( 'Reserved' , 0b01 );
      $pack->setData( 'Size of Local Color Table' , 0b101 );
      
      // then pack it to stream
      $pack->pack( $stream );

By convenience, to compile a pack, all data must be set.


### Unpacker

The `\Loops\Gif\Unpacker` class is used to parse a GIF file/stream/binary and 
to return an array of GIF packs.

    :::php
      $packs = \Loops\Gif\Unpacker::unpack( $file_or_stream_or_binary );

If you need to detect only if a certain block is present, a `\Loops\Gif\Unpacker` 
instance can be used. This instance will process packs on demand.

    :::php
      $unpacker = \Loops\Gif\Unpacker::instance( $file_or_stream_or_binary );
      // is there at least two Image Descriptor (ie. animated GIF)
      $unpacker->hasPack( 'Image Descriptor' , 1 ); // first occurence is 0
      // GIF packs after this one have not been parsed yet
      
      // process only 5 packs
      $i = 5;
      while( $i-- )
      {
        // unpack current pack and go to next one
        $unpacker->next();
      }

The unpacking process may throws `\Loops\Gif\Exception` if the GIF data are 
corrupted.


### Packer

The `\Loops\Gif\Packer` class is used to compile an array of GIF packs and to 
return binary data of the GIF.

    :::php
      $binary = \Loops\Gif\Packer::pack( $array_of_packs );

You can also play with a `\Loops\Gif\Packer` instance to compile partially the 
array of packs.

    :::php
      $packer = \Loops\Gif\Packer::instance( $array_of_packs );

      // process only 5 packs
      $i = 5;
      while( $i-- )
      {
        // pack current pack and go to next one
        $packer->next();
      }

The packing process may throw exceptions if packs are not in a correct order.

So, some important GIF specifications constraints must be respected in order to 
compile successfully an array of packs:

- Header must be the first pack in the array, and nowhere else
- Logical Screen Descriptor must be the second pack in the array, and nowhere else
- Global Color Table must be the third pack in the array, and nowhere else, but 
  only if the Logical Screen Descriptor claims that there is a Global Color Table 
  (Global Color Table Flag)
- Local Color Table can be present only after Image Descriptor, but only if the 
  Image Descriptor claims that there is a Local Color Table (Local Color Table Flag)
- Table Based Image Data can be present only after Local Color Table, or Image 
  Descriptor if the Image Descriptor claims that there is no Local Color Table 
  (Local Color Table Flag)



Contributors:
-------------

- Pierrot Evrard aka Loops — [https://twitter.com/lxxps](https://twitter.com/lxxps)


### Wanna contribute?

There is only one rule to follow: **Challenge yourself**.